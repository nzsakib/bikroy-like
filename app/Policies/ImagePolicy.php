<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\ItemGallery;

class ImagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function destroy(UserAccount $user, ItemGallery $image)
    {
        return $user->id == $image->item->posts->first()->poster_id;
    }
}
