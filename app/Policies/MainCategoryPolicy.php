<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Admin;
use App\Models\MainCategory;
use App\Models\UserInterface;

class MainCategoryPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin() && $user->hasRole('super-admin')) {
            return true;
        }

        if ($user->isAdmin() && $user->can('category')) {
            return true;
        }

        if ($user->isAdmin() && !$user->can('category')) {
            return false;
        }
    }

    public function index(UserInterface $user, MainCategory $mainCategory) 
    {   
        return $user->can('category');
    }
}
