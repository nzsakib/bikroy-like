<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\UserInterface;
use App\Models\ChatMessage;

class ChatMessagePolicy
{
    use HandlesAuthorization;

    public function destroy(UserInterface $user, ChatMessage $message)
    {
        return $user->id == $message->sender_id;
    }
}
