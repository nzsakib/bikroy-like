<?php

namespace App\Policies;

use App\Models\Post;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\UserInterface;
use App\Enum\PostStatus;

class PostPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin() && $user->hasRole('super-admin')) {
            return true;
        }

        if ($user->isAdmin() && !$user->can('post')) {
            return false;
        }

        if ($user->isAdmin() && $user->can('post')) {
            return true;
        }
    }

    public function show(?UserInterface $user, Post $post)
    {
        return $post->status == Post::STATUS_ACTIVE || $post->status == Post::STATUS_SOLD;
    }

    public function update(UserInterface $user, Post $post)
    {
        return $post->poster_id == $user->id && $post->status != PostStatus::sold;
    }

    public function destroy(UserInterface $user, Post $post)
    {
        return $post->poster_id == $user->id;
    }

    public function index(UserInterface $user, Post $post)
    {
        return false;
    }

    public function makeActive(UserInterface $user, Post $post)
    {
        return false;
    }

    public function featured(UserInterface $user, Post $post)
    {
        return false;
    }

    /**
     * Determine if the post is sold | Sold or purchased post can not be deleted
     */
    public function sold(UserInterface $user, Post $post)
    {
        return $post->item->sold == 0 && $post->status != Post::STATUS_SOLD;
    }
}
