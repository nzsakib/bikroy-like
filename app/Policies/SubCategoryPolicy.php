<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubCategoryPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin() && $user->hasRole('super-admin')) {
            return true;
        }

        if ($user->isAdmin() && $user->can('category')) {
            return true;
        }
    }
}
