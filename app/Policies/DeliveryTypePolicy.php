<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeliveryTypePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
