<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\UserInterface;

class UserAccountPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin() && $user->hasRole('super-admin')) {
            return true;
        }

        if ($user->isAdmin() && !$user->can('user')) {
            return false;
        }

        if ($user->isAdmin() && $user->can('user')) {
            return true;
        }
    }

    /**
     * Determine whether a admin/user can list all user in the system
     *
     * @param UserInterface $user
     * @param UserAccount $toUser
     * @return boolean
     */
    public function index(UserInterface $user, UserAccount $toUser)
    {
        return false;
    }

    /**
     * Determine where an user can update a account
     *
     * @param UserInterface $user
     * @param UserAccount $toUser
     * @return boolean
     */
    public function update(UserInterface $user, UserAccount $toUser)
    {
        return $user->id == $toUser->id;
    }

    /**
     * Determine whether a user can delete a user account
     *
     * @param UserInterface $user
     * @param UserAccount $toUser
     * @return boolean
     */
    public function destroy(UserInterface $user, UserAccount $toUser)
    {
        return $user->id == $toUser->id;
    }

    /**
     * Determine if a user can ban a user
     *
     * @param UserInterface $user
     * @param UserAccount $toUser
     * @return boolean
     */
    public function ban(UserInterface $user, UserAccount $toUser)
    {
        return false;
    }

    /**
     * Determine if a user can activate a user
     *
     * @param UserInterface $user
     * @param UserAccount $toUser
     * @return boolean
     */
    public function activate(UserInterface $user, UserAccount $toUser)
    {
        return false;
    }
}
