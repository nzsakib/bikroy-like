<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;

class PurchasesPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
