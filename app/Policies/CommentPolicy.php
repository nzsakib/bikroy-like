<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Comment;
use App\Models\UserInterface;

class CommentPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin() && $user->hasRole('super-admin')) {
            return true;
        }

        if ($user->isAdmin() && $user->can('update comment')) {
            return true;
        }
    }

    public function destroy(UserInterface $user, Comment $comment)
    {
        return $user->id == $comment->user_id;
    }
    
    public function update(UserInterface $user, Comment $comment)
    {
        return $user->id == $comment->user_id;
    }
}
