<?php

namespace App\Policies;

use App\Models\Cart;
use App\Enum\PostStatus;
use App\Models\UserAccount;
use App\Models\UserInterface;
use Illuminate\Auth\Access\HandlesAuthorization;

class CartPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the cart.
     *
     * @param  \App\Models\UserAccount  $user
     * @param  \App\=App\Cart  $cart
     * @return mixed
     */
    public function view(UserAccount $user, Cart $cart)
    {
        //
    }

    /**
     * Determine whether the user can create carts.
     *
     * @param  \App\Models\UserAccount  $user
     * @return mixed
     */
    public function create(UserAccount $user)
    {
        //
    }

    /**
     * Determine whether the user can update the cart.
     *
     * @param  \App\Models\UserAccount  $user
     * @param  \App\=App\Cart  $cart
     * @return mixed
     */
    public function update(UserInterface $user, Cart $cart)
    {
        return $cart->user_id == $user->id && 
                $cart->post->status == PostStatus::active;
    }

    /**
     * Determine whether the user can delete the cart.
     *
     * @param  \App\Models\UserAccount  $user
     * @param  \App\=App\Cart  $cart
     * @return mixed
     */
    public function delete(UserAccount $user, Cart $cart)
    {
        //
    }

    /**
     * Determine whether the user can restore the cart.
     *
     * @param  \App\Models\UserAccount  $user
     * @param  \App\=App\Cart  $cart
     * @return mixed
     */
    public function restore(UserAccount $user, Cart $cart)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the cart.
     *
     * @param  \App\Models\UserAccount  $user
     * @param  \App\=App\Cart  $cart
     * @return mixed
     */
    public function forceDelete(UserAccount $user, Cart $cart)
    {
        //
    }
}
