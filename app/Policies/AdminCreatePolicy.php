<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\UserInterface;
use App\Models\Admin;

class AdminCreatePolicy
{
    use HandlesAuthorization;

    public function destroy(UserInterface $user, Admin $admin)
    {
        // only super admin can perform delete
        return false;
    }

    public function update(UserInterface $user, Admin $admin)
    {
        // only super admin can perform update
        return false;
    }

    public function store(UserInterface $user, Admin $admin)
    {
        // only super admin can perform create
        return false;
    }

    public function permission(UserInterface $user, Admin $admin)
    {
        // only super admin can perform on permission model
        return false;
    }
}
