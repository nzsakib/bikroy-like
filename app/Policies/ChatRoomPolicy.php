<?php

namespace App\Policies;

use App\Models\UserAccount;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\ChatRoom;
use App\Models\UserInterface;

class ChatRoomPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(UserAccount $user, ChatRoom $room)
    {
        return ($user->id == $room->buyer_id || 
                $user->id == $room->post->poster_id);
    }

    public function update(UserInterface $user, ChatRoom $room)
    {
       return ($user->id == $room->buyer_id || 
                $user->id == $room->post->poster_id);
    }

    public function destroy(UserAccount $user, ChatRoom $room)
    {
       return false;
    }

    public function show(UserInterface $user, ChatRoom $room)
    {
        return $user->id == $room->buyer_id || 
                $user->id == $room->post->poster_id;
    }
}
