<?php

namespace App\Policies;

use App\Models\UserAccount;
use App\Models\Category;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\UserInterface;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin() && $user->hasRole('super-admin')) {
            return true;
        }

        if ($user->isAdmin() && $user->can('category')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\Models\UserAccount  $user
     * @param  \App\Models\Category  $category
     * @return mixed
     */
    public function delete(UserInterface $user, Category $category)
    {
        // it any item exists, category cannot be deleted
        return !! !$category->items()->exists();
    }

}
