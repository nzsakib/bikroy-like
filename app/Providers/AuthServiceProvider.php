<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Models\Comment::class => \App\Policies\CommentPolicy::class,
        \App\Models\ItemGallery::class => \App\Policies\ImagePolicy::class,
        \App\Models\Post::class => \App\Policies\PostPolicy::class,
        \App\Models\ChatRoom::class => \App\Policies\ChatRoomPolicy::class,
        \App\Models\ChatMessage::class => \App\Policies\ChatMessagePolicy::class,
        \App\Models\Cart::class => \App\Policies\CartPolicy::class,
        \App\Models\Category::class => \App\Policies\CategoryPolicy::class,
        \App\Models\Admin::class => \App\Policies\AdminCreatePolicy::class,
        \App\Models\UserAccount::class => \App\Policies\UserAccountPolicy::class,
        \App\Models\MainCategory::class => \App\Policies\MainCategoryPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if ($user->isAdmin() && $user->hasRole('super-admin')) {
                return true;
            } elseif (!$user->isAdmin() && $user->isActive == 0) {
                return false;
            }
        });
    }
}
