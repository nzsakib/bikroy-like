<?php 

namespace App\Traits;

trait Followable
{
    public function followers()
    {
        return $this->belongsToMany(\App\Models\UserAccount::class, 'follows', 'following_id', 'follower_id')->withTimestamps();
    }

    public function followedUsers()
    {
        return $this->belongsToMany(\App\Models\UserAccount::class, 'follows', 'follower_id', 'following_id')->withTimestamps();
    }

    public function follow($user)
    {
        return $this->followedUsers()->attach($user);
    }

    public function unfollow($user)
    {
        return $this->followedUsers()->detach($user);
    }

    public function isFollowing()
    {
        return !! $this->followers()->where('follower_id', auth()->id())->exists();
    }

    public function getIsFollowingAttribute()
    {
        return $this->isFollowing();
    }

    public function getFollowingCountAttribute()
    {
        return $this->followedUsers()->count();
    }

    
    public function getFollowerCountAttribute()
    {
        return $this->followers()->count();
    }
}