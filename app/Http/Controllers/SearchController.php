<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Filters\PostFilters;
use Illuminate\Http\Request;
use App\Http\Resources\SearchResource;

class SearchController extends Controller
{
    public function index(PostFilters $filters)
    {
        // return all latest post
        $search = (string) request('search');
        
        if ( strpos($search, '#') === 0 ) {
            // hashtag is given so search only with tags
            // $search = substr($search, 1);
            $posts = Post::whereLike('item.tags.tag_name', $search)
                        ->withCount('likes', 'comments')
                        ->with('item.tags', 'item.category',
                        'item.category.mainCategory', 'item.category.subCategory')
                        ->filter($filters)
                        ->latest()
                        ->available()
                        ->active()
                        ->paginate(20);
        }
        else {

            // search without hash
            $posts = Post::whereLike(
                ['item.heading', 'poster.userProfile.name', 'poster.userProfile.username', 
                'item.category.subCategory.name', 'item.category.mainCategory.name'], $search)->
                withCount('likes', 'comments')
                ->with('item.tags', 'item.category', 'item.category.mainCategory', 'item.category.subCategory')
                ->filter($filters)
                ->latest()
                ->available()
                ->active()
                ->paginate(20);
            
        }
        // return view('posts', compact('posts'));
        return SearchResource::collection($posts);
    }
}
