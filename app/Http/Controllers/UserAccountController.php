<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\UserAccount;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Repositories\ImageRepository;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserAccountResource;

class UserAccountController extends Controller
{

    /**
     * Set or update the the firebase token of a user 
     * 
     * @param Request firebase_token
     * @return Response firebase_token
     */
    public function firebaseToken()
    {
        $validator = Validator::make(request()->all(), [
            'firebase_token' => 'required',
            'device_type' => 'sometimes|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $token = request('firebase_token');
        $user = auth()->user();
        $user->firebase_token = $token;
        $user->device_type = request('device_type');
        $user->save();

        return response([
            'status' => 'success',
            'firebase_token' => $token
        ], 200);
    }

    /**
     * Update user profile 
     * @param Request 
     * @param ImageRepository
     * @return UserAccountResource
     */
    public function update(Request $request, ImageRepository $image)
    {
        $validator = $this->validator($request);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = auth()->user();

        try {
            $this->checkUnique($request->all(), $user);
        } catch (\Exception $e) {
            return AuthResponse::validationErrString($e->getMessage());
        }

        
        if ($request->phone_number) {
            $user->phone_number = $request->phone_number;
        }

        $profile = $user->userProfile;
        $profile->username = $request->username;
        $profile->name = $request->name;
        $profile->gender = strtolower($request->gender);
        
        if ($profile->email != $request->email && !empty($request->email)) {
            $profile->email = $request->email;
            $profile->email_verified = false;
        }
        $profile->date_of_birth = Carbon::parse($request->date_of_birth);
        $profile->address = $request->address;
        $profile->about = $request->about;

        if ($request->profile_image && $request->profile_image != "") {
            $profile->profile_image = $request->profile_image;
        }

        $file = $request->image; 
        if ($file) {
            try {
                $old_image = $profile->profile_image;
                $profile->profile_image = $image->upload($file, 'profile');
                $this->deleteIfNotDefault($old_image);
            } catch (\Exception $e) {
                return AuthResponse::validationErrString($e->getMessage());
            }
        }

        $this->updateBankAccount($request, $user);
        
        $user->isComplete = true;
        $user->save();
        $profile->save();
        $user['reviews_count'] = $user->reviews()->count();

        return new UserAccountResource($user);
    }

    protected function checkUnique(array $data)
    {
        $username = UserProfile::where('username', $data['username'])
                            ->whereNotIn( 'id', [auth()->id()] )
                            ->exists();
        if ( $username ) {
            throw new \Exception(__("The username is taken."), 400);
        }
        if (isset($data['email'])) {
            $email = UserProfile::where('email', $data['email'])
                            ->whereNotIn( 'id', [auth()->id()] )
                            ->exists();
            
            if ( $email ) {
                throw new \Exception(__("The email is taken."), 400);
            }
        }

        $phone = UserAccount::where('phone_number', $data['phone_number'])
                        ->whereNotIn( 'id', [auth()->id()] )
                        ->exists();
        if ( $phone ) {
            throw new \Exception(__("The phone number is taken."), 400);
        }
    }

    protected function deleteIfNotDefault($url)
    {
        $arr = explode('/', $url);
        $filename = end($arr);

        if ($filename == 'default.png' || $filename == 'default.jpg' || $filename == 'default.jpeg') {
            return true;
        }
        $fullname = implode('/', array_slice($arr, -2, 2));
        Storage::disk('s3')->delete($fullname);

        return true;
    }

    /**
     * Validate the incoming request for updating profile
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {
        return Validator::make($request->all(), [
            'phone_number' => 'required|string|size:14',
            'username' => 'required|string',
            'name' => 'required|string',
            'address' => 'required',
            'gender' => 'required',
            'date_of_birth' => 'required',
            'email' => 'required|email',
            'profile_image' => 'sometimes|string',
            'image' => 'sometimes|file|max:2048',
            'bank_name' => 'sometimes',
            'branch_name' => 'sometimes',
            'account_name' => 'sometimes',
            'account_number' => 'sometimes'
        ]);
    }

    /**
     * Update users bank account if exists or create
     *
     * @param Request $request
     * @param UserAccount $user
     * @return BankAccount
     */
    protected function updateBankAccount(Request $request, UserAccount $user)
    {
        return $user->bank()->updateOrCreate($request->only([
            'bank_name', 'branch_name', 'account_name', 'account_number', 'routing_number'
        ]));
    }
}

