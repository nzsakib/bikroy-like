<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Enum\PostStatus;
use Illuminate\Http\Request;
use App\Http\Resources\Home\PostResource;

class FeedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();
        
        $userWithPost = $user->followedUsers->pluck('id');
        
        $posts = Post::active()
                    ->withCount(['likes', 'comments'])
                    ->with('item.tags')
                    ->whereIn('poster_id', $userWithPost)
                    ->available()
                    ->latest()
                    ->paginate(20);
        // 
        return PostResource::collection($posts);
    }
}
