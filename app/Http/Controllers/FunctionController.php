<?php

namespace App\Http\Controllers;

use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FunctionController extends Controller
{

    public function getUsername($name) {
        $firstname = explode(' ', $name)[0];
        $firstname = strtolower($firstname);
        
        $digit = rand(1, 9999);
        $username = $firstname . '.' . $digit;
        while ( $this->usernameExists($username) ) {
            $digit = rand(1, 9999);
            $username = $firstname . '.' . $digit;
        }

        return $username;
    }

    public function isValidated($request, $rules) {
        $validator = Validator::make($request->all(), $rules);

        return !$validator->fails();
    }

    public function usernameExists($username)
    {
        return UserProfile::where('username', $username)->exists();
    }
}
