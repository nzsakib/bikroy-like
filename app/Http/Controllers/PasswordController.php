<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function change(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|string|min:8',
            'new_password' => 'required|confirmed|string|min:8'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = auth()->user();
        $current = $request->current_password;
        
        if ( ! Hash::check($current, $user->password) ) {
            return AuthResponse::validationErrString(__('Current Password does not match!'));
        }

        $user->fill([
            'password' => bcrypt($request->new_password)
        ])->save();

        return response([
            'status' => 'success',
            'message' => __('Password changed successfully.')
        ]);
    }

}
