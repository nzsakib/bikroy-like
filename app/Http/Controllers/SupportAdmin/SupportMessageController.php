<?php

namespace App\Http\Controllers\SupportAdmin;

use App\Models\ChatRoom;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Models\ChatMessage;

class SupportMessageController extends Controller
{
    /**
     * Store a chat message for a chat room
     * 
     * @param Request 
     * @return 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'chat_room_id' => 'required',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $room = ChatRoom::findOrFail($request->chat_room_id);

        // if (Gate::forUser('create', $room)) {
        //     return AuthResponse::unauthorized();
        // }

        $room->chatMessages()->create([
            'message' => $request->message
        ]);

        return response([
            'status' => 'success',
            'message' => 'Message is sent.'
        ]);
    }

    /**
     * Delete an existing message 
     * 
     * @param ChatMessage Model
     * @return Json Response
     */
    public function destroy(ChatMessage $message)
    {
        // if (Gate::denies('destroy', $room)) {
        //     return AuthResponse::unauthorized();
        // }

        $message->delete();

        return response([
            'status' => 'success',
            'message' => 'Message is deleted.'
        ]);
    }
}
