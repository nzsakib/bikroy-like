<?php

namespace App\Http\Controllers\SupportAdmin;

use App\Models\ChatRoom;
use App\Models\ChatMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Chat\ChatMessageResource;

class SupportRoomController extends Controller
{
    /**
     * Create a new chat room or return an existing chatroom 
     * 
     * @param Request admin id and buyer id (auth)
     * @return Response room id
     */
    public function create()
    {
        $record = ChatRoom::where('source_id', 1)
        ->where([['buyer_id', auth()->id()], ['source_type', \App\Models\Admin::class]])
        ->first();
        if ($record) {
            return $record;
        }

        $room = ChatRoom::create([
            'source_id' => 1,
            'buyer_id' => auth()->id(),
            'source_type' => \App\Models\Admin::class,
            'archived' => false
        ]);

        return response([
            'status' => 'success',
            'chat_room_id' => $room->id
        ]);
    }

    /**
     * Show an existing chat room messages to valid
     * 
     * @param ChatRoom Model
     * @return ChatMessageResource
     */
    public function show(ChatRoom $room)
    {
        $room->load('post');
        
        $messages = ChatMessage::where('chat_room_id', $room->id)->latest()->paginate(25);
        
        return ChatMessageResource::collection($messages);
    }

    /**
     * Delete an existing chatroom 
     * 
     * @param ChatRoom Model
     * @return Json Response
     */
    public function destroy(ChatRoom $room)
    {
        // if (Gate::denies('destroy', $room)) {
        //     return AuthResponse::unauthorized();
        // }

        $room->delete();

        return response([
            'status' => 'success',
            'message' => 'Chat room is deleted.'
        ]);
    }
}
