<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Resources\TagResource;

class TagsController extends Controller
{
    /**
     * Get all tags from db as json 
     * 
     * @param null
     * @return Response tagCollection
     */
    public function index()
    {
        $tags = Tag::distinct()->select('tag_name')->get();

        $all = $tags->pluck('tag_name');

        return response($all);
    }

    /**
     * Get related hash tags from given string
     * 
     * @param string hashTag
     * @return Response tag
     */
    public function related()
    {
        $tag = request('q');
        
        $tags = Tag::search($tag, ['tag_name'])->select('tag_name')->take(10)->get();

        return response(
            $tags->pluck('tag_name')
        );
    }

    /**
     * Get top 5 tags of all time 
     * 
     * @param void 
     * @return Json Response
     */
    public function popular()
    {
        // select groupby name tag count as count order by desc limit 5
        $tags = Tag::groupBy('tag_name')->selectRaw('tag_name, count(*) as count')->orderBy('count', 'desc')->limit(5)->get();

        return response($tags->pluck('tag_name'));
    }
}
