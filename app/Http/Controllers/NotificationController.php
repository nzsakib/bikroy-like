<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Resources\NotificationCollection;
use App\Models\UserAccount;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();

        $notifications = $user->notifications()->with('fromUser.userProfile')
                            ->whereHas('fromUser', function($q) {
                                $q->where('isActive', true);
                            })
                            ->orWhere(function($q) use($user) {
                                $q->where('from_user_id', null)
                                ->where('notifiable_type', UserAccount::class)
                                ->where('notifiable_id', $user->id);
                            })
                            
                            ->latest()->paginate(25);
        
        $unreadCount = $user->unreadNotifications()
        ->whereHas('fromUser', function($q) {
            $q->where('isActive', true);
        })
        ->orWhere(function($q) use($user) {
            $q->where('from_user_id', null)
                ->where('notifiable_type', UserAccount::class)
                ->where('notifiable_id', $user->id)
                ->where('read_at', null);
        })->count();

        return new NotificationCollection($notifications, $unreadCount);
    }

    /**
     * Mark a single notification as read
     *
     * @param Request $request
     * @return Response
     */
    public function markAsRead(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'notification_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $notification = auth()->user()
                            ->notifications()
                            ->where('id', $request->notification_id)
                            ->first();
    
        if ($notification) {
            $notification->markAsRead();
        }
        return response([
            'status' => 'success',
            'message' => 'Notification marked as read.'
        ]);
    }
}
