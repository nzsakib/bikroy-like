<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Validator;
use App\Helpers\AuthResponse;
use App\Http\Resources\Home\PostResource;
use App\Models\SavedPost;

class WishesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all saved post by authenticated user
     * 
     * @param JwtToken 
     * @return Post Resource
     */
    public function index()
    {
        $wishes = auth()->user()
                    ->savedPosts()
                    ->active()
                    ->with('item', 'item.images', 'item.tags', 'item.category')
                    ->withCount(['likes', 'comments'])
                    ->paginate(10);

        return PostResource::collection($wishes);
    }

    /**
     * Save a post for authenticated user
     * 
     * @param Post
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $post = Post::findOrFail(request('post_id'));

        $post->savedPosts()->syncWithoutDetaching([auth()->id()]);
        
        return response([
            'message' => 'Post is Saved.',
            'is_saved' => true,
        ], 200);
    }

    /**
     * Delete a saved post for an authenticated user
     * 
     * @param Post
     * @return Response
     */
    public function destroy()
    {
        $validator = Validator::make(request()->all(), [
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $post = Post::findOrFail(request('post_id'));

        SavedPost::where('post_id', $post->id)->where('user_id', auth()->id())->delete();
        
        return response([
            'message' => 'Deleted Successfully.',
            'is_saved' => false
        ], 200);
    }

    
}
