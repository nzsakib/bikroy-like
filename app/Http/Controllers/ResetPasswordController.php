<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordNotification;

class ResetPasswordController extends Controller
{
    public function sendResetLinkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $email = $request->email;
        
        $user = UserProfile::where('email', $email)->first();

        if (!$user) {
            return AuthResponse::validationErrString(__('Email does not exist.'));
        }

        $reset = PasswordReset::where('user_id', $user->user_id)
                            ->where('created_at', null)->first();
        if (!$reset) {
            $token = $this->getToken($user);
            $reset = $this->saveToken($token, $user);
        }

        Mail::to($user->email)->send(new ResetPasswordNotification($reset->token, $user));

        return response([
            'status' => 'success',
            'message' => __('Reset link is sent to the email address.')
        ]);
    }

    protected function getToken($user)
    {
        $token = str_random(40);
        while (PasswordReset::where('token', $token)->exists()) {
            $token = str_random(40);
        }
        
        return $token;
    }

    protected function saveToken($token, $user)
    {
        return PasswordReset::create([
            'user_id' => $user->user_id,
            'token' => $token
        ]);
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|string',
            'password' => 'required|confirmed|string|min:8',
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $reset = PasswordReset::where('token', $request->token)->first();
        if (!$reset) {
            return AuthResponse::validationErrString(__('Invalid token given.'));
        }

        DB::beginTransaction();
        try {
            $reset->user->password = bcrypt($request->password);
            $reset->user->save();
            $reset->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return AuthResponse::validationErrString($e->getMessage());
        }

        return response([
            'status' => 'success',
            'message' => __('Password was reset successfully.')
        ]); 
    }
}
