<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\ReportPost;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use Illuminate\Support\Facades\Validator;

class PostReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Create a post report reported by a user
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'comment' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $post = Post::findOrFail($request->post_id);
        $post->reportPosts()->create([
            'reporter_id' => auth()->id(),
            'comment' => $request->comment
        ]);

        return response([
            'status' => 'success',
            'message' => __('Post has been reported.')
        ]);
    }
}
