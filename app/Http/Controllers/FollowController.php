<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\UserAccount;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Resources\FollowResource;
use App\Notifications\YouHaveBeenFollowed;

class FollowController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth')->only(['follow', 'unfollow']);
    }


    /**
     * List of user that authenticated user follows
     * 
     * @param int UserId 
     * @return Resource FollowResource
     */
    public function following($userId)
    {
        $user = UserAccount::findOrFail($userId);

        $users = $user->followedUsers()->with('userProfile')->paginate(20);

        return FollowResource::collection($users);
    }

    /**
     * authenticated user will follow the user with given id 
     * 
     * @param int UserId
     * @return Response success
     */
    public function follow(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->failed()) {
            return AuthResponse::validationError($validator);
        }

        $toFollow = UserAccount::findOrFail($request->user_id);
        $user = auth()->user();

        if ($user->is($toFollow)) {
            return AuthResponse::validationErrString(__('You cannot follow your own profile.'));
        }
        
        if ( ! $user->followedUsers()->where('following_id', $toFollow->id)->exists()
        ) {
            $user->follow($toFollow);
            $toFollow->notify(new YouHaveBeenFollowed($user));
        }

        return response([
            'status' => 'success',
            'is_following' => true,
            'message' => 'User followed'
        ]);
    }

    /**
     * authenticated user will unfollow the user with given id 
     * 
     * @param int UserId
     * @return Response success
     */
    public function unfollow(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->failed()) {
            return AuthResponse::validationError($validator);
        }

        $target = UserAccount::findOrFail($request->user_id);
        $user = auth()->user();

        $user->unfollow($target);
        $target->deleteFollowNotification($user); // TODO extract to model event later

        return response([
            'status' => 'success',
            'is_following' => false,
            'message' => 'User unfollowed'
        ]);
    }

    /**
     * List of user follower
     * 
     * @param int UserId 
     * @return Resource FollowResource
     */
    public function follower($userId)
    {
        $user = UserAccount::findOrFail($userId);

        $users = $user->followers()->with('userProfile')->paginate(20);

        return FollowResource::collection($users);
    }

}
