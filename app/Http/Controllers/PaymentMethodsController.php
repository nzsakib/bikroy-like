<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Models\PaymentMethod;
use App\Helpers\AuthResponse;

class PaymentMethodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('index');
        $this->middleware(['role_or_permission:super-admin|payment method'])->except('index');    
    }

    /**
     * Show all payment methods in the system to general user
     *
     * @return void
     */
    public function index()
    {
        $payments = PaymentMethod::all();
        $payments->transform(function($item) {
            return $item->only(['id', 'name']);
        });

        return response(
            $payments
        );
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $payment = PaymentMethod::create($request->only('name'));

        return response([
            'status' => 'success',
            'data' => $payment
        ]);
    }

    public function update(PaymentMethod $payment, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $payment->name = $request->name;
        $payment->save();

        return response([
            'status' => 'success',
            'data' => $payment
        ]);
    }

    public function destroy(PaymentMethod $payment)
    {
        if ($payment->purchase()->exists()) {
            return AuthResponse::validationErrString(__('Sorry! This payment method is being used in past purchase.'));
        }
        $payment->delete();

        return response([
            'status' => 'success',
            'message' => 'Payment deleted.'
        ]);
    }

    /**
     * Show a single payment method row details by id
     *
     * @param int $id
     * @return Response
     */
    public function show($id) 
    {
        $payment = PaymentMethod::findOrFail($id);

        return response([
            'id' => $payment->id,
            'name' => $payment->name,
        ]);
    }
}
