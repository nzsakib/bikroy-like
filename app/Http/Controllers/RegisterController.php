<?php

namespace App\Http\Controllers;

use DB;
use App\Models\SocialId;
use App\Models\UserAccount;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\Helpers\AuthResponse;

class RegisterController extends Controller
{
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Register a new user 
     * 
     * @param Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        
        if($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
            
        DB::beginTransaction();

        try {
            $user = $this->create($request->all());

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            
            return AuthResponse::validationErrString($e->getMessage());
        }

        $token = JWTAuth::fromUser($user);

        return response([
            'token' => $token,
            'expires_in' => auth()->guard('api')->factory()->getTTL()
        ]);
    }

    /**
     * Validate user data
     * 
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $message = [
            'unique' => __('validation.unique')
        ];
        return Validator::make($data, [
            'phone_number'  => 'required|string|size:14|unique:user_accounts',
            'password'      => 'required|string|min:8|confirmed',
            'email'         => 'required|email|unique:user_profiles',
            'name'          => 'required|string',
            'social_id'     => 'sometimes',
            'social_type'   => 'required_with:social_id'
        ], $message);
    }

    /**
     * Create the user account 
     * 
     * @param array $data
     * @return \App\Models\UserAccount
     */
    protected function create(array $data)
    {
        $user = UserAccount::create([
            'phone_number'  => $data['phone_number'],
            'password'      => bcrypt($data['password']),
            'isActive'      => true,
        ]);

        $email = isset($data['email']) ? $data['email'] : null;

        $userProfile = UserProfile::create([
            'user_id'       => $user->id,
            'username'      => (new FunctionController)->getUsername($data['name']),
            'name'          => $data['name'],
            'email'         => $email,
            'phone_verified' => isset($data['phone_verified']) ? 0 : 1
        ]);

        if (isset($data['social_id'])) {
            $this->createOrFailSocial($data, $user);
        }

        return $user;
    }

    /**
     * Ceate a social account for the user or fail 
     * 
     * @param array $data
     * @param App\Models\UserAccount
     * @return \App\Models\SocialId
     */
    protected function createOrFailSocial(array $data, UserAccount $user)
    {
        $attributes = [
            'social_id' => $data['social_id'], 
            'social_type' => $data['social_type']
        ];

        if ( SocialId::where($attributes)->exists() ) {
            throw new \Exception(__('message.socialExists'), 406);
        }

        return $user->socialAccounts()->create([
            'social_id' => $data['social_id'],
            'social_type' => $data['social_type']
        ]);
    }

    /**
     * Check if given email or phone number exists in the database
     *
     * @param Request $request
     * @return Response
     */
    public function checkIfExists(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number'  => 'sometimes|string|size:14|unique:user_accounts',
            'email' => 'required_without:phone_number|email|unique:user_profiles',
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        return response([
            'status' => 'success',
            'message' => 'Given field is unique'
        ]);
    }
}