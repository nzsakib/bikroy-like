<?php

namespace App\Http\Controllers;

use App\Models\UserAccount;
use Illuminate\Http\Request;
use App\Http\Resources\Home\PostResource;
use App\Http\Resources\UserAccountResource;
use App\Helpers\AuthResponse;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'likedPost', 'allPost', 'showByUsername']);
    }
    /**
     * Show all post of an authenticated user
     * 
     * @param Token auth
     * @return Response Post
     */
    public function index()
    {
        $user = auth()->user();

        $posts = $user->posts()
                    ->withCount(['likes', 'comments'])
                    ->with('item.tags')
                    ->latest()
                    ->paginate(20);

        return PostResource::collection($posts);
    }

    /**
     * Get user profile info by user id
     * 
     * @param int userId
     * @return Response 
     */
    public function show($id)
    {
        $user = UserAccount::active()->withCount('reviews')->findOrFail($id);

        UserAccountResource::withoutWrapping();
        return new UserAccountResource($user);
    }

    /**
     * Get user profile info by username
     *
     * @param string $username
     * @return UserAccountResource
     */
    public function showByUsername($username)
    {
        $user = UserAccount::whereHas('userProfile', function ($query) use($username) {
            $query->where('username', $username);
        })->withCount('reviews')->with('bank')->firstOrFail();

        UserAccountResource::withoutWrapping();
        return new UserAccountResource($user);
    }

    /**
     * Get all liked post of a user
     * 
     * @param int userId
     * @return Response
     */
    public function likedPost($userId)
    {
        $user = UserAccount::findOrFail($userId);

        $posts = $user->likes()
                    ->withCount(['likes', 'comments'])
                    ->available()
                    ->latest()
                    ->active()
                    ->paginate(20);
        //
        return PostResource::collection($posts);
    }

    /**
     * Get all post of a user with id
     * 
     * @param int userId
     * @return Response Post
     */
    public function allPost($userId)
    {
        $user = UserAccount::findOrFail($userId);

        $posts = $user->posts()
                    ->withCount(['likes', 'comments'])
                    ->active()
                    ->latest()
                    ->paginate(20);

        return PostResource::collection($posts);
    }
}
