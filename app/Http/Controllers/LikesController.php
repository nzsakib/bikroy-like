<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Post;
use App\Helpers\AuthResponse;
use App\Http\Resources\LikeResource;
use App\Notifications\YourPostIsLiked;
use App\Models\Like;

class LikesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }
    /**
     * Store like for a single post
     * 
     * @param Post
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $post = Post::findOrFail(request('post_id'));

        $user = auth()->user();

        if ( ! $post->likes()->where('user_id', $user->id)->exists() ) {
            $post->likes()->syncWithoutDetaching([$user->id]);
            if ( !$user->is($post->poster) ) {
                $post->poster->notify(new YourPostIsLiked($user, $post));
            }
        }
        
        return response([
            'status' => 'success',
            'is_liked' => true,
            'message' => 'operation successful'
        ], 200);
    }

    /**
     * Unlike a liked post
     * 
     * @param Post
     * @return Response
     */
    public function destroy()
    {
        $validator = Validator::make(request()->all(), [
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $post = Post::findOrFail(request('post_id'));

        Like::where('post_id', $post->id)->where('user_id', auth()->id())->delete();
        $post->poster->deleteLikeNotification($post); // TODO Extract to model event later

        return response([
            'status' => 'success',
            'is_liked' => false,
            'message' => 'operation successful'
        ], 200);
    }

    /**
     * Show all users that liked a single post
     * 
     * @param Model Post
     * @return Response Users
     */
    public function show(Post $post)
    {
        $users = $post->likes()->active()->latest()->with('userProfile')->paginate(30);
        
        return LikeResource::collection($users);
    }
}
