<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserAccount;
use Illuminate\Support\Facades\Response;
use App\Http\Resources\Admin\UserAnalyticsResource;
use App\Models\Purchase;
use Carbon\Carbon;

class AnalyticsController extends Controller
{
    //get top user analytics
    public function topUserAnalytics()
    {
        $users = UserAccount::with('posts.item.itemCounter')->get();
        $users = $this->userAnalyticsData($users);
        $users = $users->sortByDesc('total_sold')->take(10);
        return UserAnalyticsResource::collection($users);
    }

    //get total user analytics
    public function userAnalytics()
    {
        $users = UserAccount::with('posts.item.itemCounter')->paginate(20);
        $users = $this->userAnalyticsData($users);

        return UserAnalyticsResource::collection($users);
    }

    //get user analytics data
    public function userAnalyticsData($users)
    {
        $i=0;
        foreach($users as $user){
            $last_post = $user->posts->sortByDesc('created_at')->first();
            if(count($user->posts) > 0) {
                $count = $sold = 0;
                foreach($user->posts as $post){
                    $count += $post->item->itemCounter->count;
                    $sold += $post->item->itemCounter->sold;
                }
                $users[$i]->last_activity = $last_post->created_at;
                $users[$i]->total_count = $count;
                $users[$i]->total_sold = $sold;
                $users[$i]->sells_percentage = round(($sold / $count) * 100);
            }
            $i++;
        }
        return $users;
    }

    //get sells analytics
    public function sellsAnalytics()
    {
        $purchase = Purchase::countOfLastSevenDays();
        
        $this->calculatePercentage($purchase);

        return Response::json($purchase);
    }
    
    /**
     * Calculate weekly percentage of the purchased post
     *
     * @param Purchase $purchase
     * @return Collection
     */
    protected function calculatePercentage($purchase)
    {
        $max = $purchase->max('sold');
        
        return $purchase->map(function($post) use($max) {
            $percent = $post->sold / $max * 100;
            $post['day'] = strtoupper($post->created_at->isoFormat('dd')); 
            $post['percentage'] = (int) $percent;
        });
    }
}
