<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Models\UserAccount;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Http\Resources\Admin\RoleResource;

class RolesController extends Controller
{
    

    public function index()
    {
        $roles = Role::with('permissions')
                    ->where('name', '<>', 'super-admin')
                    ->get();

        return RoleResource::collection($roles);
    }

    public function store(request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:roles',
            'permissions' => 'required|array|min:1',
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        foreach ($request->permissions as $p) {
            if ($p === null) {
                return response([
                    'error' => 'Permission is required'
                ], 400);
            }
        }
        DB::transaction(function() use($request) {
            $role = Role::create(['name' => $request->name]);
            $role->syncPermissions([$request->permissions]);
        }); 

        return response([
            'status' => 'success',
            'message' => 'Role is created'
        ]);
    }

    public function update(Role $role, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:roles',
            'permissions' => 'required|array|min:1',
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        foreach ($request->permissions as $p) {
            if ($p === null) {
                return response([
                    'error' => 'Permission is required'
                ], 400);
            }
        }
        DB::transaction(function() use($role, $request) {
            $role->name = $request->name;
            $role->save();
            $role->syncPermissions([$request->permissions]);
        });

        return response([
            'status' => 'success',
            'message' => 'Role updated'
        ]);
    }

    public function assignToAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role_id' => 'required|exists:roles,id',
            'user_id' => 'required|exists:admins,id'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $admin = Admin::findOrFail($request->user_id);
        $role = Role::findOrFail($request->role_id);

        $admin->syncRoles($role);

        return response([
            'status' => 'success',
            'message' => 'Role is assigned'
        ]);  
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return response([
            'status' => 'success',
            'message' => 'Deleted roles.'
        ]);
    }

    public function show(Role $role)
    {
        $role->load('permissions');
        RoleResource::withoutWrapping();
        return new RoleResource($role);
    }
}
