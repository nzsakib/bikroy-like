<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AuthResponse;
use App\Http\Resources\Admin\DeliveryTypeResource;
use App\Models\DeliveryType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DeliveryTypeController extends Controller
{
    public function __construct() 
    {
        $this->middleware(['role_or_permission:super-admin|delivery type'])->except('index');
    }

    /**
     * Show all delivery types to all users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $delivery_type = DeliveryType::all();
        DeliveryTypeResource::withoutWrapping();
        return DeliveryTypeResource::collection($delivery_type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'delivery_scope' => 'required|string',
            'cost' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $delivery = DeliveryType::create($request->only(['name', 'delivery_scope', 'cost']));

        DeliveryTypeResource::withoutWrapping();
        return new DeliveryTypeResource($delivery);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $delivery_type = DeliveryType::findOrFail($id);
        DeliveryTypeResource::withoutWrapping();
        return new DeliveryTypeResource($delivery_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return DeliveryTypeResource
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'delivery_scope' => 'required|string',
            'cost' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $delivery = DeliveryType::findOrFail($id);
        $delivery->update($request->only(['name', 'delivery_scope', 'cost']));

        DeliveryTypeResource::withoutWrapping();
        return new DeliveryTypeResource($delivery);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryType $deliveryType)
    {
        if ($deliveryType->purchase()->exists()) {
            return AuthResponse::validationErrString('Can not delete. Existing product was purchased using this delivery.');
        }
        $deliveryType->delete();
        return response([
            'status' => 'success',
            'message' => 'Delivery type deleted'
        ]);
    }
}
