<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\MainCategory;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;

class MainCategoryController extends Controller
{
    protected $image;

    public function __construct(ImageRepository $image)
    {
        $this->image = $image;
        $this->middleware(['role_or_permission:super-admin|category']);
    }

    /**
     * Show all main category 
     * 
     * @param void 
     * @return Response
     */
    public function index()
    {
        $categories = MainCategory::all();

        $cat = $categories->map(function($category) {
            return $category->only(['id', 'name', 'image']);
        });

        return response([
            'data' => $cat
        ]);
    }

    /**
     * Create a new Main category 
     * 
     * @param Request
     * @return Json response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'sometimes|file|max:2048'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $cat = new MainCategory();
        if ($request->image) {
            $url = $this->image->upload($request->image, 'category');
            $cat->image = $url;
        }

        $cat->name = $request->name;
        $cat->save();

        return response([
            'status' => 'success',
            'data' => $cat
        ]);
    }

    /**
     * Delete a sub category 
     * 
     * @param SubCategory
     * @return Json response
     */
    public function destroy(MainCategory $maincategory)
    {
        if ($maincategory->subCategory()->exists()) {
            return AuthResponse::validationErrString('Delete category relations first for this main category.');
        }
        
        ImageRepository::deleteIfNotDefault($maincategory->image);
        
        $maincategory->delete();

        return response([
            'status' => 'success',
            'message' => 'Main category deleted'
        ]);
    }

    /**
     * Update a main category 
     * 
     * @param SubCategory
     * @param Request
     * @return Json response
     */
    public function update(MainCategory $maincategory, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'sometimes|file|max:2048'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if ($request->image) {
            ImageRepository::deleteIfNotDefault($maincategory->image);
            $maincategory->image = $this->image->upload($request->image, 'category');
        }

        $maincategory->name = $request->name;
        $maincategory->save();

        return response([
            'status' => 'success',
            'data' => $maincategory
        ]);
    }

    public function show(MainCategory $maincategory)
    {
        return $maincategory->only(['id', 'name', 'image']);
    }
}
