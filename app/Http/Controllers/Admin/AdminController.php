<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Mail\AdminWasCreated;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\Admin\AdminResource;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{

    /**
     * Create a new admin account
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:admins',
            'role' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if (Gate::denies('store', new Admin)) {
            return AuthResponse::unauthorized();
        }
        
        DB::transaction(function() use($request) {
            $password = str_random(8);
            $admin = Admin::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($password),
            ]);
    
            $role = Role::findOrfail($request->role);
    
            $admin->syncRoles($role);
            
            Mail::to($admin)->send(new AdminWasCreated($admin, $password));
        });

        return response([
            'status' => 'success',
            'message' => 'Admin was created and password is sent to user by email.'
        ]);
    }

    /**
     * List all admins in the system
     */
    public function index()
    {
        $admins = Admin::with('roles')->get();
        
        return AdminResource::collection($admins);
    }

    /**
     * Update an admin using admin user id
     *
     * @param Request $request
     * @return response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            'user_id' => 'required',
            'password' => 'sometimes|string|min:8'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if (Gate::denies('update', new Admin)) {
            return AuthResponse::unauthorized();
        }

        $admin = Admin::findOrFail($request->user_id);
        $role = Role::findOrfail($request->role);

        $admin->name = $request->name;
        $admin->email = $request->email;
        if ($request->password) {
            $admin->password = bcrypt($request->password);
        } 
        $admin->save();
        $admin->syncRoles($role);
        return response([
            'status' => 'success',
            'message' => 'Admin was updated.'
        ]);
    }

    /**
     * Display a single admin user info by admin user id
     *
     * @param Admin $admin
     * @return AdminResource
     */
    public function show(Admin $admin)
    {
        $admin = $admin->load('roles');
        AdminResource::withoutWrapping();
        return new AdminResource($admin);
    }

    /**
     * Delete an admin account
     *
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $admin = Admin::findorFail($request->user_id);
        if (Gate::denies('destroy', $admin) || $admin->id == auth()->id()) {
            return AuthResponse::unauthorized();
        }
        
        $admin->delete();

        return response([
            'status' => 'success',
            'message' => 'Admin deleted successfully'
        ]);
    }
}
