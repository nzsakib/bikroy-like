<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Models\Post;
use App\Enum\PostStatus;
use App\Models\UserAccount;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Notifications\YourPostIsApproved;
use App\Http\Resources\Admin\PostResource;
use App\Http\Resources\Home\PostResource as HomePostResource;

class PostController extends Controller
{
    /**
     * Get all posts including pending, active, sold in latest order
     * 
     * @param void
     * @return Resource PostResource
     */
    public function index()
    {
        $posts = Post::paginate(20);
        if (Gate::denies('index', $posts)) {
            return AuthResponse::unauthorized();
        }
        return PostResource::collection($posts);
    }

    //get new post
    public function newPost()
    {
        $posts = Post::where('status', PostStatus::pending)->latest()->paginate(20);

        if (Gate::denies('index', $posts)) {
            return AuthResponse::unauthorized();
        }

        return PostResource::collection($posts);
    }

    //get new active
    public function activePost()
    {
        $posts = Post::where('status', PostStatus::active)->latest()->paginate(20);
        if (Gate::denies('index', $posts)) {
            return AuthResponse::unauthorized();
        }
        return PostResource::collection($posts);
    }

    //get new sold
    public function soldPost()
    {
        $posts = Post::where('status', PostStatus::sold)->latest()->paginate(20);
        if (Gate::denies('index', $posts)) {
            return AuthResponse::unauthorized();
        }
        return PostResource::collection($posts);
    }

    //make post active
    public function makePostActive(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required'
        ]);

        $post = Post::findOrFail($request->post_id);
        if (Gate::denies('makeActive', $post)) {
            return AuthResponse::unauthorized();
        }

        DB::beginTransaction();
        try {
            $post->status = PostStatus::active;
            $post->save();
            $post->poster->notify(new YourPostIsApproved($post));
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return AuthResponse::validationErrString('Post is not approved, please try again.');
        }

        PostResource::withoutWrapping();
        return new PostResource($post);
    }

    /**
     * Get all post of a user by user id
     *
     * @param int $user_id
     * @return 
     */
    public function userPosts(UserAccount $user)
    {
        $posts = $user->posts()
            ->withCount(['likes', 'comments'])
            ->latest()
            ->paginate(20);
        if (Gate::denies('index', $posts)) {
            return AuthResponse::unauthorized();
        }
        return HomePostResource::collection($posts);
    }

    /**
     * Block an active posts which will not be shown to any user
     *
     * @param Request $request
     * @return void
     */
    public function blockPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required|exists:posts,id'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        DB::beginTransaction();
        try {
            $post = Post::findOrFail($request->post_id);
            $post->status = PostStatus::block;
            $post->save();
            $post->carts()->notPurchased()->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return AuthResponse::validationErrString($e->getMessage(), 400);
        }

        return response([
            'status' => 'success',
            'message' => 'Successfully blocked the post.'
        ]);
    }

    /**
     * Get all blocked posts
     *
     * @return void
     */
    public function getBlockedPost()
    {
        $posts = Post::blocked()->latest('updated_at')->paginate(20);
        if (Gate::denies('index', $posts)) {
            return AuthResponse::unauthorized();
        }
        return PostResource::collection($posts);
    }

    /**
     * Get all featured posts
     *
     * @return void
     */
    public function getFeaturedPost()
    {
        $posts = Post::featured()->latest('updated_at')->paginate(20);
        if (Gate::denies('featured', $posts)) {
            return AuthResponse::unauthorized();
        }
        return PostResource::collection($posts);
    }

    /**
     * make post featured
     *
     * @return void
     */
    public function makeFeaturedPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required|exists:posts,id',
            'featured' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $post = Post::findOrFail($request->post_id);
        $post->featured = $request->featured;
        $post->save();

        if (Gate::denies('featured', $post)) {
            return AuthResponse::unauthorized();
        }
        PostResource::withoutWrapping();
        return new PostResource($post);
    }
}
