<?php

namespace App\Http\Controllers\Admin;

use App\Models\Purchase;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Mail\PurchaseInvoice;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PurchaseController extends Controller
{
    /**
     * Make the status of a purchase delivered
     *
     * @param Request $request
     * @return void
     */
    public function deliver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'purchase_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $purchase = Purchase::findOrFail($request->purchase_id);
        $purchase->delivered = true;
        $purchase->save();

        Mail::to($purchase->buyer->userProfile->email)->send(new PurchaseInvoice($purchase));

        return response([
            'status' => 'success',
            'message' => 'The product has been delivered'
        ]);
    }
}
