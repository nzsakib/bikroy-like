<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin;
use App\Http\Resources\Admin\PermissionResource;

class PermissionController extends Controller
{
    public function __construct()
    {
        // $this->middleware('role:super-admin');    
        $this->middleware(['role_or_permission:super-admin|permission']);    
    }

    public function index()
    {
        PermissionResource::withoutWrapping();
        return PermissionResource::collection(Permission::all());
    }

    /**
     * Admin can create a new permission
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if (Gate::denies('permission', new Admin)) {
            return AuthResponse::unauthorized();
        }

        $permission = Permission::create(['name' => $request->name]);

        return response([
            'status' => 'success',
            'message' => 'Permission is created'
        ]);
    }

    /**
     * Super admin can update a single permission
     *
     * @param Permission $permission
     * @param Request $request
     * @return Response
     */
    public function update(Permission $permission, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if (Gate::denies('permission', new Admin)) {
            return AuthResponse::unauthorized();
        }

        $permission->name = $request->name;
        $permission->save();

        return response([
            'status' => 'success',
            'message' => 'Permission updated'
        ]);
    }

    public function destroy(Permission $permission)
    {
        if (Gate::denies('permission', new Admin)) {
            return AuthResponse::unauthorized();
        }
        
        $permission->delete();

        return response([
            'status' => 'success',
            'message' => 'Deleted permissions.'
        ]);
    }

    /**
     * Assign many permissions to a role at once
     * will remove permission that is not given in the array
     *
     * @param Request $request
     * @return Response
     */
    public function assignToRole(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role_id' => 'required',
            'permissions' => 'sometimes|array'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if (Gate::denies('permission', new Admin)) {
            return AuthResponse::unauthorized();
        }

        $role = Role::findOrFail($request->role_id);
        $role->syncPermissions([$request->permissions]);

        return response([
            'status' => 'success',
            'message' => 'Permission is assigned to role ' . $role->name
        ]);  
    }
}
