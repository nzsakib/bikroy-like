<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\Purchase;
use App\Models\SocialId;
use App\Models\UserAccount;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CountController extends Controller
{

    /**
     * Get all user count in the database
     * 
     * @param Void
     * @return int UserAccount count
     */
    public function userCount()
    {
        $count = UserAccount::count();

        return response([
            'userCount' => $count
        ]);
    }

    /**
     * Get all post count in the database
     * 
     * @param Void
     * @return int Post Count
     */
    public function postCount()
    {
        $count = Post::count();

        return response([
            'postCount' => (int) $count
        ]);
    }

    /**
     * Get all purchased post count from DB
     * 
     * @param void 
     * @return int Purchase count 
     */
    public function transactionCount()
    {
        $count = Purchase::count();

        return response([
            'transactionCount' => $count
        ]);
    }

    /**
     * Count number of signup with Facebook 
     * 
     * @param void 
     * @return int SocialId count 
     */
    public function fbCount()
    {
        $count = SocialId::where('social_type', 'facebook')->count();

        return response([
            'facebookCount' => $count
        ]);
    }

    /**
     * Count number of signup with Google 
     * 
     * @param void 
     * @return int SocialId count
     */
    public function googleCount()
    {
        $count = SocialId::where('social_type', 'google')->count();

        return response([
            'googleCount' => $count
        ]);
    }

    /**
     * Total number of gender signed up
     * 
     * @param void 
     * @return int Response 
     */
    public function genderCount()
    {
        $totalUsers = UserProfile::count();
        $male = UserProfile::where('gender', 'male')->count();
        $male_percentage = round(($male / $totalUsers) * 100);
        $female = UserProfile::where('gender', 'female')->count();
        $female_percentage = round(($female / $totalUsers) * 100);
        $unknown = UserProfile::where('gender', null)->count();

        return response([
            'male' => $male,
            'female' => $female,
            'unknown' => $unknown,
            'male_percentage' => (int) $male_percentage,
            'female_percentage' => (int) $female_percentage
        ]);
    }

    /**
     * New signup of current month 
     * 
     * @param void
     * @return int UserAccount count 
     */
    public function newSignup()
    {
        $count = UserAccount::whereMonth('created_at', Carbon::now()->month)->count();

        return response([
            'newSignup' => $count
        ]);
    }

    /**
     * All count api in one api call
     *
     * @return Response
     */
    public function allCount()
    {
        $user = UserAccount::count();
        $post = Post::count();
        $purchase = Purchase::count();
        $fb = SocialId::where('social_type', 'facebook')->count();
        $google = SocialId::where('social_type', 'google')->count();
        
        $male = UserProfile::where('gender', 'male')->count();
        if ($user == 0) {
            $male_percentage = 0;
        } 
        else {
            $male_percentage = round(($male / $user) * 100);
        }
        $female = UserProfile::where('gender', 'female')->count();
        if ($female == 0) {
            $female_percentage = 0;
        }
        else {
            $female_percentage = round(($female / $user) * 100);
        }
        $unknown = UserProfile::where('gender', null)->count();
        if ($unknown == 0) {
            $unknown_percentage = 0;
        }
        else {
            $unknown_percentage = round(($unknown / $user) * 100);
        }
        $newSignUp = UserAccount::whereMonth('created_at', Carbon::now()->month)->count();

        return response([
            'userCount' => $user,
            'postCount' => (int) $post,
            'transactionCount' => $purchase,
            'facebookCount' => $fb, 
            'googleCount' => $google,
            'male' => $male,
            'male_percentage' => (int) $male_percentage,
            'female' => $female,
            'female_percentage' => (int) $female_percentage,
            'unknown' => $unknown,
            'unknown_percentage' => (int) $unknown_percentage,
            'newSignUp' => $newSignUp,
        ]);
    }
}
