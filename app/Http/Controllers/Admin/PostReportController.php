<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AuthResponse;
use App\Http\Resources\Admin\PostReportResource;
use App\Models\ReportPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PostReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = ReportPost::latest()->with('user.userProfile')->get();

        return PostReportResource::collection($reports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'user_id' => 'required',
            'comment' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $user_id = $request->user_id;
        $post_id = $request->post_id;
        $comment = $request->comment;

        $post_report = new ReportPost();
        $post_report->reporter_id = $user_id;
        $post_report->post_id = $post_id;
        $post_report->comment = $comment;
        $post_report->save();

        return response([
            'status' => 'success',
            'message' => 'Post has been reported'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = ReportPost::with('user.userProfile')->findOrFail($id);
        PostReportResource::withoutWrapping();
        return new PostReportResource($report);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'user_id' => 'required',
            'comment' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $user_id = $request->user_id;
        $post_id = $request->post_id;
        $comment = $request->comment;

        $post_report = ReportPost::findOrFail($id);
        $post_report->reporter_id = $user_id;
        $post_report->post_id = $post_id;
        $post_report->comment = $comment;
        $post_report->status = $request->status;
        $post_report->save();

        return response([
            'status' => 'success',
            'message' => 'Post report updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportPost $post_report)
    {
        $post_report->delete();
        return response([
            'status' => 'success',
            'message' => 'Post report deleted'
        ]);
    }

    /**
     * Resolve post report
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resolvePostReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'report_id' => 'required'
        ]);
        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $id = $request->report_id;
        
        $post_report = ReportPost::findOrFail($id);
        $post_report->status = 1;
        $post_report->save();

        return response([
            'status' => 'success',
            'message' => 'Post report resolved'
        ]);
    }
    /**
     * Make post report pending
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pendingPostReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'report_id' => 'required'
        ]);
        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $id = $request->report_id;
        
        $post_report = ReportPost::findOrFail($id);
        $post_report->status = 0;
        $post_report->save();

        return response([
            'status' => 'success',
            'message' => 'Post report pending'
        ]);
    }
}
