<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AuthResponse;
use App\Http\Resources\Admin\UserAccountResource;
use App\Models\UserAccount;
use App\Models\UserProfile;
use App\Repositories\ImageRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Gate;
use App\Models\Post;

class UserController extends Controller
{
    //get all users list
    public function allUsers()
    {
        $users = UserAccount::with('userProfile')->latest('id')->paginate(20);
        if (Gate::denies('index', $users)) {
            return AuthResponse::unauthorized();
        }
        UserAccountResource::withoutWrapping();
        return UserAccountResource::collection($users);
    }

    //update user details
    public function updateUser(Request $request, ImageRepository $image)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|string',
            'username' => 'required|string',
            'name' => 'required|string',
            'email' => 'sometimes|email',
            'profile_image' => 'sometimes|string',
            'image' => 'sometimes|file|max:2048',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $user_id = $request->user_id;
        $user = UserAccount::findOrFail($user_id);
        if (Gate::denies('update', $user)) {
            return AuthResponse::unauthorized();
        }

        try {
            $this->checkUnique($request->all(), $user);
        } catch (\Exception $e) {
            return AuthResponse::validationErrString($e->getMessage());
        }
        //update account table
        $user->phone_number = $request->phone_number;
        $user->password = bcrypt($request->password);
        $user->isActive = $request->isActive;
        $user->save();

        //update profile table
        $profile = $user->userProfile;
        $profile->username = $request->username;
        $profile->name = $request->name;
        $profile->gender = strtolower($request->gender);
        $profile->email = $request->email;
        $profile->email_verified = $request->email_verified;
        $profile->phone_verified = $request->phone_verified;
        $profile->date_of_birth = Carbon::parse($request->date_of_birth);
        $profile->address = $request->address;
        $profile->about = $request->about;
        if ($request->profile_image && $request->profile_image != "") {
            $profile->profile_image = $request->profile_image;
        }
        $file = $request->image;
        if ($file) {
            try {
                $old_image = $profile->profile_image;
                $profile->profile_image = $image->upload($file, 'profile');
                $image::deleteIfNotDefault($old_image);
            } catch (\Exception $e) {
                return AuthResponse::validationErrString($e->getMessage());
            }
        }
        $profile->save();
        return new UserAccountResource($user);
    }

    //delete user
    public function deleteUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user_id = $request->user_id;
        $user = UserAccount::findOrFail($user_id);
        if (Gate::denies('destroy', $user)) {
            return AuthResponse::unauthorized();
        }
        $user->delete();

        return response([
            'status' => 'success',
            'message' => 'User Deleted'
        ]);
    }

    protected function checkUnique(array $data, $user)
    {
        $username = UserProfile::where('username', $data['username'])
            ->whereNotIn( 'id', [$user->id] )
            ->exists();
        if ( $username ) {
            throw new \Exception("The username is taken.", 400);
        }

        if (isset($data['email'])) {
            $email = UserProfile::where('email', $data['email'])
                ->whereNotIn( 'id', [$user->id] )
                ->exists();

            if ( $email ) {
                throw new \Exception("The email is taken.", 400);
            }
        }

        $phone = UserAccount::where('phone_number', $data['phone_number'])
            ->whereNotIn( 'id', [$user->id] )
            ->exists();
        if ( $phone ) {
            throw new \Exception("The phone number is taken.", 400);
        }
    }

    /**
     * Ban an active user 
     * 
     * @param Request
     * @return Json
     */
    public function banUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = UserAccount::findOrFail($request->user_id);
        if (Gate::denies('ban', $user)) {
            return AuthResponse::unauthorized();
        }
        $user->isActive = 0;
        $user->save();
        $user->posts->each(function($post) {
            $post->carts()->notPurchased()->delete();
        });

        $user->posts()->active()->update(['status' => Post::STATUS_USER_BANNED]);
        
        return response([
            'status' => 'success',
            'message' => 'User is banned'
        ]);
    }

    /**
     * Make a user active
     *
     * @param Request
     * @return Json
     */
    public function activateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = UserAccount::findOrFail($request->user_id);
        if (Gate::denies('activate', $user)) {
            return AuthResponse::unauthorized();
        }
        $user->isActive = 1;
        $user->save();
        $user->posts()->userBanned()->update(['status' => Post::STATUS_ACTIVE]);

        return response([
            'status' => 'success',
            'message' => 'User is activated'
        ]);
    }

}
