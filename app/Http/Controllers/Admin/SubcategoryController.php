<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Repositories\ImageRepository;

class SubcategoryController extends Controller
{
    protected $image;

    public function __construct(ImageRepository $image)
    {
        $this->image = $image;
        $this->middleware(['role_or_permission:super-admin|category']);
    }

    /**
     * Show all sub category 
     * 
     * @param void 
     * @return SubCategory collection
     */
    public function index()
    {
        $categories = SubCategory::all();

        $cat = $categories->map(function($category) {
            return $category->only(['id', 'name', 'image']);
        });

        return response([
            'data' => $cat
        ]);
    }

    /**
     * Create a new sub category 
     * 
     * @param Request
     * @return Json response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'sometimes|file|max:2048'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $cat = new SubCategory();
        if ($request->image) {
            $url = $this->image->upload($request->image, 'category');
            $cat->image = $url;
        }
        
        $cat->name = $request->name;
        $cat->save();

        return response([
            'status' => 'success',
            'data' => $cat
        ]);
    }

    /**
     * Delete a sub category 
     * 
     * @param SubCategory
     * @return Json response
     */
    public function destroy(SubCategory $subcategory)
    {
        if ($subcategory->mainCategory()->exists()) {
            return AuthResponse::validationErrString('Delete category relations first for this sub category.');
        }
        
        ImageRepository::deleteIfNotDefault($subcategory->image);

        $subcategory->delete();

        return response([
            'status' => 'success',
            'message' => 'Sub category deleted'
        ]);
    }

    /**
     * Update a sub category 
     * 
     * @param SubCategory
     * @param Request
     * @return Json response
     */
    public function update(SubCategory $subcategory, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'sometimes|file|max:2048',
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if ($request->image) {
            ImageRepository::deleteIfNotDefault($subcategory->image);
            $subcategory->image = $this->image->upload($request->image, 'category');
        }

        $subcategory->name = $request->name;
        $subcategory->save();

        return response([
            'status' => 'success',
            'data' => $subcategory
        ]);
    }

    public function show(SubCategory $subcategory)
    {
        return $subcategory->only(['id', 'name', 'image']);
    }


}
