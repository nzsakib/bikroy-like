<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Helpers\AuthResponse;
use App\Http\Resources\Admin\AdminResource;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('authenticate');
    }

    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        
        $token = auth()->guard('admin')
                        ->attempt($request->only('email', 'password')) ;

        if (!$token) {
            return AuthResponse::validationErrString('Incorrect Credentials.', 401);
        } 

        return response([
            'token' => $token,
            'expires_in' => auth()->factory()->getTTL()
        ]);
    }

    /**
     * Get admin details from token
     *  
     * @param Void
     * @return Model Admin
     */
    public function getAdminDetails()
    {
        $admin = auth()->user();

        $admin['role'] = $admin->roles()->first()->name;
        unset($admin['updated_at']);
        // return $admin;
        AdminResource::withoutWrapping();
        return new AdminResource($admin);
    }

    /**
     * Refresh admin JWT token 
     * 
     * @param Void
     * @return Token
     */
    public function refreshToken()
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::refresh($token);

        return response([
            'token' => $token,
            'expires_in' => auth()->guard('api')->factory()->getTTL()
        ]);
    }
}
