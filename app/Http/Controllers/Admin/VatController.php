<?php

namespace App\Http\Controllers\Admin;

use App\Models\Vat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class VatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vat = Vat::first();
        return Response::json($vat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $percentage = $request->percentage;
        $vat = new Vat();
        $vat->percentage = $percentage;
        $vat->save();

        return Response::json($vat);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vat = Vat::findOrFail($id);
        return Response::json($vat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $percentage = $request->percentage;
        $vat = Vat::findOrFail($id);
        $vat->percentage = $percentage;
        $vat->save();

        return Response::json($vat);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vat $vat)
    {
        $vat->delete();
        return response([
            'status' => 'success',
            'message' => 'Vat deleted'
        ]);
    }
}
