<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\Purchase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Transaction\TransactionResource;
use App\Http\Resources\Admin\Transaction\SingleTransactionResource;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware(['role_or_permission:super-admin|transaction']);
    }
    
    /**
     * Get all transaction from purchases table using transaction id
     *
     * @param void
     * @return TransactionResource
     */
    public function index()
    {
        $transactions = Purchase::with('buyer')
                        ->groupBy('transaction_id')
                        ->select(['id', 'transaction_id', 'created_at', 'cart_id', 'delivered'])
                        ->selectRaw('sum(price) as total, count(*) as count')
                        ->latest()
                        ->paginate(30);
        
        return TransactionResource::collection($transactions);
    }
    
    /**
     * Show transaction details by transaction id
     *
     * @param string $transaction_id
     * @return SingleTransactionResource
     */
    public function show($transaction_id)
    {
        $purchases = Purchase::with('post')->where('transaction_id', $transaction_id)->latest()->paginate(30);
        
        return SingleTransactionResource::collection($purchases);
    }

    /**
     * show all transactions details by post id
     *
     * @param int $postId
     * @return SingleTransactionResource
     */
    public function showByPost($postId)
    {
        $post = Post::findorfail((int) $postId);
        
        $purchases = Purchase::with('cart', 'buyer')->whereHas('cart', function($query) use($postId) {
            $query->where('post_id', $postId);
        })->paginate(30);

        return SingleTransactionResource::collection($purchases);
    }
}
