<?php

namespace App\Http\Controllers\Admin;

use Gate;
use Validator;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\CategoryResource;

class CategoryController extends Controller
{
    public function __construct() 
    {
        $this->middleware(['role_or_permission:super-admin|category']);
    }
    
    /**
     * Show all Relation category 
     * 
     * @param void 
     * @return SubCategory collection
     */
    public function index()
    {
        $categories = Category::with('mainCategory', 'subCategory')->OrderBy('main_category_id', 'asc')->get();

        return CategoryResource::collection($categories);
    }

    /**
     * Create a new Relation category 
     * 
     * @param Request
     * @return Json response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'main_category_id' => 'required',
            'sub_category_id' => 'sometimes'
        ]);
        
        $sub = $request->sub_category_id;
        $main = $request->main_category_id;

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        try {
            $this->checkUnique($request->all());
        } catch(\Exception $e) {
            return AuthResponse::validationErrString($e->getMessage());
        }

        $cat = Category::create([
            'main_category_id' => $main,
            'sub_category_id' => $sub
        ]);
        
        return response([
            'status' => 'success',
            'data' => new CategoryResource($cat)
        ]);
    }

    /**
     * Delete a Relation category 
     * 
     * @param SubCategory
     * @return Json response
     */
    public function destroy(Category $category)
    {
        if (Gate::denies('delete', $category)) {
            return AuthResponse::unauthorized();
        }

        if ($category->items()->exists()) {
            return AuthResponse::validationErrString(__('You cannot delete category that has posts.'));
        }

        $category->delete();

        return response([
            'status' => 'success',
            'message' => 'Category Relation deleted'
        ]);
    }

    /**
     * Update a relation category 
     * 
     * @param SubCategory
     * @param Request
     * @return Json response
     */
    public function update(Category $category, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'main_category_id' => 'required',
            'sub_category_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        try {
            $this->checkUnique($request->all());
        } catch(\Exception $e) {
            return AuthResponse::validationErrString($e->getMessage());
        }

        $category->main_category_id = $request->main_category_id;
        $category->sub_category_id = $request->sub_category_id;
        $category->save();

        return response([
            'status' => 'success',
            'data' => new CategoryResource($category)
        ]);
    }

    /**
     * Check if the category relation laready exists
     */
    protected function checkUnique(array $data) 
    {
        $exists = Category::where('main_category_id', $data['main_category_id'])->where('sub_category_id', $data['sub_category_id'])->exists();

        if ($exists) {
            throw new \Exception('Relation already Exists.');
        }
    }

    public function show(Category $category)
    {
        CategoryResource::withoutWrapping();
        return new CategoryResource($category);
    }
}
