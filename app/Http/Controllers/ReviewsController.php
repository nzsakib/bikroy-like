<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Review;
use App\Models\Purchase;
use App\Models\UserAccount;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Notifications\YouHaveAReview;
use App\Http\Resources\ReviewResource;

class ReviewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * User can review a purchased post 
     * 
     * @param Request $request
     * @return Response 
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'purchase_id' => 'required',
            'rating' => 'required',
            'body' => 'sometimes|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $purchase = Purchase::with('post', 'cart')->findOrFail($request->purchase_id);

        // if product not delivered then can't rate
        if ($purchase->delivered == 0) {
            return AuthResponse::validationErrString(__('Product is not delivered yet.'));
        }

        // User can not post duplicate review on one purchased product
        if ($purchase->review()->exists()) {
            return AuthResponse::validationErrString(__('You already reviewed this purchase.'));
        }

        // if auth user is not purchased the product return error
        if (auth()->id() != $purchase->cart->user_id) {
            return AuthResponse::validationErrString(__('You did not purchase this product.'));
        }

        $poster = $purchase->post->poster;
        $reviewer = auth()->user();

        $review = $purchase->review()->create([
            'rating' => $request->rating,
            'body' => $request->body,
            'poster_id' => $poster->id,
            'reviewer_id' => $reviewer->id
        ]);

        $poster->notify(new YouHaveAReview($reviewer, $review));

        ReviewResource::withoutWrapping();
        return new ReviewResource($review);
    }

    /**
     * Show all review of a seller on their post
     */
    public function index($poster_id)
    {
        $user = UserAccount::with('reviews.reviewer')->findOrFail($poster_id);

        $reviews = $user->reviews()->with('cart.post')->latest()->paginate(20);
        // $reviews = Review::with('reviewer')->where('poster_id', $poster_id)->get();

        return ReviewResource::collection($reviews);
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'review_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $review = Review::findOrFail($request->review_id);

        if ($review->reviewer_id != auth()->id()) {
            return AuthResponse::validationErrString(__('You do not have correct permission.'));
        }

        $review->delete();

        return response([
            'status' => 'success',
            'message' => 'Review is deleted successfully.',
            'review' => $review
        ]);
    }

    /**
     * Show single review of a user
     *
     * @param Review $id
     * @return Response
     */
    public function show($id)
    {
        $review = auth()->user()->reviewed()->where('id', $id)->firstOrFail();

        ReviewResource::withoutWrapping();
        return new ReviewResource($review);
    }

    /**
     * Edit review of a user
     *
     * @param Review $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'rating' => 'required',
            'body' => 'sometimes|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $review = Review::findOrFail($id);
        // if auth user is not editing return error
        if (auth()->id() != $review->reviewer_id) {
            return AuthResponse::validationErrString(__('You do not have correct permission.'));
        }

        $review->rating = $request->rating;
        $review->body = $request->body;
        $review->save();

        ReviewResource::withoutWrapping();
        return new ReviewResource($review);
    }
}
