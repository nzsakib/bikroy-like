<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Models\Otp;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Mail\OtpEmailVerification;
use Illuminate\Support\Facades\Mail;

class EmailVerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sendCode(Request $request)
    {
        // receive email 
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $check = $this->checkUniqueEmail($request->email);
        
        if ($check) {
            return AuthResponse::validationErrString(__('Email already exists, please use another email.'));
        }

        DB::beginTransaction();

        try {
            // generate OTP code and save to database
            $code = random_int(100000, 999999);
            while (Otp::where('code', $code)->exists()) {
                $code = random_int(100000, 999999);
            }
    
            // save to db 
            $otp = Otp::create([
                'code' => $code,
                'user_id' => auth()->id(),
                'email' => $request->email,
            ]);
            // send the OTP through email and send in response
            $user = auth()->user()->userProfile;
            
            Mail::to($request->email)->send(new OtpEmailVerification($user, $otp->code));
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return AuthResponse::validationErrString($e->getMessage());
        }

        return response([
            'status' => 'success',
            'message' => __('Verification code is sent to the email.')
        ]);
    }

    public function verify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = auth()->user();
        $otp = $user->otp()->latest()->unused()->first();

        if (!$otp || $otp->code !== $request->code) {
            return AuthResponse::validationErrString(__('Invalid Otp Code.'));
        }

        if ($otp->created_at->diffInMinutes(now()) > 10) {
            return AuthResponse::validationErrString(__('Code Expired.'));
        }
        
        DB::beginTransaction();
        try {
            $user->userProfile->email = $otp->email;
            $user->userProfile->email_verified = true;
            $user->userProfile->save();
            $otp->used = true;
            $otp->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return AuthResponse::validationErrString(__('Could not verify email, try again.'));
        }

        return response([
            'status' => 'success',
            'message' => __('Email is verified successfully.')
        ]);
    }

    protected function checkUniqueEmail($email)
    {
        $user = auth()->user()->userProfile;
        return UserProfile::where('email', $email)->where('user_id', '<>', auth()->id())->exists();
    }
}
