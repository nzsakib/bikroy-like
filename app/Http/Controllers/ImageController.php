<?php

namespace App\Http\Controllers;

use File;
use Gate;
use Storage;
use Validator;
use App\Models\ItemGallery;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;

class ImageController extends Controller
{
    public function __construct()
    {
        // $this->middleware('jwt.verify');
        // TODO Apply middleware to lock down
        // \Auth::shouldUse('api');
    }

    protected $allowedfileExtension = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

    /**
     * Store all image to AWS and return an array of urls
     * 
     * @param Request
     * @return array
     */
    public function storeAll(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'images' => 'required|array'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $result = [];

        $images = $request->file('images');

        foreach ($images as $image) {
            $check = $this->checkExtension($image);

            if ( !$check ) {
                return response()->json([ 'errors' => [__('Invalid file format.')]], 422);
            }
        }

        foreach ($request->images as $image) {
            $url = $this->upload($image);

            array_push($result, $url);
        }

        return response([
            'urls' => $result
        ], 200);
    }

    /**
     * Store a single Image and return the image url
     * 
     * @param Request
     * @return string url
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|file|max:2048'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $image = $request->file('image');
        
        $check = $this->checkExtension($image);

        if ( !$check ) {
            return AuthResponse::validationErrString(__('Invalid file format.'));
        }

        $url = $this->upload($image);

        return response([
            'url' => $url
        ], 200);
    }

    /**
     * Delete a user item image using image url
     * 
     * @param string  $image 
     * @return Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $image = $request->url;

        $image = ItemGallery::where('image_link', $image)->first();

        if (Gate::denies('destroy', $image)) {
            return AuthResponse::unauthorized();
        }
        

        $file_array = explode('/', $image->image_link);
        $filename = end($file_array);
        Storage::disk('s3')->delete('items/' . $filename);
        
        $image->delete();
        
        return response([
            'status' => 'success',
            'message' => 'image deleted',
            'image' => [
                'id' => $image->id,
                'image_link' => $image->image_link
            ]
        ], 200);
    }

    /**
     * Uploads the given image to AWS
     * 
     * @param File
     * @return string url
     */
    protected function upload($image)
    {
        $filename = 'img_' . md5(uniqid() . time()) . '.' .
                        $image->getClientOriginalExtension();
        
        Storage::disk('s3')->put('items/' . $filename, File::get($image));
            
        return Storage::disk('s3')->url('items/' . $filename);
    }

    /**
     * Check extension of the image file 
     * 
     * @param File 
     * @return boolean
     */
    protected function checkExtension($image)
    {
        $extension = $image->getClientOriginalExtension();

        return in_array($extension, $this->allowedfileExtension);
    }
}
