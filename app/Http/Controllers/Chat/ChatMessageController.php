<?php

namespace App\Http\Controllers\Chat;

use App\Models\ChatRoom;
use App\Models\ChatMessage;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Notifications\YouHaveNewMessage;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Chat\ChatMessageResource;

class ChatMessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Store a chat message for a chat room
     * 
     * @param Request 
     * @return ChatMessageResource
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'chat_room_id' => 'required',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $room = ChatRoom::findOrFail($request->chat_room_id);

        if (Gate::denies('create', $room)) {
            return AuthResponse::unauthorized();
        }
        
        if ($room->buyer_id == auth()->id()) {
            $receiver = $room->poster;
        } else {
            $receiver = $room->buyer;
        }

        if ($receiver->isBlocked()) {
            return AuthResponse::validationErrString('Sorry! The user is currently unavailable.');
        }

        $message = $room->chatMessages()->create([
            'message' => $request->message,
            'sender_id' => auth()->id(),
            'receiver_id' => $receiver->id
        ]);
        if ($room->buyer_archived == true || $room->seller_archived == true) {
            $room->buyer_archived = $room->seller_archived = false;
            $room->save();
        }

        $sender = auth()->user();
        $message = $message->fresh();
        $receiver->notify(new YouHaveNewMessage($sender, $message, $room));
        
        ChatMessageResource::withoutWrapping();
        return new ChatMessageResource($message);
    }

    public function destroy(ChatMessage $message)
    {
        if (Gate::denies('destroy', $message)) {
            return AuthResponse::unauthorized();
        }

        $message->delete();

        return response([
            'status' => 'success',
            'message' => 'Message deleted successfully.'
        ]);
    }

    /**
     * Count of all unseen chat messages of authenticated user
     *
     * @return void
     */
    public function unseenCount()
    {
        $unseenCount = ChatRoom::has('myUnseenMessage', '>', 0)->count();

        return response([
            'unseenCount' => $unseenCount
        ]);
    }
}
