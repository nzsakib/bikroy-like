<?php

namespace App\Http\Controllers\Chat;

use App\Models\Post;
use App\Models\ChatRoom;
use App\Models\ChatMessage;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Chat\ChatRoomResource;
use App\Http\Resources\Chat\ChatMessageResource;

class ChatRoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * List all chat rooms of a user (buying+selling)
     *
     * @return ChatRoomResource
     */
    public function index()
    {
        $user = auth()->user();
        $posts = $user->posts()->pluck('id')->toArray();
        $rooms = ChatRoom::whereIn('post_id', $posts)
                ->where('seller_archived', false)
                ->orWhere('buyer_id', auth()->id())
                ->where('buyer_archived', false)
                ->with(['post.poster', 'latestMessage', 'buyer'])
                ->notEmpty()
                ->latest('updated_at')
                ->paginate(20);
        
        return ChatRoomResource::collection($rooms);
    }

    /**
     * Show an existing chat room messages to valid user
     * 
     * @param ChatRoom Model
     * @return ChatMessageResource
     */
    public function show(ChatRoom $room)
    {
        $room->load('post', 'buyer', 'poster');

        if (auth()->id() != $room->buyer_id && 
            auth()->id() != $room->poster->id ) {
            
            return AuthResponse::validationErrString('Sorry, you don\'t have permission', 403);
        }
        
        $messages = ChatMessage::where('chat_room_id', $room->id)->latest()->paginate(25);
        
        if ( $room->buyer->isBlocked() || 
            $room->poster->isBlocked() || 
            $room->post->status == Post::STATUS_BLOCKED || 
            $room->post->status == Post::STATUS_USER_BANNED ) 
        {
            return ChatMessageResource::collection($messages)->response()->setStatusCode(226);
        }

        return ChatMessageResource::collection($messages);
    }

    /**
     * Create a new chat room or return an existing chatroom 
     * 
     * @param Request post_id and buyer id (auth)
     * @return Response room id
     */
    public function create()
    {
        $validator = Validator::make(request()->all(), [
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $post = Post::findOrFail(request('post_id'));

        if ($post->poster_id == auth()->id()) {
            return AuthResponse::validationErrString('Oops, you cannot message yourself.');
        }

        $room = $this->findOrCreate();

        return response([
            'status' => 'success',
            'chat_room_id' => $room->id
        ]);
    }

    /**
     * Archive an existing chatroom 
     * 
     * @param ChatRoom Model
     * @return Json Response 
     */
    public function archive(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'chat_room_id' => 'required'
        ]);

        if ( $validator->fails() ) {
            return AuthResponse::validationError($validator);
        }
        $room = ChatRoom::findorFail($request->chat_room_id);
        
        if (Gate::denies('update', $room)) {
            return AuthResponse::unauthorized();
        }
        if ($room->buyer_id == auth()->id()) {
            $room->buyer_archived = true;
        } else {
            $room->seller_archived = true;
        }
        
        $room->save(['timestamps' => false]);

        return response([
            'status' => 'success',
            'message' => 'Chat room is archived.'
        ]);
    }

    /**
     * Delete an existing chatroom 
     * 
     * @param ChatRoom Model
     * @return Json Response
     */
    public function destroy(ChatRoom $room)
    {
        if (Gate::denies('destroy', $room)) {
            return AuthResponse::unauthorized();
        }

        $room->delete();

        return response([
            'status' => 'success',
            'message' => 'Chat room is deleted.'
        ]);
    }

    /**
     * Check if the room exists, if not then create a new one 
     * 
     * @param Request 
     * @return ChatRoom Model
     */
    protected function findOrCreate()
    {
        $record = ChatRoom::where('post_id', request('post_id'))
                ->where('buyer_id', auth()->id())
                ->first();
        if ($record) {
            return $record;
        }

        return ChatRoom::create([
            'post_id' => request('post_id'),
            'buyer_id' => auth()->id(),
            'archived' => false
        ]);
    }

    /**
     * List all chatrooms where user is buyer
     *
     * @return ChatRoomResource
     */
    public function buying()
    {
        $user = auth()->user();
        $rooms = $user->chatRoom()
                    ->with(['post.poster', 'latestMessage', 'buyer'])
                    ->notEmpty()
                    ->where('buyer_archived', false)
                    ->latest('updated_at')
                    ->paginate(20);
        
        return ChatRoomResource::collection($rooms);
    }

    /**
     * List all chatroom where user is a seller
     *
     * @return ChatRoomResource
     */
    public function selling()
    {
        $user = auth()->user();
        $posts = $user->posts()->pluck('id')->toArray();
        $rooms = ChatRoom::whereIn('post_id', $posts)
                        ->with('post.poster', 'latestMessage', 'buyer')
                        ->notEmpty()
                        ->where('seller_archived', false)
                        ->latest('updated_at')
                        ->paginate(20);
        
        return ChatRoomResource::collection($rooms);
    }

    /**
     * Make authenticated user chat message in a chat room seen
     *
     * @param Request $request
     * @return Response
     */
    public function seen(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'chat_room_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }
        $room = ChatRoom::findOrFail($request->chat_room_id);
        if (Gate::denies('show', $room)) {
            return AuthResponse::unauthorized();
        }

        $room->chatMessages()->notSentByMe()->unseen()->update(['seen' => true]);

        return response([
            'status' => 'success',
            'message' => 'All messages now seen'
        ]);
    }
}
