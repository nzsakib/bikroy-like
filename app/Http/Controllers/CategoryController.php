<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\MainCategory;
use Illuminate\Http\Request;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Category\SubCategoryResource;

class CategoryController extends Controller
{
    /**
     * Get all category relation for all users
     *
     * @return CategoryResource
     */
    public function index()
    {
        $cat = Category::distinct()->select('main_category_id')
                ->with('mainCategory.subcategory')
                ->get();

        $cat = $cat->pluck('mainCategory');
        
        CategoryResource::withoutWrapping();      
        return CategoryResource::collection($cat);
    }

    /**
     * Return all related subcategories of a main category
     *
     * @param MainCategory $mainCategory
     * @return SubCategoryResource
     */
    public function showSubcategory($mainCategory)
    {
        $mainCategory = MainCategory::findOrFail($mainCategory);
        $subcategories = $mainCategory->subCategory;
        
        SubCategoryResource::withoutWrapping();
        return SubCategoryResource::collection($subcategories);
    }
}
