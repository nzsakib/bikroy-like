<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\SocialId;
use App\Models\UserAccount;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Resources\UserAccountResource;
use Illuminate\Auth\AuthenticationException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class LoginController extends Controller
{
    /**
     * Login a new user
     * 
     * @param Request $request
     * @return Json response
     */
    public function authenticate(Request $request)
    {
        $messages = [
            'required_without' => __('validation.required'),
            'required_with' => __('validation.required')
        ];
        
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required_without:social_id|size:14',
            'password' => 'required_with:phone_number|min:8',
            'social_id' => 'sometimes',
            'social_type' => 'required_with:social_id'
        ], $messages);
        
        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        if ($request->social_id) {
            $user = $this->getSocialUser($request);
            
            if ( ! $user ) {
                return AuthResponse::validationErrString(__('Incorrect credentials.'));
            }

            $token = JWTAuth::fromUser($user);
            
        } else {
            $credentials = $request->only('phone_number', 'password');
    
            try {
                
                if ( !$token = JWTAuth::attempt($credentials) )
                    return AuthResponse::validationErrString(__('Incorrect credentials.'));

            } catch (JWTException $e) {
                return AuthResponse::validationErrString('could_not_create_token');
            }
            $user = auth()->user();
        }

        if ($user->isBlocked()) {
            return AuthResponse::validationErrString(__('message.bannedLogin'), 418);
            // throw new AuthenticationException(__('message.bannedLogin'));
        }

        return response([
            'token' => $token,
            'expires_in' => auth()->guard('api')->factory()->getTTL()
        ]);
    }

    /**
     * Get authenticated user details 
     * 
     * @param Token 
     * @return Resource UserAccountResource
     */
    public function getAuthenticatedUser()
    {
        try {
            
            if (!$user = JWTAuth::parseToken()->authenticate())
                return AuthResponse::validationErrString('user_not_found');

        } catch (TokenExpiredException $e) {

            return AuthResponse::validationErrString('token_expired');
            
        } catch (TokenInvalidException $e) {

            return AuthResponse::validationErrString('token_invalid');
            
        } catch (JWTException $e) {

            return AuthResponse::validationErrString('token_absent');
        }
        $user['reviews_count'] = $user->reviews()->count();
        $user->load('bank');
        UserAccountResource::withoutWrapping();
        
        return new UserAccountResource($user);
    }

    /**
     * Refresh Existing api token
     * 
     * @param Token
     * @return Json Response
     */
    public function refreshToken(){
        $token = JWTAuth::getToken();

        if(!$token){
            return AuthResponse::validationErrString('token_not_provided');
        }

        try{
            $token = JWTAuth::refresh($token);
        }catch(TokenInvalidException $e){
            return AuthResponse::validationErrString('invalid_token');
        }

        return response([
            'token' => $token,
            'expires_in' => auth()->guard('api')->factory()->getTTL()
        ]);
    }

    /**
     * Find the user using social_id and social_type
     * 
     * @param Request $request
     * @return Model UserAccount
     */
    protected function getSocialUser(Request $request)
    {
        $social = SocialId::where('social_id', $request->social_id)
                            ->where('social_type', $request->social_type)
                            ->first();
        
        if ( ! $social ) {
            return false;
        }

        return UserAccount::findOrFail($social->user_id);
    }

    /**
     * Logout authenticated user 
     * 
     * @param Token 
     * @return Json Response
     */
    public function logout()
    {
        try {
            if (auth()->check()) {
                $user = auth()->user();
                $user->firebase_token = null; 
                $user->device_type = null;
                $user->save();
                JWTAuth::invalidate(JWTAuth::getToken());

                return response([
                    'status' => 'success',
                    'message' => 'User successfully logged out.'
                ], 200);
            } else {
                return response([
                    'status' => 'success',
                    'message' => 'Already logged out.'
                ], 201);
            }

        } catch (\Exception $e) {
            return AuthResponse::validationErrString($e->getMessage());
        }
    }
}
