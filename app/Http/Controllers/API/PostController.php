<?php

namespace App\Http\Controllers\Api;

use DB;
use File;
use Storage;
use Validator;
use App\Models\Tag;
use App\Models\Item;
use App\Models\Post;
use App\Enum\PostStatus;
use App\Models\Category;
use App\Models\ItemGallery;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\Home\PostCollection;
use App\Http\Resources\Post\PostResource as SinglePost;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show', 'minMaxPriceRange', 'destroy']);
        $this->middleware('auth.all')->only('show');
        $this->middleware('auth:api,admin')->only('destroy');
    }
    /**
     * Show all active posts for homepage and explore
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::available()
            ->active()
            ->latest()
            ->withCount(['likes', 'comments'])
            ->with('item.tags')
            ->paginate(20);
        // 
        return new PostCollection($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'heading' => 'required|string',
            'price' => 'required|integer|gt:0',
            'caption' => 'required|string',
            'used' => 'required|boolean',
            'tags' => 'required|array',
            'main_category_id' => 'required',
            'sub_category_id' => 'sometimes',
            'address' => 'string',
            'images' => 'sometimes|array|max:4',
            'urls' => 'sometimes|array|max:4',
            'item_quantity' => 'sometimes|integer|min:1'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        DB::beginTransaction();
        try {

            $item = new Item();
            $item->heading = $request->heading;
            $item->price = $request->price;
            $item->caption = $request->caption;
            $item->used = $request->used;
            if ($request->address) {
                $item->address = $request->address;
            }
            $item->category_id = $this->getCategoryId();
            $item->save();

            $quantity = $request->item_quantity ? $request->item_quantity : 1;
            $item->addQuantity($quantity);

            $tagIds = $this->createTags($request->tags);
            $item->tags()->attach($tagIds);

            $upload_img = false;
            if ($request->images) {
                $this->uploadImages($request, $item);
                $upload_img = true;
            }

            if ($request->urls) {
                foreach ($request->urls as $url) {
                    if ($url == null) throw new \Exception(__('Please select an image.'));
                    $item->images()->create([
                        'image_link' => $url
                    ]);
                    $upload_img = true;
                }
            }
            if (!$upload_img) {
                throw new \Exception(__('Please select image.'));
            }
            $post = Post::create([
                'poster_id' => auth()->id(),
                'item_id' => $item->id,
                'featured' => 0,
                'status' => Post::STATUS_PENDING
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return AuthResponse::validationErrString($e->getMessage(), 400);
        }

        return $post;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::withCount(['likes', 'comments'])
            ->with('item.itemCounter', 'item.tags', 'item.category')
            ->findOrFail($id);

        if (Gate::denies('show', $post)) {
            return AuthResponse::validationErrString(__('What you are looking for is not here.'), 404);
        }

        return new SinglePost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::with('item.itemCounter')->findOrFail($id);

        if (Gate::denies('update', $post)) {
            return AuthResponse::unauthorized();
        }

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $item = $post->item;
        $item->heading = $request->heading;
        $item->price = $request->price;
        $item->caption = $request->caption;
        $item->used = $request->used;
        // if ($request->address) {
        //     $item->address = $request->address;
        // }
        $item->save();

        $quantity = $request->item_quantity ? $request->item_quantity : 1;
        if ($request->item_quantity <= $item->itemCounter->sold) {
            return AuthResponse::validationErrString(__('Item quantity must be greater than :count', ['count' => $item->itemCounter->sold]));
        }
        $item->itemCounter->count = $quantity;
        $item->itemCounter->save();


        $tagIds = $this->createTags($request->tags);
        $item->tags()->sync($tagIds);

        return response([
            'status' => 'success',
            'message' => __('Item is updated.')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        if (Gate::denies('destroy', $post)) {
            return AuthResponse::unauthorized();
        }

        if (Gate::denies('sold', $post)) {
            return AuthResponse::validationErrString(__('You cannot delete a product which has been purchased.'));
        }

        $post->delete();

        return response([
            'status' => 'success',
            'message' => __('Post deleted successfully.')
        ]);
    }

    /**
     * Inserts new tags into database and return array of id
     * 
     * @param array of tags 
     * @return array of ids 
     */
    protected function createTags($tags)
    {
        // dd($tags);
        $ids = [];
        foreach ($tags as $tag) {
            $record = Tag::create(['tag_name' => $tag]);
            $ids[] = $record->id;
        }

        return $ids;
    }

    /**
     * Uploads images to AWS 
     * 
     * @return void
     */
    protected function uploadImages(Request $request, $item)
    {
        $images = $request->file('images');
        $allowedfileExtension = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

        foreach ($images as $image) {
            $extension = $image->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);

            if (!$check) {
                return response()->json(['error' => __('Invalid file format.')], 422);
            }
        }

        foreach ($request->images as $image) {
            $gallery = new ItemGallery();

            $filename = 'img_' . md5(uniqid() . time()) . '.' .
                $image->getClientOriginalExtension();

            Storage::disk('s3')->put('items/' . $filename, File::get($image));

            $url = Storage::disk('s3')->url('items/' . $filename);

            $gallery->image_link = $url;

            $gallery->item_id = $item->id;

            $gallery->save();
        }
    }

    /**
     * Get related category from given all main and sub cat
     * 
     * @return int id
     */
    protected function getCategoryId()
    {
        $main = request('main_category_id');
        $sub = request('sub_category_id');

        $category = Category::where('main_category_id', $main)
            ->where('sub_category_id', $sub)
            ->first();

        if (!$category) {
            throw new \Exception(__("Category Not Found."), 500);
        }

        return $category->id;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'heading' => 'required|string',
            'price' => 'required|integer|gt:0',
            'caption' => 'required|string',
            'used' => 'required|boolean',
            'tags' => 'required|array|min:1',
            // 'main_category_id' => 'required',
            // 'sub_category_id' => 'sometimes',
            'address' => 'string',
            // 'images' => 'sometimes|array',
            // 'urls' => 'sometimes|array',
            'item_quantity' => 'sometimes|integer|min:1'
        ]);
    }

    /**
     * Get minimum and maximum price of active posts
     * 
     * @param void
     * @return Response array
     */
    public function minMaxPriceRange()
    {
        $max = \DB::table('posts')
            ->where('status', PostStatus::active)
            ->leftJoin('items', 'posts.item_id', '=', 'items.id')
            ->leftJoin('item_counters', 'items.id', '=', 'item_counters.item_id')
            ->whereRaw('sold < count')
            ->orderBy('price', 'desc')
            ->first();
        $max = $max->price;

        $min = \DB::table('posts')
            ->where('status', PostStatus::active)
            ->leftJoin('items', 'posts.item_id', '=', 'items.id')
            ->leftJoin('item_counters', 'items.id', '=', 'item_counters.item_id')
            ->whereRaw('sold < count')
            ->orderBy('price', 'asc')
            ->first();
        $min = $min->price;

        return response([
            'min' => $min,
            'max' => $max
        ]);
    }
}
