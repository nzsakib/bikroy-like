<?php

namespace App\Http\Controllers\Api;

use Gate;
use Validator;
use App\Models\Post;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use App\Events\PostRecievedNewComment;
use App\Http\Resources\Comments\CommentResource;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $posts = $post->comments()->with('user.userProfile')->latest()->paginate(20);

        return CommentResource::collection($posts);
    }

    /**
     * Save a new comment for the given post
     *
     * @param  Post  $post 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Post $post, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = auth()->user();
        // dd($post->comments());
        $comment = $post->comments()->create([
            'comment' => $request->comment,
            'user_id' => $user->id
        ]);

        event(new PostRecievedNewComment($comment, $user, $post));

        CommentResource::withoutWrapping();
        return new CommentResource($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        if (Gate::denies('update', $comment)) {
            return AuthResponse::unauthorized();
        }

        $validator = Validator::make($request->all(), [
            'comment' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $comment->comment = $request->comment;
        $comment->save();

        CommentResource::withoutWrapping();
        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if (Gate::denies('destroy', $comment)) {
            return AuthResponse::unauthorized();
        }


        $comment->delete();

        CommentResource::withoutWrapping();
        return new CommentResource($comment);
    }

    public function mentionableUsername($postId)
    {
        // auth user following username 
        $user = auth()->user();
        $users = $user->followedUsers()->with(['userProfile' => function ($query) {
            $query->select(['username', 'user_id']);
        }])->get();

        $users = $users->pluck('userProfile')->flatten()->pluck('username')->all();
        // username of the post owner
        $post = Post::findOrFail($postId);
        $owner = $post->poster->userProfile->username;

        // username of the comments in the post 
        $commenter = $post->comments()->with('user.userProfile')->get();

        $commenter = $commenter->pluck('user')->flatten()->pluck('userProfile')->flatten()->pluck('username')->all();

        // username of the mentioned users in the post 
        $inpost = $post->comments->flatMap->mentionedUserNames()->all();

        return collect(array_merge($users, [$owner], $commenter, $inpost))->unique()->map(function ($user) {
            return '@' . $user;
        })->values()->all();
    }
}
