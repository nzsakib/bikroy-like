<?php

namespace App\Http\Controllers\Api;

use DB;
use Gate;
use App\Models\Post;
use App\Enum\PostStatus;
use App\Models\Purchase;
use Validator;
use Illuminate\Http\Request;
use App\Helpers\AuthResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\Cart\CartResource;
use App\Notifications\YourPostWasPurchased;
use App\Http\Resources\Purchase\PurchaseResource;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get all cart item of an authenticated user
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->guard('api')->user();

        $carts = $user->availableCart->load('post.poster.userProfile');

        CartResource::withoutWrapping();
        return CartResource::collection($carts);
    }

    /**
     * Add a post in user cart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = auth()->guard('api')->user();
        $post = Post::active()->findOrFail($request->post_id);
        if (!$post->poster->isActive) {
            return AuthResponse::validationErrString(__('You can not buy this product.'));
        }

        if ($post->poster_id == $user->id) {
            return AuthResponse::validationErrString(__('You can not buy your own product.'));
        }

        if ($user->availableCart()->where('post_id', $request->post_id)->exists()) {
            return AuthResponse::validationErrString(__('The item you are trying to add is already in your cart.'), 409);
        }

        $cart = $user->carts()->create([
            'post_id' => $post->id,
            'bought' => false,
            'removed' => false
        ]);

        return response([
            'status' => 'success',
            'message' => __('Product added to cart.'),
            'cart' => $cart
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->guard('api')->user();
        $cart = $user->availableCart()->findOrFail($id);

        if ($cart->user_id != $user->id) {
            return AuthResponse::unauthorized();
        }

        $cart->removed = true;
        $cart->save();

        return $cart;
    }

    /**
     * Purchase the cart specified with id
     * 
     * @param Request 
     */
    public function purchase(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_method_id' => 'required',
            'delivery_type_id' => 'required',
            'shipping_address' => 'required',
            'cart_id' => 'required',
            'transaction_id' => 'required|string'
        ]);

        if ($validator->fails()) {
            return AuthResponse::validationError($validator);
        }

        $user = auth()->guard('api')->user();
        $cart = $user->availableCart()->findOrFail($request->cart_id);

        if (Gate::denies('update', $cart)) {
            return AuthResponse::validationErrString(__('Product is not available.'));
        }

        DB::beginTransaction();
        try {
            $cart->bought = true;
            $cart->save();

            $payment_method = $request->payment_method_id;
            $delivery_type = $request->delivery_type_id;
            $address = $request->shipping_address;

            $purchase = $cart->purchase()->create([
                'seller_release' => false,
                'at_head_office' => false,
                'qc_passed' => false,
                'head_office_release' => false,
                'payment_method_id' => $payment_method,
                'delivery_type_id' => $delivery_type,
                'shipping_address' => $address,
                'transaction_id' => $request->transaction_id,
                'price' => $cart->post->item->price,
            ]);

            $cart->post->item->itemCounter->increment('sold');

            if (
                $cart->post->item->itemCounter->count <=
                $cart->post->item->itemCounter->sold
            ) {

                $cart->post->update(['status' => PostStatus::sold]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
        $cart->post->poster->notify(new YourPostWasPurchased($user, $cart->post, $purchase));

        return response([
            'status' => 'success',
            'message' => 'Cart is purchased.',
        ], 200);
    }

    /**
     * All purchased post of a authenticated user
     * 
     */
    public function allPurchased()
    {
        $user = auth()->user();

        $purchases = Purchase::with([
            'cart.user.userProfile',
            'post.item.images',
            'paymentMethod',
            'deliveryType',
            'post.poster.userProfile',
            'ownReview'
        ])
            ->withCount('ownReview')
            ->whereHas('cart', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            })->latest()->paginate(20);

        return PurchaseResource::collection($purchases);
    }

    /**
     * Get all sold post of a poster
     */
    public function allSold()
    {

        $user = auth()->user();

        // 'cart.user.userProfile -> is the buyer
        // post.poster is the seller 
        $purchases = Purchase::with([
            'cart.user.userProfile',
            'post.item.images',
            'paymentMethod',
            'deliveryType',
            'post.poster.userProfile'
        ])
            ->whereHas('post', function ($q) use ($user) {
                $q->where('poster_id', $user->id);
            })->latest()->paginate(20);

        return PurchaseResource::collection($purchases);
    }

    /**
     * Get the count of the user cart 
     * 
     * @param Token
     * @return int 
     */
    public function count()
    {
        $user = auth()->user();

        $count = $user->availableCart()->count();

        return response([
            'count' => $count
        ]);
    }

    /**
     * Generate a unique transaction id for purchase tracking
     * 
     * @return string transaction_id
     */
    public function generateTransaction()
    {
        $id = 'NEO_' . strtoupper(str_random(12));

        while (Purchase::where('transaction_id', $id)->exists()) {
            $id = 'NEO_' . strtoupper(str_random(12));
        }

        return response([
            'transaction_id' => $id
        ]);
    }
}
