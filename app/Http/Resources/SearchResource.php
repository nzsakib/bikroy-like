<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'post_id' => $this->id,
            'post_created_at' => (string) $this->created_at,
            'user_name' => $this->poster->userProfile->name,
            'profile_image' => $this->poster->userProfile->profile_image,
            'item_heading' => $this->item->heading,
            'price' => $this->item->price,
            'item_caption' => $this->item->caption,
            'item_images' =>  $this->item->images_array,
            'user_address' => $this->poster->userProfile->address,
            'item_address' => $this->item->address,
            'is_saved' => $this->is_saved,
            'is_liked' => $this->is_liked,
            'likes_count' => $this->likes_count,
            'comments_count' => $this->comments_count,
            'used' => !! $this->item->used,
            'tags' => $this->item->tags->pluck('tag_name'),
            'available_quantity' => $this->item->available_quantity,
            'total_quantity' => $this->item->total_quantity,
        ];
    }
}
