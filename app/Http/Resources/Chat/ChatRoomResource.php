<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatRoomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $this is the chatRoom instance
        return [
            'chat_room_id' => $this->id,
            'product_image' => $this->post->item->images_array[0],
            'other_person' => $this->otherPersonName,
            'item_heading' => $this->post->item->heading,
            'last_message' => $this->latestMessage->message,
            'last_message_time' => (string) $this->latestMessage->created_at,
            'unseenMessageCount' => $this->my_unseen_message_count
        ];
    }
}
