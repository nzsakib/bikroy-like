<?php

namespace App\Http\Resources\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\Item\ItemResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'post_id' => $this->post_id,
            'post_owner' => $this->post->poster->userProfile->username,
            'item_details' => ItemResource::make($this->itemDetails()),
        ];
    }
}
