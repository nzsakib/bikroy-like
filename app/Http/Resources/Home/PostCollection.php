<?php

namespace App\Http\Resources\Home;

use Illuminate\Support\Collection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'data' => $this->format($this->collection),
        ];
    }

    public function format(Collection $posts) 
    {
        return $posts->map(function($post) {
            return new PostResource($post);
        });
    }
}
