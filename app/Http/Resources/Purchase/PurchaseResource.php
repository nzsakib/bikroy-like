<?php

namespace App\Http\Resources\Purchase;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // the purchase instance
        return [
            'purchase_id' => $this->id,
            'post_id' => $this->post->id,
            'item_heading' => $this->post->item->heading,
            'item_images' => $this->post->item->images_array,
            'buyer_user_name' => $this->cart->user->userProfile->name,
            'seller_user_name' => $this->post->poster->userProfile->name,
            'price' => $this->post->item->price,
            'payment_method' => $this->paymentMethod->name,
            'purchase_date' => (string) $this->created_at,
            'delivery_type' => $this->deliveryType->name,
            'seller_release' => $this->seller_release,
            'at_head_office' => $this->at_head_office,
            'qc_passed' => $this->qc_passed,
            'head_office_release' => $this->head_office_release,
            'isDelivered' => $this->delivered,
            'isReviewed' => $this->own_review_count > 0 ? true : false,
            'review_id' => $this->ownReview ? $this->ownReview->id : null,
        ];
    }
}
