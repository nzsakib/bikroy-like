<?php

namespace App\Http\Resources\Comments;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'user_name' => '@' . $this->user->userProfile->username,
            'full_name' => $this->user->userProfile->name,
            'profile_image' => $this->user->userProfile->profile_image,
            'isOwnComment' => $this->user_id == auth()->id(),
            'created_at' => (string) $this->created_at   
        ];
    }
}
