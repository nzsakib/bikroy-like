<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rating' => $this->rating,
            'body' => $this->body,
            'created_at' => (string) $this->created_at,
            'reviewer_name' => $this->reviewer->userProfile->name,
            'profile_image' => $this->reviewer->userProfile->profile_image,
            'isOwnReview' => auth()->id() == $this->reviewer_id,
            'post_id' => $this->cart->post->id,
            'item_image' => $this->cart->post->item->images_array[0]
        ];
    }
}
