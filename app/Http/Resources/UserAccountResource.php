<?php

namespace App\Http\Resources;

use App\Http\Resources\BankAccountResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'isOwnProfile' => $this->id == auth()->id(),
            'phone_number' => $this->phone_number,
            'created_at' => (string) $this->created_at,
            'reviews_count' => $this->reviews_count,
            'avg_rating' => round( (double) $this->avg_rating, 1 ),
            'isFollowing' => $this->is_following,
            'following_count' => $this->following_count,
            'follower_count' => $this->follower_count,
            'isComplete' => $this->isComplete,
            'isActive' => $this->isActive,
            'user_profile' => new UserProfileResource($this->userProfile),
            'bank_details' => auth()->id() == $this->id ? new BankAccountResource($this->bank) : null,
        ];

        return $data;
    }
}
