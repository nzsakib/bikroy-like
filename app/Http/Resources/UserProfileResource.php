<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            'email' => $this->email,
            'email_verified' => !! $this->email_verified,
            'phone_verified' => !! $this->phone_verified,
            'date_of_birth' => (string) $this->date_of_birth ?: null,
            'profile_image' => $this->profile_image,
            'address' => $this->address,
            'about' => $this->about,
            'gender' => strtolower($this->gender)
        ];
    }
}
