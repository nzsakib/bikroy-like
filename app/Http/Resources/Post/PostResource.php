<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'post_id' => $this->id,
            'poster_id' => $this->poster_id,
            'poster_status' => $this->poster->isActive,
            'user_name' => $this->poster->userProfile->name,
            'username' => $this->poster->userProfile->username,
            'profile_image' => $this->poster->userProfile->profile_image,
            'isFollowing' => $this->poster->isFollowing,
            'user_address' => $this->poster->userProfile->address,
            'price' => $this->item->price,
            'likes_count' => $this->likes_count,
            'comments_count' => $this->comments_count,
            'item_heading' => $this->item->heading,
            'item_caption' => $this->item->caption,
            'item_address' => $this->item->address,
            'item_images' => $this->item->images_array,
            'post_created_at' => (string) $this->created_at,
            'is_saved' => $this->isSaved,
            'is_liked' => $this->is_liked,
            'is_own_post' => $this->poster_id == auth()->id(),
            'used' => !! $this->item->used,
            'tags' => $this->item->tags->pluck('tag_name'),
            'available_quantity' => $this->item->available_quantity,
            'status' => $this->status,
            'category' => [
                'main' => [
                    'id' => $this->item->category->mainCategory->id,
                    'name' => $this->item->category->mainCategory->name,
                ],
                'sub' => [
                    'id' => $this->item->category->subCategory->id,
                    'name' => $this->item->category->subCategory->name
                ]
            ],
            'total_quantity' => $this->item->total_quantity,
            'sold_quantity' => $this->item->sold,
        ];
    }

    // protected function payment($collection) 
    // {
    //     return $collection->transform(function($item) {
    //         return $item->only(['id', 'name']);
    //     });
    // }
}
