<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\NotificationResource;

class NotificationCollection extends ResourceCollection
{
    public function __construct($resource, $unreadCount)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        
        $this->unreadCount = $unreadCount;
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => NotificationResource::collection($this->collection),
            'unreadNotifications' => $this->unreadCount,
        ];
    }
}
