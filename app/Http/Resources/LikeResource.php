<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LikeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $this is the useraccount instance
        return [
            'user_id' => $this->id,
            'isOwnProfile' => $this->id == auth()->id(),
            'name' => $this->userProfile->name,
            'profile_image' => $this->userProfile->profile_image
        ];
    }
}
