<?php

namespace App\Http\Resources\Item;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'heading' => $this->heading,
            'caption' => $this->caption,
            'address' => $this->address,
            'used' => !! $this->used,
            'item_images' =>  $this->images_array,
        ];
    }
}
