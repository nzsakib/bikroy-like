<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'main_category' => [
                'id' => $this->mainCategory->id,
                'name' => $this->mainCategory->name,
                'image' => $this->mainCategory->image, 
            ],
            'sub_category' => [
                'id' => $this->subCategory->id,
                'name' => $this->subCategory->name,
                'image' => $this->subCategory->image,
            ]
        ];
    }
}
