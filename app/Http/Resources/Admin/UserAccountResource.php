<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->userProfile->name,
            'phone_number' => $this->phone_number,
            'gender' => $this->userProfile->gender,
            'status' => $this->isActive,
            'new_post' => $this->posts()->where('status', 0)->count(),
            'active_post' => $this->posts()->where('status', 1)->count(),
            'total_post' => $this->posts()->count()
        ];
    }
}
