<?php

namespace App\Http\Resources\Admin\Transaction;

use Illuminate\Http\Resources\Json\JsonResource;

class SingleTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $this is the Purchase model instance 
        return [
            'purchase_id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'post_id' => $this->post->id,
            'post_title' => $this->post->item->heading,
            'price' => (int) $this->price,
            'seller_name' => $this->post->poster->userProfile->name,
            'buyer_name' => $this->buyer->userProfile->name,
            'buyer_id' => $this->buyer->id,
            'transaction_date' => (string) $this->created_at,
            'delivery_type' => $this->deliveryType->name,
            'payment_method' => $this->paymentMethod->name
        ];
    }
}
