<?php

namespace App\Http\Resources\Admin\Transaction;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $this is the Purchase model instance 
        return [
            'purchase_id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'transaction_date' => (string) $this->created_at,
            'price' => (int) $this->total,
            'total_product' => $this->count,
            'delivered' => !!$this->delivered,
            'buyer_name' => $this->buyer->userProfile->name,
            'buyer_id' => $this->buyer->id,
        ];
    }
}
