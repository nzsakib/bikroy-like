<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAnalyticsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->userProfile->name,
            'profile_image' => $this->userProfile->profile_image,
            'phone_number' => $this->phone_number,
            'gender' => $this->userProfile->gender,
            'registration_date' => (string) $this->created_at,
            'total_items_count' => $this->total_count ?: 0,
            'total_items_sold' => $this->total_sold ?: 0,
            'sells_percentage' => $this->sells_percentage ?: 0,
            'last_activity' => (string) $this->last_activity,
        ];
    }
}
