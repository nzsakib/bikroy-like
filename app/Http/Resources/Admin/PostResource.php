<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'post_id' => $this->id,
            'user_name' => $this->poster->userProfile->name,
            'post_created_at' => (string) $this->created_at,
            'status' => $this->status,
            'featured' => $this->featured,
            'post_heading' => $this->item->heading
        ];
    }
}
