<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\UserAccount;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->getMessage(),
            'isRead' => $this->read_at ? true : false,
            'profile_image' => $this->getImage(),
            'source_id' => $this->data['source_id'],
            'source_type' => $this->data['source_type'],
            'link' => $this->data['link'], 
            'created_at' => (string) $this->created_at
        ];
    }

    protected function getMessage()
    {
        if ($this->fromUser) {
            return str_replace('$NAME$', $this->fromUser->userProfile->name, $this->data['message']);
        } else {
            return $this->data['message'];
        }
    }

    protected function getImage()
    {
        $link = $this->fromUser ? $this->fromUser->userProfile->profile_image : $this->data['profile_image'];
        
        return $link;
    }
}
