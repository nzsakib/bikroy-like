<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;

class CheckBlockedUserMiddleware
{
    protected $except_urls = [
        'api/logout',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $regex = '#' . implode('|', $this->except_urls) . '#';
        
        if (preg_match($regex, $request->path())) {
            return $next($request);
        }

        if (auth()->guard('api')->check() && !auth()->user()->isAdmin() && auth()->user()->isBlocked()) {
            auth()->logout();

            throw new AuthenticationException('You have been banned.');
        }
        
        return $next($request);
    }
}
