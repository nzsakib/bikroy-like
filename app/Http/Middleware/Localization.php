<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('Content-Language');
        if (!$locale) {
            $locale = config('app.locale');
        }

        if (!array_key_exists($locale, config('app.supported_languages'))) {
            $locale = config('app.fallback_locale');
        }

        app()->setLocale($locale);

        return $next($request)->header('Content-Language', $locale);
    }
}
