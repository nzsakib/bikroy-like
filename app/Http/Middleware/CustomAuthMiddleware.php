<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;


class CustomAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // This middleware is for using guard [if exists] every where 
        // this file is fix for auth()->check() returns null for admin token
        if ( auth()->guard('admin')->check() ) {
            auth()->shouldUse('admin');
             return $next($request);
        } else if (auth()->guard('api')->check()) {
            auth()->shouldUse('api');
            return $next($request);
        }
        return $next($request);
        // throw new AuthenticationException('Unauthenticated.');
    }
}
