<?php 

namespace App\Repositories;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageRepository
{

    protected $allowedfileExtension = ['jpg', 'jpeg', 'png', 'gif', 'svg'];
    
    public function upload($image, $folder = 'items')
    {        
        $check = $this->checkExtension($image);

        if ( !$check ) {
            throw new \Exception("invalid file format", 422);
        }

        $filename =  'img_' . md5(uniqid() . time()) . '.' .
                        $image->getClientOriginalExtension();
        
        Storage::disk('s3')->put($folder . '/' . $filename, File::get($image));
            
        return Storage::disk('s3')->url($folder . '/' . $filename);
    }

    protected function checkExtension($image)
    {
        $extension = $image->getClientOriginalExtension();

        return in_array(strtolower($extension), $this->allowedfileExtension);
    }

    public static function deleteIfNotDefault($url)
    {
        $arr = explode('/', $url);
        $filename = end($arr);

        if ($filename == 'default.png' || $filename == 'default.jpg' || $filename == 'default.jpeg') {
            return true;
        }
        $fullname = implode('/', array_slice($arr, -2, 2));
        Storage::disk('s3')->delete($fullname);

        return true;
    }
}