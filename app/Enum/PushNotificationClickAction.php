<?php 

namespace App\Enum;

class PushNotificationClickAction 
{
    const NOTFICATION_PANEL = 1;
    const CHAT = 2;
}