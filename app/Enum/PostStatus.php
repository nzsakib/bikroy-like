<?php

namespace App\Enum;

abstract class PostStatus
{
    //enum type for post status
    const pending = 0;
    const active = 1;
    const sold = 2;
    const block = 3;
}