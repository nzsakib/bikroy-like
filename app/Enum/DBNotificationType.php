<?php 

namespace App\Enum;

class DBNotificationType 
{
    const REVIEW = 'review';
    const SOLD = 'sold';
    const FOLLOW = 'golbal_user';
    const POST = 'post';
}