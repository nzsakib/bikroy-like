<?php 

namespace App\Enum;

class DeviceType 
{
    const ANDROID = 'android';
    const IPHONE = 'iphone';
    const WEB = 'web';
    const NOT_SET = null;
}

