<?php

namespace App\Listeners;

use App\Models\UserAccount;
use App\Notifications\YouWereMentioned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyMentionedUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $usernames = $event->comment->mentionedUserNames();

        foreach ($usernames as $username) {
            $mentioned = UserAccount::with('userProfile')
                    ->whereHas('userProfile', function($query) use($username) {
                        $query->where('username', $username);
            })->first();
            if ($mentioned && $mentioned->id != $event->user->id) {
                $mentioned->notify(new YouWereMentioned($event->user, $event->post, $event->comment));
            }
        }
    }
}
