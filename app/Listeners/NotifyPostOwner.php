<?php

namespace App\Listeners;

use App\Events\PostRecievedNewComment;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\YouHaveNewComment;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyPostOwner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostRecievedNewComment  $event
     * @return void
     */
    public function handle(PostRecievedNewComment $event)
    {
        if ($event->post->poster_id != $event->user->id) {
            $event->post->poster->notify(new YouHaveNewComment($event->user, $event->post, $event->comment));
        }
    }
}
