<?php

namespace App\Observers;

use App\Models\UserAccount;

class UserAccountObserver
{
    /**
     * Handle the user account "created" event.
     *
     * @param  \App\Models\UserAccount  $userAccount
     * @return void
     */
    public function created(UserAccount $userAccount)
    {
        //
    }

    /**
     * Handle the user account "updated" event.
     *
     * @param  \App\Models\UserAccount  $userAccount
     * @return void
     */
    public function updated(UserAccount $userAccount)
    {
        //
    }

    /**
     * Handle the user account "deleted" event.
     *
     * @param  \App\Models\UserAccount  $userAccount
     * @return void
     */
    public function deleted(UserAccount $userAccount)
    {
        // $userAccount->userProfile()->delete();
        // $userAccount->posts->each->delete();
        // $userAccount->reportPosts()->delete();
        // $userAccount->reviews->each->delete();
        // $userAccount->socialAccounts()->delete();
        // \App\Models\Like::where('user_id', $userAccount->id)->delete();
        // $userAccount->carts->each->delete();
        // \App\Models\SavedPost::where('post_id')->delete();
        // $userAccount->comments->each->delete();
        // $userAccount->chatRoom->each->delete();
        // $userAccount->otp()->delete();
        // $userAccount->bank()->delete();
        // \App\Models\Notification::whereFromUserId($userAccount->id)->delete();
    }

    /**
     * Handle the user account "restored" event.
     *
     * @param  \App\Models\UserAccount  $userAccount
     * @return void
     */
    public function restored(UserAccount $userAccount)
    {
        // $userAccount->userProfile()->restore();
        // $userAccount->posts->each->restore();
        // $userAccount->reportPosts()->restore();
        // $userAccount->reviews->each->restore();
        // $userAccount->socialAccounts()->restore();
        // \App\Models\Like::where('user_id', $userAccount->id)->restore();
        // $userAccount->carts->each->restore();
        // \App\Models\SavedPost::where('post_id')->restore();
        // $userAccount->comments->each->restore();
        // $userAccount->chatRoom->each->restore();
        // $userAccount->otp()->restore();
        // $userAccount->bank()->restore();
    }

    /**
     * Handle the user account "force deleted" event.
     *
     * @param  \App\Models\UserAccount  $userAccount
     * @return void
     */
    public function forceDeleted(UserAccount $userAccount)
    {
        // $userAccount->userProfile()->forceDelete();
        // $userAccount->posts->each->forceDelete();
        // $userAccount->reportPosts()->forceDelete();
        // $userAccount->reviews->each->forceDelete();
        // $userAccount->socialAccounts()->forceDelete();
        // \App\Models\Like::where('user_id', $userAccount->id)->forceDelete();
        // $userAccount->carts->each->forceDelete();
        // \App\Models\SavedPost::where('post_id')->forceDelete();
        // $userAccount->comments->each->forceDelete();
        // $userAccount->chatRoom->each->forceDelete();
        // $userAccount->otp()->forceDelete();
        // $userAccount->bank()->forceDelete();
    }
}
