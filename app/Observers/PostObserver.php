<?php

namespace App\Observers;

use App\Models\Post;

class PostObserver
{
    /**
     * Handle the post "created" event.
     *
     * @param  \App\Models\post  $post
     * @return void
     */
    public function created(post $post)
    {
        //
    }

    /**
     * Handle the post "updated" event.
     *
     * @param  \App\Models\post  $post
     * @return void
     */
    public function updated(post $post)
    {
        //
    }

    /**
     * Handle the post "deleted" event.
     *
     * @param  \App\Models\post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        
    }

    /**
     * Handle the post "deleting" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleting(Post $post)
    {
        $post->item->delete();
        \App\Models\Like::where('post_id', $post->id)->delete();
        \App\Models\SavedPost::where('post_id')->delete();
        $post->chatRooms->each->delete();
        $post->comments->each->delete();
        $post->carts->each->delete();
        $post->poster->deleteApprovePostNotification($post);
        $post->poster->deleteLikeNotification($post);
    }
    /**
     * Handle the post "restored" event.
     *
     * @param  \App\Models\post  $post
     * @return void
     */
    public function restored(post $post)
    {
        // $post->item->restore();
        // \App\Models\Like::where('post_id', $post->id)->restore();
        // \App\Models\SavedPost::where('post_id')->restore();
        // $post->chatRooms->each->restore();
        // $post->comments->each->restore();
        // $post->carts->each->restore();
    }

    /**
     * Handle the post "force deleted" event.
     *
     * @param  \App\Models\post  $post
     * @return void
     */
    public function forceDeleted(post $post)
    {
        // $post->item->forceDelete();
        // \App\Models\Like::where('post_id', $post->id)->forceDelete();
        // \App\Models\SavedPost::where('post_id')->forceDelete();
        // $post->chatRooms->each->forceDelete();
        // $post->comments->each->forceDelete();
        // $post->carts->each->forceDelete();
        // $post->poster->deleteApprovePostNotification($post);
        // $post->poster->deleteLikeNotification($post);
    }
}
