<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{   
    protected $fillable = ['purchase_id', 'rating', 'body', 'reviewer_id', 'poster_id'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function($review) {
            $review->poster->deleteReviewNotification($review);
        });
    }

    public function purchase()
    {
        return $this->belongsTo(\App\Models\Purchase::class);
    }

    /**
     * Review on which cart to get user_id
     */
    public function cart()
    {
        return $this->hasOneThrough('App\Models\Cart',
                                    'App\Models\Purchase',
                                    'id',
                                    'id',
                                    'purchase_id',
                                    'cart_id'
                                );
    }

    // public function getReviewerIdAttribute()
    // {
    //     return $this->cart->user_id;
    // }

    public function reviewer()
    {
        return $this->belongsTo(\App\Models\UserAccount::class)->with('userProfile');
    }

    public function poster()
    {
        return $this->belongsTo(\App\Models\UserAccount::class);
    }
}
