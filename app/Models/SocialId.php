<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialId extends Model
{
    protected $fillable = ['social_id', 'social_type', 'user_id'];

    public function user() 
    {
        return $this->belongsTo('App\Models\UserAccount', 'user_id');
    }
}
