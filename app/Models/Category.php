<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Relations\Pivot;

class Category extends Model
{
    protected $fillable = ['main_category_id', 'sub_category_id'];

    public function items()
    {
        return $this->hasMany(\App\Models\Item::class);
    }

    public function mainCategory()
    {
        return $this->belongsTo(\App\Models\MainCategory::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(\App\Models\SubCategory::class);
    }
}
