<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    protected $fillable = ['name', 'image'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function($model) {
            $model->subCategory()->detach();
        });
    }

    public function subCategory()
    {
        return $this->belongsToMany(\App\Models\SubCategory::class, 'categories')
                    ->withTimestamps();
    }
}
