<?php

namespace App\Models;

use App\Traits\Followable;
use Neomarket\Notifications\Notifiable;
// use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserAccount extends Authenticatable implements JWTSubject, UserInterface
{
    use Followable, Notifiable;

    protected $guard = 'api';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_number', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'password_salt', 'password-resetable',
    ];

    protected $appends = ['avg_rating'];
    
        
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Route notifications for the FCM channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        return $this->firebase_token;
    }

    public function isAdmin()
    {
        return false;
    }
    
    public function userProfile()
    {
        return $this->hasOne(\App\Models\UserProfile::class, 'user_id');
    }

    public function chatRoom()
    {
        return $this->hasMany(\App\Models\ChatRoom::class, 'buyer_id');
    }

    public function likes()
    {
        return $this->belongsToMany(\App\Models\Post::class, 'likes', 'user_id', 'post_id')->withTimestamps();
    }

    public function savedPosts()
    {
        return $this->belongsToMany(\App\Models\Post::class, 'saved_posts', 'user_id', 'post_id')->withTimestamps();
    }

    public function posts()
    {
        return $this->hasMany(\App\Models\Post::class, 'poster_id');
    }

    public function carts()
    {
        return $this->hasMany(\App\Models\Cart::class, 'user_id');
    }

    public function reportPosts()
    {
        return $this->hasMany(\App\Models\ReportPost::class, 'reporter_id');
    }

    public function comments()
    {
        return $this->hasMany(\App\Models\Comment::class, 'user_id');
    }

    public function socialAccounts()
    {
        return $this->hasMany(\App\Models\SocialId::class, 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany(\App\Models\Review::class, 'poster_id');
    }

    /**
     * Own posted review
     */
    public function reviewed()
    {
        return $this->hasMany(\App\Models\Review::class, 'reviewer_id');
    }

    public function otp()
    {
        return $this->hasOne(\App\Models\Otp::class, 'user_id');
    }

    public function bank()
    {
        return $this->hasOne(\App\Models\BankAccount::class, 'user_id');
    }

    /**
     * Get user cart which is not removed and not purchased yet
     * 
     * @return Builder
     */
    public function availableCart()
    {
        return $this->carts()
                    ->where('bought', false)
                    ->where('removed', false);
    }

    public function purchasedCart()
    {
        return $this->carts()->purchased();
    }

    public function getAvgRatingAttribute()
    {
        return $this->reviews()->avg('rating') ?: 0;
    }

    public function path()
    {
        return url("user/{$this->id}");
    }

    public function isBlocked() : bool
    {
        return !! !$this->isActive;
    }

    public function addToCart(\App\Models\Post $post) 
    {
        return $this->carts()->create(['post_id' => $post->id]);
    }

    public function scopeActive($query)
    {
        return $query->where('isActive', true);
    }
}
