<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'date_of_birth', 'user_id', 'email', 'phone_verified', 'email_verified'
    ];

    protected $dates = ['date_of_birth'];

    public function user()
    {
        return $this->belongsTo('App\Models\UserAccount', 'user_id');
    }
}
