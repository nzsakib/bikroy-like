<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['seller_release', 'at_head_office', 'qc_passed', 'head_office_release', 'payment_method_id', 'delivery_type_id', 'shipping_address', 'transaction_id', 'price'];

    public function review()
    {
        return $this->hasOne(\App\Models\Review::class);
    }

    public function ownReview()
    {
        return $this->hasOne(\App\Models\Review::class)->where('reviewer_id', auth()->id());
    }

    public function cart()
    {
        return $this->belongsTo(\App\Models\Cart::class);
    }

    /**
     * Next level of joining in laravel 
     * Get the post related to this purchase
     * 
     * @return Model Post
     */
    public function post()
    {
        return $this->hasOneThrough(
            'App\Models\Post',
            'App\Models\Cart',
            'id',
            'id',
            'cart_id',
            'post_id'
        );
    }

    public function paymentMethod()
    {
        return $this->belongsTo(\App\Models\PaymentMethod::class);
    }

    public function deliveryType()
    {
        return $this->belongsTo(\App\Models\DeliveryType::class);
    }

    public static function countofLastSevenDays()
    {
        $date = Carbon::today()->subDays(7);
        return self::selectRaw('created_at, count(*) as sold')
            ->where('created_at', '>=', $date)
            ->where('created_at', '<', Carbon::today())
            ->groupBy(\DB::raw('day(created_at)'))->get();
    }

    public function buyer()
    {
        return $this->hasOneThrough(
            'App\Models\UserAccount',
            'App\Models\Cart',
            'id',
            'id',
            'cart_id',
            'user_id'
        );
    }
}
