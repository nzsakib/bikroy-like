<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportPost extends Model
{   
    protected $fillable = ['reporter_id', 'post_id', 'comment'];

    const SOLVED = 1;
    const PENDING = 0;

    public function user()
    {
        return $this->belongsTo(\App\Models\UserAccount::class, 'reporter_id');
    }

    public function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }

    public function scopePending($query)
    {
        return $query->where('status', self::PENDING);
    }
}
