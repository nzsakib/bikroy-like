<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class SavedPost extends Pivot
{
    protected $table = 'saved_posts';
    
    public $incrementing = true;

    public function user()
    {
        return $this->belongsTo('App\Models\UserAccount');
    }

    public function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }
}
