<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['post_id', 'bought', 'removed', 'user_id'];

    protected $casts = [
        'bought' => 'boolean',
        'removed' => 'boolean'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($cart) {
            $cart->purchase()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\UserAccount::class, 'user_id');
    }

    public function purchase()
    {
        return $this->hasOne(\App\Models\Purchase::class);
    }

    public function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }

    /**
     * Get item details of the post
     * 
     * @return Item
     */
    public function itemDetails()
    {
        return $this->post->item;
    }

    public function scopePurchased($query)
    {
        return $query->where('bought', true);
    }

    public function scopeNotPurchased($query)
    {
        return $query->where('bought', false);
    }
}
