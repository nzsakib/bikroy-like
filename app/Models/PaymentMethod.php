<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $fillable = ['name'];

    public function purchase()
    {
        return $this->hasMany(\App\Models\Purchase::class);
    }
}
