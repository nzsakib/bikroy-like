<?php

namespace App\Models;

use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $guarded = [];

    protected $touches = ['chatRoom'];

    public function chatRoom()
    {
        return $this->belongsTo('App\Models\ChatRoom');
    }

    public function scopeUnseen($query)
    {
        return $query->where('seen', false);
    }

    public function scopeNotSentByMe($query)
    {
        return $query->where('sender_id', '<>', auth()->id());
    }

    public function scopeReceived($query)
    {
        return $query->where('receiver_id', auth()->id());
    }

    public function setMessageAttribute($value)
    {
        $key = base64_decode(env('ENCRYPTION_KEY'));
        $newEncrypter = new Encrypter($key, Config::get('app.cipher'));
        
        $this->attributes['message'] = $newEncrypter->encrypt($value);
    }

    public function getMessageAttribute($value)
    {
        $key = base64_decode(env('ENCRYPTION_KEY'));
        $newEncrypter = new Encrypter($key, Config::get('app.cipher'));
        
        return $newEncrypter->decrypt($value);
    }
}
