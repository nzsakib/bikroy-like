<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne(\App\Models\UserAccount::class, 'id', 'user_id');
    }
}
