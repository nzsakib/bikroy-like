<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryType extends Model
{
    protected $fillable = ['name', 'delivery_scope', 'cost'];

    public function purchase()
    {
        return $this->hasMany(\App\Models\Purchase::class);
    }

}
