<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{   
    protected $fillable = ['post_id', 'comment', 'user_id'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($comment) {
            $comment->post->poster->deleteCommentNotification($comment);
            $comment->deleteMentionedNotification(); // forcedeleting notification
        });
    }
    
    public function user() 
    {
        return $this->belongsTo(\App\Models\UserAccount::class); 
    }

    public function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }

    public function mentionedUserNames()
    {
        preg_match_all('/@([^\s]+(?=(?:\.$)|\b))/', $this->comment, $matches);

        return $matches[1];
    }

    public function deleteMentionedNotification()
    {
        \App\Models\Notification::source($this)->whereType('YouWereMentioned')->delete();
    }
}
