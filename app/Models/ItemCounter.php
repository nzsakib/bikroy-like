<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCounter extends Model
{
    protected $fillable = ['item_id', 'count', 'sold'];

    public function item()
    {
        return $this->belongsTo('App\Models\Item');
    }
}