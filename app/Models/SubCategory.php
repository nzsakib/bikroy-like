<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = ['name', 'image'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function($model) {
            $model->mainCategory()->detach();
        });
    }

    public function mainCategory()
    {
        return $this->belongsToMany(\App\Models\MainCategory::class, 'categories')->withTimestamps();
    }
}
