<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\DatabaseNotificationCollection;

class Notification extends Model
{   
    protected $guarded = [];
    public $incrementing = false;

    protected $casts = [
        'data' => 'array',
        'read_at' => 'datetime',
    ];
    
    public function notifiable()
    {
        return $this->morphTo();
    }

    public function fromUser() 
    {
        return $this->belongsTo(\App\Models\UserAccount::class);
    }
    
    /**
     * Mark the notification as read.
     *
     * @return void
     */
    public function markAsRead()
    {
        if (is_null($this->read_at)) {
            $this->forceFill(['read_at' => $this->freshTimestamp()])->save();
        }
    }

    /**
     * Mark the notification as unread.
     *
     * @return void
     */
    public function markAsUnread()
    {
        if (! is_null($this->read_at)) {
            $this->forceFill(['read_at' => null])->save();
        }
    }

    /**
     * Determine if a notification has been read.
     *
     * @return bool
     */
    public function read()
    {
        return $this->read_at !== null;
    }

    /**
     * Determine if a notification has not been read.
     *
     * @return bool
     */
    public function unread()
    {
        return $this->read_at === null;
    }

    /**
     * Create a new database notification collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Notifications\DatabaseNotificationCollection
     */
    public function newCollection(array $models = [])
    {
        return new DatabaseNotificationCollection($models);
    }

    public function scopeSource($query, $model)
    {
        $type = (new \ReflectionClass($model))->getShortName();
        $id = $model->id;
        return $query->where('source_type', $type)->where('source_id', $id);
    }
}
