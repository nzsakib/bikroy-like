<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ChatRoom extends Model
{
    protected $fillable = ['post_id', 'buyer_id', 'source_type', 'buyer_archived', 'seller_archived'];

    public static function boot() 
    {
        parent::boot();

        static::deleting(function ($room) {
            $room->chatMessages()->delete();
        });

        static::addGlobalScope('unseenCount', function (Builder $builder) {
            $builder->withCount('myUnseenMessage');
        });
    }

    public function buyer()
    {
        return $this->belongsTo('App\Models\UserAccount', 'buyer_id');
    }

    public function chatMessages()
    {
        return $this->hasMany(\App\Models\ChatMessage::class);
    }

    public function myUnseenMessage()
    {
        return $this->chatMessages()->received()->unseen();
    }

    public function latestMessage()
    {
        return $this->hasOne(\App\Models\ChatMessage::class)->latest();
    }

    public function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }

    public function poster()
    {
        return $this->hasOneThrough('App\Models\UserAccount', 
                                    'App\Models\Post', 
                                    'id', 
                                    'id', 
                                    'post_id', 
                                    'poster_id');
    }

    public function scopeNotEmpty($query)
    {
        return $query->has('chatMessages', '>', 0);
    }

    public function getOtherPersonNameAttribute()
    {
        if ($this->buyer_id == auth()->id()) {
            return $this->post->poster->userProfile->name;
        } else {
            return $this->buyer->userProfile->name;
        }
    }
}
