<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['tag_name'];

    public function items()
    {
        return $this->belongsToMany('App\Models\Item');
    }
}
