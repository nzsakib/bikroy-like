<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{   
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(\App\Models\UserAccount::class);
    }

    public function scopeUnused($query)
    {
        return $query->where('used', false);
    }
}
