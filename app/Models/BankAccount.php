<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{   
    protected $fillable = ['bank_name', 'branch_name', 'account_name', 'account_number', 'routing_number'];
    
    public function user()
    {
        return $this->belongsTo(\App\Models\UserAccount::class);
    }
}
