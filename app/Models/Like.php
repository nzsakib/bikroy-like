<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Like extends Pivot
{
    protected $table = 'likes';
    
    public $incrementing = true;

    public function user()
    {
        return $this->belongsTo('App\Models\UserAccount');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
