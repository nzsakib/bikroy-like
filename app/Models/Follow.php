<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    public function follower()
    {
        return $this->belongsto(\App\Models\UserAccount::class, 'follower_id');
    }

    public function following()
    {
        return $this->belongsTo(\App\Models\UserAccount::class, 'following_id');
    }
}
