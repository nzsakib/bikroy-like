<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemGallery extends Model
{
    protected $fillable = ['image_link', 'item_id'];

    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }
}
