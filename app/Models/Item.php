<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price', 'heading', 'caption', 'address', 'delivery_type_id', 'payment_method_id', 'category_relation_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'delivery_type_id', 'payment_method_id'
    ];

    public static function boot() 
    {
        parent::boot();

        static::deleting(function ($item) {
            $item->itemCounter()->delete();
            $item->tags()->delete();
            $item->images()->delete();
        });
    }

    public function itemCounter()
    {
        return $this->hasOne(\App\Models\ItemCounter::class);
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
     * Pivot table for storing tags
     */
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class, 'tag_item')->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    /**
     * Add item quantity 
     * 
     * @return ItemCounter
     */
    public function addQuantity($quantity, $sold = 0)
    {
        return $this->itemCounter()->create([
            'count' => $quantity,
            'sold' => $sold
        ]);
    }

    public function images()
    {
        return $this->hasMany(\App\Models\ItemGallery::class);
    }
    /**
     * Get total quantity of the item as property
     * 
     * @return int
     */
    public function getTotalQuantityAttribute()
    {
        return $this->itemCounter->count;
    }

    /**
     * Get sold count of the item as property
     * 
     * @return int
     */
    public function getSoldAttribute()
    {
        return $this->itemCounter->sold;
    }

    /**
     * Get available quantity as property
     * 
     * @return int
     */
    public function getAvailableQuantityAttribute()
    {
        return $this->itemCounter->count - $this->itemCounter->sold;
    }

    /**
     * Get Gallary Image for a item as array 
     * 
     * @return array
     */
    public function getImagesArrayAttribute()
    {
        return $this->images->reverse()->pluck('image_link');
    }

}
