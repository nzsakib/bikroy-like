<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Enum\PostStatus;

class Post extends Model
{

    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_SOLD = 2;
    const STATUS_BLOCKED = 3;
    const STATUS_USER_BANNED = 4;

    protected $with = ['poster.userProfile', 'item'];

    protected $fillable = ['poster_id', 'item_id', 'featured', 'status'];

    protected $appends = ['is_saved', 'is_liked'];

    public static function boot()
    {
        parent::boot();

        // static::deleting(function ($post) {
        //         $post->item->delete();
        //         \App\Models\Like::where('post_id', $post->id)->delete();
        //         \App\Models\SavedPost::where('post_id')->delete();
        //         $post->chatRooms->each->delete();
        //         $post->comments->each->delete();
        //         $post->carts->each->delete();
        //         // $post->poster->deleteApprovePostNotification($post);
        //         // $post->poster->deleteLikeNotification($post);

        // });
    }

    public function poster()
    {
        return $this->belongsTo(\App\Models\UserAccount::class, 'poster_id');
    }

    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    public function carts()
    {
        return $this->hasMany(\App\Models\Cart::class, 'post_id');
    }

    public function reportPosts()
    {
        return $this->hasMany(\App\Models\ReportPost::class);
    }

    public function likes()
    {
        return $this->belongsToMany(\App\Models\UserAccount::class, 'likes', 'post_id', 'user_id')
            ->using(\App\Models\Like::class)
            ->withPivot('id', 'deleted_at')
            ->where('isActive', true)
            ->withTimestamps();
    }

    public function chatRooms()
    {
        return $this->hasMany(\App\Models\ChatRoom::class, 'post_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(\App\Models\Comment::class);
    }

    public function savedPosts()
    {
        return $this->belongsToMany(\App\Models\UserAccount::class, 'saved_posts', 'post_id', 'user_id')
            ->using(\App\Models\SavedPost::class)
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    /**
     * Fetch if the post is saved by authenticated user
     * 
     * @return bool
     */
    public function isSaved()
    {
        return !!$this->savedPosts()->where('user_id', auth()->guard('api')->id())->exists();
    }

    /**
     * Get saved status as property
     * 
     * @return bool
     */
    public function getIsSavedAttribute()
    {
        if (auth()->guard('api')->check()) {
            return $this->isSaved();
        }

        return false;
    }

    public function isLiked()
    {
        return !!$this->likes()->where('user_id', auth()->guard('api')->id())->exists();
    }

    public function getIsLikedAttribute()
    {
        if (auth()->check()) {
            return $this->isLiked();
        }

        return false;
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function path()
    {
        return url("posts/{$this->id}");
    }

    // Filter Posts that are not sold out
    public function scopeAvailable($query)
    {
        return $query->whereHas('item.itemCounter', function ($q) {
            $q->whereRaw('sold < count');
        });
    }

    public function scopeActive($query)
    {
        return $query->where('status', PostStatus::active);
    }

    public function scopeSold($query)
    {
        return $query->where('status', PostStatus::sold);
    }

    public function scopeBlocked($query)
    {
        return $query->where('status', PostStatus::block);
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    public function scopeUserBanned($query)
    {
        return $query->where('status', self::STATUS_USER_BANNED);
    }
}
