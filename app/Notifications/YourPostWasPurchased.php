<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Notifications\Firebase;
use App\Enum\DBNotificationType;
use Neomarket\Channels\DBChannel;
use Neomarket\Channels\FirebaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class YourPostWasPurchased extends Notification
{
    use Queueable;

    protected $user;
    protected $purchase;
    public $source;
    public $from;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $post, $purchase)
    {
        $this->user = $user;
        $this->post = $post;
        $this->purchase = $purchase;
        $this->source = $purchase;
        $this->from = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FirebaseChannel::class, DBChannel::class];
    }

    public function toFirebase($notifiable)
    {
        $content = [
            'body' => "Your item was purchased by {$this->user->userProfile->name}.",
        ];
        $firebase = new Firebase();
        
        return $firebase->to($notifiable)->prepare($content)->send();
    }

    public function toDatabase($notifiable)
    {
        return [
            'message' => 'Your item was purchased by $NAME$.',
            'source_id' => $this->purchase->id,
            'source_type' => DBNotificationType::SOLD,
            'link' => $this->post->path(),
        ];
    }
}
