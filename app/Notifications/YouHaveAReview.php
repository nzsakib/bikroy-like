<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Notifications\Firebase;
use App\Enum\DBNotificationType;
use Neomarket\Channels\DBChannel;
use Neomarket\Channels\FirebaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class YouHaveAReview extends Notification
{
    use Queueable;

    protected $user;
    public $source;
    public $from;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $review)
    {
        $this->user = $user;
        $this->source = $review;
        $this->from = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FirebaseChannel::class, DBChannel::class];
    }

    public function toFirebase($notifiable)
    {
        $content = [
            'body' => $this->user->userProfile->name . ' posted a review about your item.',
        ];
        $firebase = new Firebase();
        
        return $firebase->to($notifiable)->prepare($content)->send();
    }

    public function toDatabase($notifiable)
    {
        return [
            'message' => '$NAME$ posted a review about your item.',
            'source_id' => $notifiable->id,
            'source_type' => DBNotificationType::REVIEW,
            'link' => $this->user->path(),
        ];
    }
}
