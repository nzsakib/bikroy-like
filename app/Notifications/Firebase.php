<?php 
namespace App\Notifications;

use App\Enum\DeviceType;
use App\Enum\PushNotificationClickAction;

class Firebase
{
    protected $to; // Firebase token
    protected $device_type;

    private $message;

    public function __construct()
    {
        $this->key = env('FIREBASE_KEY');
    }

    public function to($notifiable)
    {
        $this->to = $notifiable->firebase_token;
        $this->device_type = $notifiable->device_type;
        
        return $this;
    }

    /**
     * Prepare the message to be sent
     *
     * @param array $content
     * @return self
     */
    public function prepare(array $content, array $extraData = [])
    {
        $title = isset($content['title']) ? $content['title'] : 'NeoMarket';
        $body = $content['body'];
        $this->message = [
            'registration_ids' => [$this->to],
            "alert" => [
                "title" => $title,
                "body" => $body
            ],
            'data' => [
                "title" => $title,
                "body" => $body,
                "sound" => '',
                'icon' => '',
                'click_action' => isset($content['click_action']) ? $content['click_action'] : PushNotificationClickAction::NOTFICATION_PANEL,
                'is_background' => false,
                'timestamp' => now(),
            ],
        ];

        if (!empty($extraData)) {
            $this->message['data'] = array_merge($this->message['data'], $extraData);
        }
        
        if ($this->device_type == DeviceType::IPHONE) {
            $this->message['notification'] = [
                "title" => $title,
                "body" => $body,
                "badge" => 0,
                "priority"=>"high",
                "sound"=> "note"
            ];
        }

        return $this;
    }

    /**
     * Send the payload to firebase and get the response
     *
     * @return response
     */
    public function send()
    {
        $ch = curl_init();
        $headers = [
            'Content-Type:application/json',
            'Authorization:key=' . $this->key
        ];
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => json_encode($this->message)
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function test($token)
    {
        $this->to = $token;
        $this->device_type = DeviceType::IPHONE;

        return $this;
    }
}