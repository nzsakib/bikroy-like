<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Benwilkins\FCM\FcmMessage;
use Neomarket\Channels\FirebaseChannel;
use App\Enum\PushNotificationClickAction;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class YouHaveNewMessage extends Notification
{
    use Queueable;

    public $message;
    public $sender;
    public $chatRoom;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sender, $message, $chatRoom)
    {
        $this->sender = $sender;
        $this->message = $message;
        $this->chatRoom = $chatRoom;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FirebaseChannel::class];
    }

    public function toFirebase($notifiable)
    {
        $content = [
            'title' => $this->sender->userProfile->name,
            'body' => 'You have a new message.',
            'click_action' => PushNotificationClickAction::CHAT
        ];
        $extraData = [
            'chat' => [
                'id' => $this->message->id,
                'message' => $this->message->message,
                'created_at' => (string) $this->message->created_at,
                'isOwnMessage' => false,
                'seen' => 0
            ],
            'chatroom' => [
                'chat_room_id' => $this->chatRoom->id,
                'product_image' => $this->chatRoom->post->item->images_array[0],
                'seller_name' => $this->chatRoom->post->poster->userProfile->name,
                'item_heading' => $this->chatRoom->post->item->heading,
                'last_message' => $this->chatRoom->latestMessage->message,
                'last_message_time' => (string) $this->chatRoom->latestMessage->created_at,
                'unseenMessageCount' => $this->chatRoom->my_unseen_message_count
            ],
        ];

        $firebase = new Firebase();

        return $firebase->to($notifiable)
                    ->prepare($content, $extraData)
                    ->send();
    }
}
