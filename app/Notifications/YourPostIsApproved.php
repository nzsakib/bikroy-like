<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Enum\DBNotificationType;
use Neomarket\Channels\DBChannel;
use Neomarket\Channels\FirebaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class YourPostIsApproved extends Notification
{
    use Queueable;

    public $post;
    public $source;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $this->post = $post;
        $this->source = $post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FirebaseChannel::class, DBChannel::class];
    }

    public function toFirebase($notifiable)
    {
        $content = [
            'title' => 'Neo Market Admin',
            'body' => 'Your product has been approved. Click to view more.',
        ];
        $firebase = new Firebase();

        return $firebase->to($notifiable)->prepare($content)->send();
    }

    public function toDatabase($notifiable)
    {
        return [
            'message' => 'Your product has been approved. Click to view more.',
            'profile_image' => 'https://shopitbd.s3.ap-south-1.amazonaws.com/profile/neomarket.png',
            'source_id' => $this->post->id,
            'source_type' => DBNotificationType::POST,
            'link' => $this->post->path(),
        ];
    }
}
