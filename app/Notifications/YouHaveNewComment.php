<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Notifications\Firebase;
use App\Enum\DBNotificationType;
use Neomarket\Channels\DBChannel;
use Neomarket\Channels\FirebaseChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class YouHaveNewComment extends Notification
{
    use Queueable;

    protected $user;
    protected $post;
    public $source;
    public $from;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $post, $comment)
    {
        $this->user = $user;
        $this->post = $post;
        $this->source = $comment;
        $this->from = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FirebaseChannel::class, DBChannel::class];
    }

    public function toFirebase($notifiable)
    {
        $content = [
            'body' => $this->user->userProfile->name . ' commented on your post.',
        ];
        $firebase = new Firebase();
        
        return $firebase->to($notifiable)->prepare($content)->send();
    }

    public function toDatabase($notifiable)
    {
        return [
            'message' => '$NAME$ commented on your post.',
            'source_id' => $this->post->id,
            'source_type' => DBNotificationType::POST,
            'link' => $this->post->path(),
        ];
    }
}
