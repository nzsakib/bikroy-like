<?php namespace App\Helpers;

use Illuminate\Validation\Validator;

class AuthResponse 
{
    public static function unauthorized()
    {
        return response([
            'error' => __('You do not have correct permission.')
        ], 403);
    }

    public static function validationError(Validator $validator) 
    {
        return response([
            'error' => $validator->messages()->first()
        ], 400);
    }

    public static function validationErrString(string $message, $code = 400)
    {
        return response([
            "error" => $message
        ], $code);
    }
}