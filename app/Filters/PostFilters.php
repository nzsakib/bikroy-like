<?php 

namespace App\Filters;

use App\User;

class PostFilters extends Filters
{
    protected $filters = ['used', 'min', 'max', 'popular', 'price', 'category'];

	protected function used($value)
	{
		$value = boolval($value);

		return $this->builder->whereHas('item', function($query) use($value) {
			$query->where('used', $value);
		});
	}

	protected function max($price)
	{
		return $this->builder->whereHas('item', function($query) use($price) {
			$query->where('price', '<=', $price);
		});
	}

	protected function min($price)
	{
		return $this->builder->whereHas('item', function($query) use($price) {
			$query->where('price', '>=', $price);
		});
	}


	protected function popular($value)
	{
		$value = boolval($value);
		if ($value == true) {
			return $this->builder->orderBy('likes_count', 'DESC');
		}

		return $this->builder;
	}

	protected function price($orderBy)
	{
		$orderBy = strtolower($orderBy);		
		if ($orderBy === 'asc') {
			// choto theke boro
			return $this->builder->leftJoin('items', 'posts.item_id', '=', 'items.id')->orderBy('price', 'asc');
			
		} else if ($orderBy === 'desc'){
			// boro theke choto 
			return $this->builder->leftJoin('items', 'posts.item_id', '=', 'items.id')->orderBy('price', 'desc');
		}

		return $this->builder;

	}

	public function category($category)
	{
		return $this->builder
					->whereHas('item.category', function($query) use($category) {
						$query->where('main_category_id', $category);
		});
	}

}