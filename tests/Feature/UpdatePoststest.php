<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Enum\PostStatus;

class UpdatePoststest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_user_can_update_their_own_post()
    {
        $this->signIn();

        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        $category = create('App\Models\Category');
        $data = make('App\Models\Item')->toArray();
        $data['tags'] = [create('App\Models\Tag')];
        $data['item_quantity'] = $post->item->itemCounter->count;
        $data = array_merge($data, $category->toArray());
        
        $this->patch('/api/posts/' . $post->id, $data)
            ->assertStatus(200);

        $this->get('/api/posts/' . $post->id)
            ->assertStatus(200)
            ->assertJsonFragment([
                'item_heading' => $data['heading']
            ]);
    }

    /** @test */
    public function unauthorized_users_can_not_update_any_post()
    {
        $post = create('App\Models\Post');

        $this->patch('/api/posts/' . $post->id, [])
            ->assertStatus(401);
    }

    /** @test */
    public function authorized_users_can_not_update_other_users_post()
    {
        $this->signIn();

        $post = create('App\Models\Post');

        $this->patch('/api/posts/' . $post->id, [])
            ->assertStatus(400);
    }

    /** @test */
    public function authorized_user_can_not_update_sold_post()
    {
        $this->signIn();

        $post = create('App\Models\Post', 
                [
                    'poster_id' => auth()->id(), 
                    'status' => PostStatus::sold
                ]);
        $r = $this->patch('/api/posts/' . $post->id, [])
                ->assertStatus(403);
    }
}
