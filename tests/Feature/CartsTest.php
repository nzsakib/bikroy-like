<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use App\Enum\PostStatus;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_user_can_add_post_to_cart()
    {
        $this->signIn();

        $post = create('App\Models\Post');

        $this->post('/api/cart', ['post_id' => $post->id])
            ->assertStatus(200);

        $this->get('/api/cart')
            ->assertJson([
                [
                    'post_id' => $post->id
                ]
            ]);
    }

    /** @test */
    public function can_not_add_to_cart_inactive_post()
    {
        $this->signIn();
        $post = create('App\Models\Post', ['status' => Post::STATUS_PENDING]);
        $this->post('/api/cart', ['post_id' => $post->id])
            ->assertStatus(404);
        $this->assertDatabaseMissing('carts', ['post_id' => $post->id]);

        $post = create('App\Models\Post', ['status' => Post::STATUS_SOLD]);
        $this->post('/api/cart', ['post_id' => $post->id])
            ->assertStatus(404);
        $this->assertDatabaseMissing('carts', ['post_id' => $post->id]);

        $post = create('App\Models\Post', ['status' => Post::STATUS_BLOCKED]);
        $this->post('/api/cart', ['post_id' => $post->id])
            ->assertStatus(404);

        $this->assertDatabaseMissing('carts', ['post_id' => $post->id]);
    }

    /** @test */
    public function  authorized_user_can_delete_post_from_cart()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $cart = create('App\Models\Cart', ['user_id' => auth()->id(), 'bought' => false]);

        $this->delete('/api/cart/' . $cart->id)
            ->assertSuccessful();
        $this->assertDatabaseHas('carts', ['id' => $cart->id, 'removed' => true]);
    }

    /** @test */
    public function authorized_user_can_purchase_a_cart()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $cart = create('App\Models\Cart', [
            'user_id' => auth()->id(),
            'bought' => false,
            'removed' => false
        ]);
        $data = [
            'payment_method_id' => 1,
            'delivery_type_id' => 1,
            'shipping_address' => 'usa',
            'cart_id' => $cart->id,
            'transaction_id' => 'SHOPIT_8e3fa505138c71ec567ab95b3a2f3fa2'
        ];
        $this->post('/api/cart/purchase', $data)
            ->assertSuccessful();
        $this->assertDatabaseHas('carts', [
            'id' => $cart->id,
            'bought' => true
        ]);
        $this->assertDatabaseHas('purchases', [
            'cart_id' => $cart->id,
            'transaction_id' => $data['transaction_id']
        ]);
    }

    /** @test */
    public function authorized_user_can_view_purchased_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $cart = create('App\Models\Cart', [
            'user_id' => auth()->id(),
            'bought' => false,
            'removed' => false
        ]);
        create('App\Models\Purchase', ['cart_id' => $cart->id]);

        $response = $this->get('/api/purchased')
            ->assertSuccessful();

        $result = json_decode($response->getContent())->data;
        $this->assertCount(1, $result);
        $this->assertCount(17, (array) $result[0]);
    }

    /** @test */
    public function unauthorized_user_can_not_view_purchased_post()
    {
        $this->get('/api/purchased')
            ->assertStatus(401);
    }

    /** @test */
    public function seller_can_not_add_own_product_to_cart()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);

        $this->post('/api/cart', ['post_id' => $post->id])
            ->assertStatus(400);
    }
}
