<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_user_can_post_a_comment_on_a_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post');
        $data = [
            'comment' => 'A new comment'
        ];
        $this->post('/api/posts/' . $post->id . '/comment', $data)
            ->assertSuccessful();
        $this->assertDatabaseHas('comments', [
            'user_id' => auth()->id(),
            'post_id' => $post->id,
            'comment' => $data['comment']
        ]);
    }

    /** @test */
    public function user_can_see_all_comments_of_a_post()
    {
        $this->withoutExceptionHandling();
        $post = create('App\Models\Post');
        $comment = create('App\Models\Comment', ['post_id' => $post->id]);
        $comment2 = create('App\Models\Comment', ['post_id' => $post->id]);
        
        $response = $this->get('/api/posts/' . $post->id . '/comment')
            ->assertSee($comment->comment)
            ->assertSee($comment2->comment);
        
        $result = json_decode($response->getContent())->data;

        $this->assertCount(2, $result);
    }

    /** @test */
    public function authorized_user_can_delete_a_comment()
    {
        $this->signIn();
        $comment = create('App\Models\Comment', ['user_id' => auth()->id()]);
        $this->delete('/api/comment/' . $comment->id)
            ->assertSuccessful();
        $this->assertDatabaseMissing('comments', ['id' => $comment->id]);
    }

    /** @test */
    public function unauthorized_user_can_not_delete_any_comment()
    {
        $this->delete('/api/comment/1')
            ->assertStatus(401);
    }

    /** @test */
    public function authorized_user_can_not_delete_other_users_comment()
    {
        $this->signIn();
        $comment = create('App\Models\Comment');
        $this->delete('/api/comment/' . $comment->id)
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_update_their_comment()
    {
        $this->signIn();
        $comment = create('App\Models\Comment', ['user_id' => auth()->id()]);
        $data = ['comment' => 'Updated Comment'];
        $this->patch('/api/comment/' . $comment->id, $data)
            ->assertSuccessful();
        $this->assertDatabaseHas('comments', [
            'id' => $comment->id, 
            'comment' => $data['comment']
        ]);
    }

    /** @test */
    public function authorized_user_can_not_update_other_user_comments()
    {
        $this->signIn();
        $comment = create('App\Models\Comment');
        $data = ['comment' => 'Updated Comment'];
        $this->patch('/api/comment/' . $comment->id, $data)
            ->assertStatus(403);
    }

    /** @test */
    public function unauthorized_user_cannot_update_any_comment()
    {
        $this->patch('/api/comment/1')
            ->assertStatus(401);
    }

    /** @test */
    public function user_gets_unique_mention_user_suggesion_in_comment()
    {
        $this->withoutExceptionHandling();

        $this->signIn();
        
        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id]);
        
        // owner same
        $post = create('App\Models\Post', ['poster_id' => $user->id]);
        // commenter same 
        // mentioned same user / myself
        create('App\Models\Comment', [
            'post_id' => $post->id, 
            'user_id' => $user->id, 
            'comment' => "@{$user->userProfile->username}"
        ]);

        // follows the same user that commented
        auth()->user()->follow($user);

        $response = $this->get("/api/posts/{$post->id}/mention")->assertSuccessful();
        $result = json_decode($response->getContent());

        $this->assertCount(1, $result);
    }
}
