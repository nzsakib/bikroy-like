<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WishListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_user_can_save_a_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post');

        $this->post('/api/wishlist', ['post_id' => $post->id])
            ->assertSuccessful();
        $this->assertDatabaseHas('saved_posts', [
            'user_id' => auth()->id(),
            'post_id' => $post->id
        ]);
    }

    /** @test */
    public function unauthorized_user_can_not_save_anything()
    {
        $this->post('/api/wishlist', ['post_id' => 1])
            ->assertStatus(401);
    }
    /** @test */
    public function authorized_user_can_not_save_invalid_post()
    {
        $this->signIn();

        $this->post('/api/wishlist', ['post_id' => 1])
            ->assertStatus(404);
        $this->assertDatabaseMissing('saved_posts', [
            'user_id' => auth()->id(),
            'post_id' => 1
        ]);
    }

    /** @test */
    public function authorized_users_can_delete_a_saved_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post');
        $user = auth()->user();
        $user->savedPosts()->attach($post);
        
        $this->delete('/api/wishlist', ['post_id' => $post->id])
            ->assertSuccessful();
        $this->assertDatabaseMissing('saved_posts', [
            'user_id' => $user->id,
            'post_id' => $post->id
        ]);
    }

    /** @test */
    public function authorized_user_can_see_all_saved_posts()
    {
        $this->signIn();
        $post = create('App\Models\Post');
        $post2 = create('App\Models\Post');
        $user = auth()->user();
        $user->savedPosts()->attach($post);
        $user->savedPosts()->attach($post2);

        $response = $this->get('/api/wishlist')
                        ->assertSuccessful();
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(2, $result);
    }

}
