<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReadPostsTest extends TestCase
{
    use RefreshDatabase;

    protected $post; 

    public function setUp() : void
    {
        parent::setUp();

        $this->post = create('App\Models\Post');
    }

    /** @test */
    public function a_user_can_read_all_posts()
    {
        $this->get('/api/posts')
            ->assertSee($this->post->item->header);
    }

    /** @test */
    public function a_user_can_read_a_single_post()
    {
        $this->withoutExceptionHandling();
        
        $this->get('/api/posts/' . $this->post->id)
            ->assertStatus(200)
            ->assertSee($this->post->item->header);
    }

    /** @test */
    public function a_user_may_see_sold_post()
    {
        $post = create('App\Models\Post', ['status' => 2]);

        $this->get('/api/posts/' . $post->id)
            ->assertStatus(200);
    }
}
