<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use function GuzzleHttp\json_decode;
use App\Models\UserAccount;

class UserLoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_login_with_phone_and_password()
    {
        $this->withoutExceptionHandling();
        create('App\Models\UserAccount', [
            'phone_number' => '+8801911111111',
            'password' => Hash::make('password')
        ]);
        $data = [
            'phone_number' => '+8801911111111',
            'password' => 'password',
        ];
        $response = $this->post('/api/login', $data)
            ->assertStatus(200)
            ->assertSee('token');
        
        $result = json_decode($response->content());

        $this->get('/api/user', ['HTTP_Authorization' => 'Bearer ' . $result->token])
            ->assertSuccessful();
    }

    /** @test */
    public function user_can_login_using_social_id()
    {
        $data = ['social_id' => '123456', 'social_type' => 'facebook'];
        create('App\Models\SocialId', $data);
        
        $response = $this->post('/api/login', $data)
                        ->assertSuccessful()
                        ->assertSee('token');
        $result = json_decode($response->content());

        $this->get('/api/user', ['HTTP_Authorization' => 'Bearer ' . $result->token])
            ->assertSuccessful();
    }

    /** @test */
    public function a_user_can_refresh_jwt_token()
    {
        $this->withoutExceptionHandling();
        create('App\Models\UserAccount', ['phone_number' => '01917169306']);
        $token = JWTAuth::attempt(['phone_number' => '01917169306', 'password' => 'Hello1234']);
        
        $response =$this->get('/api/token/refresh', ['HTTP_Authorization' => 'Bearer ' . $token])
                ->assertSuccessful();
        $result = json_decode($response->content());
        $this->get('/api/user', ['HTTP_Authorization' => 'Bearer ' . $token])
            ->assertStatus(400);

        $this->get('/api/user', ['HTTP_Authorization' => 'Bearer ' . $result->token])
            ->assertSuccessful();
    }

    /** @test */
    public function a_guest_can_register()
    {
        $data = [
            'phone_number' => '+8801911111111',
            'password' => 'secret20',
            'email' => 'john@doe.com',
            'password_confirmation' => 'secret20',
            'name' => 'John Doe',
        ];

        $response = $this->post('/api/register', $data)
                        ->assertSuccessful()
                        ->assertSee('token');
        
        $this->assertDatabaseHas('user_accounts', ['phone_number' => $data['phone_number']]);
        $this->assertDatabaseHas('user_profiles', ['name' => $data['name']]);
    }

    /** @test */
    public function authorized_user_can_change_current_password()
    {
        // $this->withoutExceptionHandling();
        $user = create('App\Models\UserAccount', ['password' => Hash::make('secret20')]);

        $this->signIn($user);
        $data = [
            'current_password' => 'secret20',
            'new_password' => 'secret20',
            'new_password_confirmation' => 'secret20'
        ];
        
        $this->post('/api/password/change', $data)
                ->assertSuccessful();
                
        $this->assertTrue(Hash::check($data['new_password'], $user->fresh()->password));
    }

    /** @test */
    public function unauthorized_user_can_not_change_password()
    {
        $data = [
            'current_password' => 'secret20',
            'new_password' => 'secret20',
            'new_password_confirmation' => 'secret20'
        ];
        $this->post('/api/password/change', $data)
            ->assertStatus(401);
    }

    /** @test */
    public function banned_user_will_not_proceed()
    {
        $user = create('App\Models\UserAccount', ['isActive' => false]);
        $this->be($user);
        $this->get('/api/posts')->assertStatus(401);
        $this->get('/api/chatroom/all')->assertStatus(401);
    }

    /** @test */
    public function banned_user_can_not_login()
    {
        create('App\Models\UserAccount', [
            'phone_number' => '+8801911111111',
            'password' => Hash::make('password'),
            'isActive' => false,
        ]);
        $data = [
            'phone_number' => '+8801911111111',
            'password' => 'password',
        ];
        $this->post('/api/login', $data)
            ->assertStatus(418);
    }

    /** @test */
    public function user_email_and_phone_number_must_be_unique_during_register()
    {
        $this->withoutExceptionHandling();
        $data = [
            'email' => 'test@test.com',
            'phone_number' => '+8801911111111',
        ];

        $this->post('/api/check/email-mobile', $data)
            ->assertStatus(200);
        
        create('App\Models\UserAccount', ['phone_number' => $data['phone_number']]);
        $this->post('/api/check/email-mobile', ['phone_number' => $data['phone_number']])
            ->assertStatus(400);
        
        create('App\Models\UserProfile', ['email' => $data['email']]);
        $this->post('/api/check/email-mobile', ['email' => $data['email']])
            ->assertStatus(400);

        $this->post('/api/check/email-mobile', [])
            ->assertStatus(400);
    }
}
