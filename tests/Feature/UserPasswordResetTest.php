<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Mail\ResetPasswordNotification;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Hash;

class UserPasswordResetTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_receive_password_reset_email()
    {
        $this->withoutExceptionHandling();
        Mail::fake();
        $data = [
            'email' => 'test@email.com'
        ];
        create('App\Models\UserProfile', ['email' => $data['email']]);
        $this->post('/api/password/email', $data)
            ->assertStatus(200);
        Mail::assertSent(ResetPasswordNotification::class, function ($mail) use ($data) {
            return $mail->hasTo($data['email']);
        });
    }

    /** @test */
    public function user_can_reset_password_from_email_link()
    {
        $this->withoutExceptionHandling();
        $user = create('App\Models\UserAccount');
        $token = str_random(40);
        PasswordReset::create([
            'user_id' => $user->id,
            'token' => $token,
            'created_at' => null,
        ]);
        $data = [
            'token' => $token,
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $this->post('/api/password/reset', $data)
            ->assertStatus(200);
        $this->assertTrue(Hash::check($data['password'], $user->fresh()->password));
        $this->assertDatabaseMissing('password_resets', [
            'token' => $token
        ]);
    }
}
