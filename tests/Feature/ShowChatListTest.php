<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowChatListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_view_all_buying_chat_room()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        create('App\Models\ChatMessage', ['chat_room_id' => $room->id]);
        
        $room2 = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        create('App\Models\ChatMessage', ['chat_room_id' => $room2->id]);
        
        $response = $this->get('/api/chatroom/buying')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(2, $result);
        $this->assertEquals($result[0]->other_person, $room->poster->userProfile->name);
    }

    /** @test */
    public function empty_chat_room_will_not_be_in_the_list()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $response = $this->get('/api/chatroom/buying');
        $result = json_decode($response->getContent())->data;
        $this->assertCount(0, $result);

        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        create('App\Models\ChatRoom', ['post_id' => $post->id]);
        $response = $this->get('/api/chatroom/selling');
        $result = json_decode($response->getContent())->data;
        $this->assertCount(0, $result);

        $response = $this->get('/api/chatroom/all');
        $result = json_decode($response->getContent())->data;
        $this->assertCount(0, $result);
    }

    /** @test */
    public function user_can_view_all_selling_chat_room()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        $room = create('App\Models\ChatRoom', ['post_id' => $post->id]);
        create('App\Models\ChatMessage', ['chat_room_id' => $room->id]);
        
        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        $room2 = create('App\Models\ChatRoom', ['post_id' => $post->id]);
        create('App\Models\ChatMessage', ['chat_room_id' => $room2->id]);
        
        $response = $this->get('/api/chatroom/selling')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(2, $result);
        $this->assertEquals($result[0]->other_person, $room->buyer->userProfile->name);
    }

    /** @test */
    public function user_can_see_all_chat_rooms_including_buying_and_selling()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        // Selling 
        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        $sellRoom = create('App\Models\ChatRoom', ['post_id' => $post->id]);
        create('App\Models\ChatMessage', ['chat_room_id' => $sellRoom->id]);

        // Buying 
        $buyRoom = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        create('App\Models\ChatMessage', ['chat_room_id' => $buyRoom->id]);

        $response = $this->get('/api/chatroom/all')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(2, $result);
        $this->assertEquals($result[0]->other_person, $sellRoom->buyer->userProfile->name);
        $this->assertEquals($result[1]->other_person, $buyRoom->poster->userProfile->name);
    }

    /** @test */
    public function user_should_see_unseen_message_count_with_chatroom()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $data = [ 'chat_room_id' => $room->id, 'receiver_id' => auth()->id() ];
        
        create('App\Models\ChatMessage', $data); // other user sent message
        create('App\Models\ChatMessage', $data); // other user sent message
        create('App\Models\ChatMessage', [
            'chat_room_id' => $room->id,
            'sender_id' => auth()->id()
        ]); // I sent a message

        $response = $this->get('/api/chatroom/buying');
        $result = json_decode($response->getContent())->data;
        $unseenMessageCount = $result[0]->unseenMessageCount;
        $this->assertEquals(2, $unseenMessageCount);
    }

    /** @test */
    public function chat_rooms_will_be_order_by_latest_message_timestamp()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        // Buying chat rooms
        $shirt = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        create('App\Models\ChatMessage', ['chat_room_id' => $shirt->id]);
        $shirt->updated_at = Carbon::yesterday();
        $shirt->save();

        $pant = create('App\Models\ChatRoom', ['buyer_id' => auth()->id(), 'updated_at' => Carbon::yesterday()]);
        create('App\Models\ChatMessage', ['chat_room_id' => $pant->id, 'created_at' => now()->addMinutes(10)]);

        $response = $this->get('/api/chatroom/buying');
        $result = json_decode($response->getContent())->data;

        $this->assertCount(2, $result);
        $this->assertEquals($pant->id, $result[0]->chat_room_id);

        // selling chat rooms
        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        $sellRoom1 = create('App\Models\ChatRoom', ['post_id' => $post->id]);
        create('App\Models\ChatMessage', ['chat_room_id' => $sellRoom1->id]);
        $sellRoom1->updated_at = Carbon::yesterday();
        $sellRoom1->save();

        $sellRoom2 = create('App\Models\ChatRoom', ['post_id' => $post->id, 'updated_at' => Carbon::yesterday()]);
        create('App\Models\ChatMessage', ['chat_room_id' => $sellRoom2->id, 'created_at' => now()->addMinutes(20)]);

        $response = $this->get('/api/chatroom/selling');
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(2, $result);
        $this->assertEquals($sellRoom2->id, $result[0]->chat_room_id);
    }

    /** @test */
    public function buyer_archived_chat_room_will_not_show_to_buyer_but_will_show_in_seller()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        // Buying chat rooms
        $shirt = create('App\Models\ChatRoom', ['buyer_id' => auth()->id(), 'buyer_archived' => true]);
        create('App\Models\ChatMessage', ['chat_room_id' => $shirt->id]);

        $response = $this->get('/api/chatroom/buying')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(0, $result);

        $this->be($shirt->post->poster); // Seller
        $response = $this->get('/api/chatroom/selling')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(1, $result);
    }

    /** @test */
    public function seller_archived_chat_room_will_not_show_to_seller_but_will_show_in_buyer()
    {
        $this->withoutExceptionHandling();
        
        $shirt = create('App\Models\ChatRoom', ['seller_archived' => true]);
        create('App\Models\ChatMessage', ['chat_room_id' => $shirt->id]);

        $this->be($shirt->buyer);
        $response = $this->get('/api/chatroom/buying')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(1, $result);

        $this->be($shirt->poster); // Seller
        $response = $this->get('/api/chatroom/selling')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(0, $result);
    }

    /** @test */
    public function all_chat_room_should_exclude_archived_chat_rooms()
    {
        $this->withoutExceptionHandling();
        
        // Buyer archived the room
        $shirt = create('App\Models\ChatRoom', ['buyer_archived' => true]);
        create('App\Models\ChatMessage', ['chat_room_id' => $shirt->id]);

        $this->be($shirt->buyer);
        $response = $this->get('/api/chatroom/all')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(0, $result);

        $this->be($shirt->poster); // Seller
        $response = $this->get('/api/chatroom/all')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(1, $result);

        // Now Seller archived the room
        $shirt->seller_archived = true;
        $shirt->save();
        $response = $this->get('/api/chatroom/all')->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(0, $result);
    }
}
