<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostReviewTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_post_review_on_purchased_post()
    {
        $this->signIn();
        $cart = create('App\Models\Cart', [
            'user_id' => auth()->id(),
            'bought' => true
        ]);
        $purchase = create('App\Models\Purchase', ['cart_id' => $cart->id, 'delivered' => true]);
        $this->post('/api/review', [
            'purchase_id' => $purchase->id,
            'rating' => 4,
            'body' => 'new review'
        ])->assertStatus(201);
        
        $this->assertDatabaseHas('reviews', ['purchase_id' => $purchase->id, 'rating' => 4]);
    }

    /** @test */
    public function user_can_review_purchased_post_only_once()
    {
        $review = create('App\Models\Review');
        $this->be($review->reviewer);
        $this->post('/api/review', [
            'purchase_id' => $review->purchase_id,
            'rating' => 4,
            'body' => 'new review'
        ])->assertStatus(400);
    }
}
