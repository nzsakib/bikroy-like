<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArchiveChatRoomTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function buyer_user_can_archive_a_chatroom()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $one = create('App\Models\ChatMessage', ['chat_room_id' => $room->id]);

        $this->post('/api/chatroom/archive', ['chat_room_id' => $room->id])
            ->assertStatus(200);

        $this->assertDatabaseHas('chat_rooms', [
            'id' => $room->id,
            'buyer_archived' => true,
            'seller_archived' => false
        ]);
    }

    /** @test */
    public function seller_can_archive_a_chat_room()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        $room = create('App\Models\ChatRoom', ['post_id' => $post->id]);
        create('App\Models\ChatMessage', ['chat_room_id' => $room->id]);

        $this->post('/api/chatroom/archive', ['chat_room_id' => $room->id])
            ->assertStatus(200);

        $this->assertDatabaseHas('chat_rooms', [
            'id' => $room->id,
            'buyer_archived' => false,
            'seller_archived' => true
        ]);
    }

    /** @test */
    public function archived_chatroom_will_be_un_archived_on_new_message_sent()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom', [
            'buyer_id' => auth()->id(),
            'buyer_archived' => true, 
            'seller_archived' => true
        ]);
        $this->post('/api/chat', ['chat_room_id' => $room->id, 'message' => 'hey'])
            ->assertSuccessful();
        $this->assertDatabaseHas('chat_rooms', [
            'id' => $room->id,
            'buyer_archived' => false,
            'seller_archived' => false
        ]);
    }
}
