<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserNotificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_will_get_notification_when_a_post_is_liked()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post');
        $this->post('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();
        $notification = $post->poster->notifications;

        $this->assertCount(1, $notification);
        $this->assertCount(12, $notification->first()->toArray());
    }

    /** @test */
    public function a_user_will_get_notification_when_a_new_comment_is_posted()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post');
        
        $this->post('/api/posts/' . $post->id . '/comment', ['comment' => 'hey'])
            ->assertSuccessful();
        
        $notification = $post->poster->notifications;
        
        $this->assertCount(1, $notification);
        $this->assertCount(12, $notification->first()->toArray());
    }

    /** @test */
    public function a_user_will_get_notification_if_someone_purchases_a_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $cart = create('App\Models\Cart', ['user_id' => auth()->id()]);
        $p = create('App\Models\PaymentMethod');
        $del = create('App\Models\DeliveryType');
        $data = [
            'payment_method_id' => $p->id,
            'delivery_type_id' => $del->id, 
            'shipping_address' => 'USA',
            'cart_id' => $cart->id,
            'transaction_id' => 'NEO_123456789'
        ];
        $this->post('/api/cart/purchase', $data)
            ->assertSuccessful();
        $notification = $cart->post->poster->notifications;
        $this->assertCount(1, $notification);
        $this->assertCount(12, $notification->first()->toArray());
    }

    /** @test */
    public function a_user_will_get_notification_if_someone_follows()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $user = create('App\Models\UserAccount');
        $this->post('/api/follow', ['user_id' => $user->id])
            ->assertSuccessful();
        $notification = $user->notifications;
        $this->assertCount(1, $notification);
        $this->assertCount(12, $notification->first()->toArray());
    }

    /** @test */
    public function user_will_get_notification_if_someone_mentions_username()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id, 'username' => 'sakib']);
        $post = create('App\Models\Post');
        
        $this->post('/api/posts/' . $post->id . '/comment', ['comment' => '@sakib'])
            ->assertSuccessful();
        
        $notification = $user->notifications;
        
        $this->assertCount(1, $notification);
        $this->assertCount(12, $notification->first()->toArray());
    }

    /** @test */
    public function mention_notification_will_be_deleted_if_comment_is_deleted()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id, 'username' => 'sakib']);
        $post = create('App\Models\Post');
        
        $response = $this->post('/api/posts/' . $post->id . '/comment', ['comment' => '@sakib'])
            ->assertSuccessful();
        $comment = json_decode($response->getContent());
        $notification = $user->notifications;
        $this->assertCount(1, $notification);
        
        $this->delete("/api/comment/{$comment->id}")->assertSuccessful();
        $notification = $user->fresh()->notifications;
        $this->assertCount(0, $notification);
    }

    /** @test */
    public function user_will_get_notification_if_review_is_posted()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $cart = create('App\Models\Cart', ['user_id' => auth()->id()]);
        $purchase = create('App\Models\Purchase', ['cart_id' => $cart->id, 'delivered' => true]);
        $this->post('/api/review', [
            'purchase_id' => $purchase->id,
            'rating' => 4, 
            'body' => 'a good review'
        ])->assertSuccessful();
        $notification = $cart->post->poster->notifications;
        $this->assertCount(1, $notification);
        $this->assertCount(12, $notification->first()->toArray());
    }

    /** @test */
    public function user_notification_will_be_deleted_if_post_is_unliked()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post');
        $this->post('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();
        $notification = $post->poster->notifications;
        $this->assertCount(1, $notification);
        $this->delete('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();
        $notification = $post->fresh()->poster->notifications;
        $this->assertCount(0, $notification);
    }

    /** @test */
    public function notification_will_be_deleted_if_comment_is_deleted()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post');
        $response = $this->post("/api/posts/{$post->id}/comment", ['comment' => 'hey'])
                        ->assertSuccessful();
        $comment = json_decode($response->getContent());
        
        $this->assertCount(1, $post->poster->notifications);

        $this->delete("/api/comment/{$comment->id}")
            ->assertSuccessful();
        
        $this->assertCount(0, $post->fresh()->poster->notifications);
    }

    /** @test */
    public function notification_will_be_deleted_if_unfollows()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $user = create('App\Models\UserAccount');
        $this->post('/api/follow', ['user_id' => $user->id])
            ->assertSuccessful();
        $this->assertCount(1, $user->notifications);
        $this->delete('/api/unfollow', ['user_id' => $user->id]);
        $this->assertCount(0, $user->fresh()->notifications);
    }

    /** @test */
    public function notification_will_be_deleted_if_review_is_deleted()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $cart = create('App\Models\Cart', ['user_id' => auth()->id()]);
        $purchase = create('App\Models\Purchase', ['cart_id' => $cart->id, 'delivered' => true]);
        $response = $this->post('/api/review', [
            'purchase_id' => $purchase->id,
            'rating' => 4, 
            'body' => 'a good review'
        ])->assertSuccessful();
        $review = json_decode($response->getContent());
        $user = $cart->post->poster;
        $this->assertCount(1, $user->notifications);

        $this->delete("/api/review", ['review_id' => $review->id])
            ->assertSuccessful();
        $this->assertCount(0, $user->fresh()->notifications);
    }

    /** @test */
    public function user_should_not_get_notification_if_comment_in_own_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);
        
        $this->post("/api/posts/{$post->id}/comment", ['comment' => 'hey'])
            ->assertSuccessful();
        $notification = auth()->user()->notifications;
        $this->assertCount(0, $notification);
    }

    /** @test */
    public function a_notification_should_be_marked_as_read()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post');
        $this->post('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();
        $notification = $post->poster->notifications[0];
        $this->be($post->poster);
        $this->post('/api/notification', ['notification_id' => $notification->id])
            ->assertStatus(200);
        $this->assertDatabaseHas('notifications', [
            'id' => $notification->id,
            'read_at' => now()
        ]);
    }

    /** @test */
    public function user_will_get_notification_when_post_is_approved()
    {
        $this->withoutExceptionHandling();
        $post = create('App\Models\Post', ['status' => Post::STATUS_PENDING]);
        $this->signInAdmin();

        $this->patch('/api/admin/post/active', ['post_id' => $post->id])
            ->assertStatus(200);
        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'status' => 1
        ]);

        $this->assertCount(1, $post->poster->notifications);
    }

    /** @test */
    public function notification_will_be_deleted_if_post_is_deleted()
    {
        $this->withoutExceptionHandling();
        $post = create('App\Models\Post', ['status' => Post::STATUS_PENDING]);
        $this->signInAdmin();

        $this->patch('/api/admin/post/active', ['post_id' => $post->id])
            ->assertStatus(200);
        $this->assertCount(1, $post->poster->notifications);

        $this->be($post->poster);
        $this->delete("/api/posts/$post->id")->assertSuccessful();
        // dd($post->poster->fresh()->notifications);
        $this->assertDatabaseMissing('posts', ['id' => $post->id]);
        $this->assertCount(0, $post->poster->fresh()->notifications);
    }

    /** @test */
    public function user_name_in_notification_will_change_if_user_updates_his_name()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $user = create('App\Models\UserAccount');
        $this->post('/api/follow', ['user_id' => $user->id]);
        $from = auth()->user()->userProfile;
        
        $this->be($user);
        $response = $this->get('/api/notifications')
                        ->assertSee($from->name)
                        ->assertSee(json_encode($from->profile_image));

        // Update user profile
        $from->name = 'Changed Name';
        $from->profile_image = 'Changed Image';
        $from->save();

        $response = $this->get('/api/notifications')
                        ->assertSee($from->name)
                        ->assertSee($from->profile_image);
    }
}
