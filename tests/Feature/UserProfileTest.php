<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserProfileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_seller_can_see_all_sold_post()
    {
        $this->signIn();

        $p = create('App\Models\Post', ['poster_id' => auth()->id()]);
        $cart = create('App\Models\Cart', ['post_id' => $p->id]);
        create('App\Models\Purchase', ['cart_id' => $cart->id]);
        $cart = create('App\Models\Cart', ['post_id' => $p->id]);
        create('App\Models\Purchase', ['cart_id' => $cart->id]);
        
        $response = $this->get('/api/sold')
            ->assertSuccessful();
        $result = json_decode($response->getContent())->data;
        $this->assertCount(2, $result);
        $this->assertCount(17, (array)$result[0]);
    }

    /** @test */
    public function authorized_buyer_can_see_all_purchased_post()
    {
        $this->signIn();

        $cart = create('App\Models\Cart', ['user_id' => auth()->id()]);
        create('App\Models\Purchase', ['cart_id' => $cart->id]);
        $cart = create('App\Models\Cart', ['user_id' => auth()->id()]);
        create('App\Models\Purchase', ['cart_id' => $cart->id]);
        
        $response = $this->get('/api/purchased')
            ->assertSuccessful();
        $result = json_decode($response->getContent())->data;
        $this->assertCount(2, $result);
        $this->assertCount(17, (array)$result[0]);
    }

    /** @test */
    public function user_can_update_own_profile_information()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $data = [
            'phone_number' => '+8801917169307',
            'username' => 'sukkumia',
            'name' => 'sakib',
            'gender' => 'male',
            'address' => 'adshgasjd',
            'date_of_birth' => '1992-1-1',
            'email' => 'sajib@asd.com',
            'bank_name' => 'Bank asia',
            'branch_name' => 'Bashundhara',
            'account_name' => 'nazmus sakib',
            'account_number' => '65987545'
        ];
        $this->post('/api/user/update', $data)
            ->assertStatus(200);
        $this->assertDatabaseHas('bank_accounts', [
            'user_id' => auth()->id(),
            'bank_name' => $data['bank_name'],
            'branch_name' => $data['branch_name'],
            'account_name' => $data['account_name'],
            'account_number' => $data['account_number']
        ]);
    }

    /** @test */
    public function user_can_visit_profile_by_username()
    {
        $this->withoutExceptionHandling();
        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id]);
        
        $response = $this->get("/api/profile/{$user->userProfile->username}")->assertStatus(200);

        $result = json_decode($response->getContent());
        $this->assertCount(13, (array) $result);
    }
}
