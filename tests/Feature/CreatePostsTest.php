<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Enum\PostStatus;

class CreatePostsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_create_a_new_post()
    {
        $this->signIn();
        $this->withoutExceptionHandling();

        $item = make('App\Models\Item');
        $tags = make('App\Models\Tag');
        $category = create('App\Models\Category');
        
        $data = $item->toArray();

        $data['tags'] = [$tags->tag_name];
        $data['urls'] = ['asdasd'];
        $data = array_merge($data, $category->toArray());
        $response = $this->post('/api/posts', $data)->assertSuccessful();

        $this->assertDatabaseHas('posts', [
            'id' => $response->getOriginalContent()->id,
            'status' => 0
        ]);
    }

    /** @test */
    public function a_guest_cannot_create_a_post()
    {
        $this->post('/api/posts', []) 
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Unauthenticated.'
            ]);
    }

    /** @test */
    public function a_post_requires_a_heading()
    {
        $this->signIn();

        $item = make('App\Models\Item', ['heading' => null]);
        $tags = make('App\Models\Tag');
        $category = create('App\Models\Category');
        
        $data = $item->toArray();

        $data['tags'] = [$tags->tag_name];

        $data = array_merge($data, $category->toArray());

        $this->post('/api/posts', $data)
            ->assertStatus(400)
            ->assertJson([
                "error" => "The heading field is required."
            ]);
    }

    /** @test */
    public function a_post_requires_a_price()
    {
        $this->signIn();

        $item = make('App\Models\Item', ['price' => null]);
        $tags = make('App\Models\Tag');
        $category = create('App\Models\Category');
        
        $data = $item->toArray();

        $data['tags'] = [$tags->tag_name];

        $data = array_merge($data, $category->toArray());

        $this->post('/api/posts', $data)
            ->assertStatus(400)
            ->assertJson([
                "error" => "The price field is required."
            ]);
    }

    /** @test */
    public function a_post_requires_a_caption()
    {
        $this->signIn();

        $item = make('App\Models\Item', ['caption' => null]);
        $tags = make('App\Models\Tag');
        $category = create('App\Models\Category');
        
        $data = $item->toArray();

        $data['tags'] = [$tags->tag_name];

        $data = array_merge($data, $category->toArray());

        $this->post('/api/posts', $data)
            ->assertStatus(400)
            ->assertJson([
                "error" => "The caption field is required."
            ]);
    }

    /** @test */
    public function a_post_requires_a_used_field()
    {
        $this->signIn();

        $item = make('App\Models\Item', ['used' => null]);
        $tags = make('App\Models\Tag');
        $category = create('App\Models\Category');
        
        $data = $item->toArray();

        $data['tags'] = [$tags->tag_name];

        $data = array_merge($data, $category->toArray());

        $this->post('/api/posts', $data)
            ->assertStatus(400)
            ->assertJson([
                "error" => "The used field is required."
            ]);
    }

    /** @test */
    public function a_post_requires_tags()
    {
        $this->signIn();

        $item = make('App\Models\Item');
    
        $category = create('App\Models\Category');
        
        $data = $item->toArray();

        $data = array_merge($data, $category->toArray());

        $this->post('/api/posts', $data)
            ->assertStatus(400)
            ->assertJson([
                "error" => "The tags field is required."
            ]);
    }

    /** @test */
    public function a_post_requires_a_main_category_id()
    {
        $this->signIn();

        $item = make('App\Models\Item');
        $tags = make('App\Models\Tag');
        
        $data = $item->toArray();

        $data['tags'] = [$tags->tag_name];

        $this->post('/api/posts', $data)
            ->assertStatus(400)
            ->assertJson([
                "error" => "The main category id field is required."
            ]);
    }

    /** @test */
    public function authorized_user_can_delete_their_own_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);

        $this->delete('/api/posts/' . $post->id )
            ->assertStatus(200);

        $this->assertDatabaseMissing('posts', ['id' => $post->id]);
        $this->assertDatabaseMissing('items', ['id' => $post->item_id]);
    }
    
    /** @test */
    public function unauthorized_user_may_not_delete_other_posts()
    {
        $this->signIn();

        $post = create('App\Models\Post');

        $this->delete('/api/posts/' . $post->id )
            ->assertStatus(403);
        $this->assertDatabaseHas('posts', [
            'id' => $post->id
        ]);
    }

    /** @test */
    public function guest_cannot_delete_any_posts()
    {
        $post = create('App\Models\Post');

        $this->delete('/api/posts/' . $post->id )
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Unauthenticated.'
            ]);
    }

    /** @test */
    public function authorized_user_can_not_delete_sold_post()
    {
        $this->signIn();

        $post = create('App\Models\Post', 
                [
                    'poster_id' => auth()->id(), 
                    'status' => PostStatus::sold
                ]);

        $this->delete('/api/posts/' . $post->id)
            ->assertStatus(400);
    }

}
