<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Post;

class PostPermissionTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();
        $this->signInAdmin(null, 'manager');
    }
    /** @test */
    public function admin_can_not_list_new_posts_without_permission()
    {
        $pending = create('App\Models\Post', ['status' => Post::STATUS_PENDING]);
        $this->get('/api/admin/posts/new')
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_not_list_active_post_without_permission()
    {
        $active = create('App\Models\Post');
        $this->get('/api/admin/posts/active')
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_not_list_sold_post_without_permission()
    {
        $sold = create('App\Models\Post', ['status' => Post::STATUS_SOLD]);
        $this->get('/api/admin/posts/sold')
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_not_approve_post_without_permission()
    {
        $post = create('App\Models\Post', ['status' => Post::STATUS_PENDING]);
        $this->patch('/api/admin/post/active', ['post_id' => $post->id])
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_not_list_all_post_of_a_user_by_user_id_without_permission()
    {
        $user = create('App\Models\UserAccount');
        $this->get('/api/admin/user/' . $user->id . '/posts')
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_not_list_blocked_post_without_permission()
    {
        $this->get('/api/admin/posts/blocked')
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_not_list_all_post_without_permission()
    {
        $this->get('/api/admin/posts/all')
            ->assertStatus(403);
    }
}
