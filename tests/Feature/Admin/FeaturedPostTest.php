<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeaturedPostTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function admin_can_see_featured_post()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        create('App\Models\Post', ['featured' => true]);
        $response = $this->get('/api/admin/posts/featured')
            ->assertSuccessful();
        $result = json_decode($response->getContent())->data;

        $this->assertCount(1, $result);
    }

    /** @test */
    public function admin_can_make_post_featured()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        $post = create('App\Models\Post', ['featured' => false]);
        $this->patch(
            '/api/admin/post/featured',
            [
                'post_id' => $post->id,
                'featured' => 1
            ]
        )->assertSuccessful();

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'featured' => 1
        ]);
    }

    public function admin_can_make_post_notFeatured()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        $post = create('App\Models\Post', ['featured' => true]);
        $this->patch(
            '/api/admin/post/featured',
            [
                'post_id' => $post->id,
                'featured' => 0
            ]
        )->assertSuccessful();

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'featured' => 0
        ]);
    }
}
