<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostReportsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_see_all_reports()
    {
        $this->signInAdmin();

        $report = create('App\Models\ReportPost');
        $this->get('/api/admin/post-report')
                        ->assertStatus(200)
                        ->assertSee($report->comment)
                        ->assertSee($report->user->userProfile->name);
    }
}
