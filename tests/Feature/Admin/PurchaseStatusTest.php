<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PurchaseStatusTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_see_all_purchase_with_seller_release_status()
    {
        $this->get('/')->assertStatus(200);
        // $this->signInAdmin();
        // $response = create('App\Models\Purchase', ['seller_release' => false]);

        // $this->get('/api/admin/purchase/seller_release')
        //     ->assertSuccessful();
        // $result = json_decode($response->getContent())->data;

        // $this->assertCount(1, $result);
    }

    /** @test */
    public function admin_can_change_status_of_a_purchase_to_delivered()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();

        $purchase = create('App\Models\Purchase');
        $this->assertFalse($purchase->delivered);
        $this->post('/api/admin/delivered', ['purchase_id' => $purchase->id])->assertStatus(200);
        $this->assertDatabaseHas('purchases', [
            'id' => $purchase->id, 
            'delivered' => true,
        ]);
    }
}
