<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_ban_a_user()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();

        $user = create('App\Models\UserAccount');

        $this->post('/api/admin/user/ban', ['user_id' => $user->id])
            ->assertStatus(200);
        
        $this->assertDatabaseHas('user_accounts', ['id' => $user->id, 'isActive' => 0]);
    }

    /** @test */
    public function admin_can_not_ban_any_user_without_permission()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin(null, 'manager');
        $user = create('App\Models\UserAccount');
        $this->post('/api/admin/user/ban', ['user_id' => $user->id])
            ->assertStatus(403);
    }

    /** @test */
    public function super_admin_can_list_all_users()
    {
        $this->signInAdmin();

        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id]);
        $user = create('App\Models\UserAccount', ['isActive' => 0]);
        create('App\Models\UserProfile', ['user_id' => $user->id]);

        $response = $this->get('/api/admin/all-users')
                        ->assertSuccessful();
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(2, $result);
        $this->assertCount(8, (array)$result[0]);
    }
    
    /** @test */
    public function admin_can_not_list_users_without_permission()
    {
        $this->signInAdmin(null, 'reviewer');
        $this->get('/api/admin/all-users')
            ->assertStatus(403);
    }

    /** @test */
    public function super_admin_can_delete_a_user()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id]);

        $this->delete('/api/admin/delete-user', ['user_id' => $user->id])
            ->assertSuccessful();
    }

    /** @test */
    public function admin_can_not_delete_user_without_permission()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin(null, 'manager');
        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id]);
        $this->delete('/api/admin/delete-user', ['user_id' => $user->id])
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_activate_a_user()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        $user = create('App\Models\UserAccount', ['isActive' => false]);
        $this->assertDatabaseHas('user_accounts', [
            'id' => $user->id,
            'isActive' => false
        ]);
        $this->post('/api/admin/user/activate', ['user_id' => $user->id])
            ->assertStatus(200);
        $this->assertDatabaseHas('user_accounts', [
            'id' => $user->id,
            'isActive' => true
        ]);
    }

    /** @test */
    public function admin_can_not_activate_user_without_permission()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin(null, 'manager');
        $user = create('App\Models\UserAccount', ['isActive' => false]);
        $this->assertDatabaseHas('user_accounts', [
            'id' => $user->id,
            'isActive' => false
        ]);
        $this->post('/api/admin/user/activate', ['user_id' => $user->id])
            ->assertStatus(403);
        $this->assertDatabaseHas('user_accounts', [
            'id' => $user->id,
            'isActive' => false
        ]);
    }
}
