<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MainCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function super_admin_can_view_all_main_category()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();

        create('App\Models\MainCategory');
        $this->get('/api/admin/maincategory')->assertStatus(200);
    }

    /** @test */
    public function admin_can_not_list_maincategories_without_permission()
    {
        // $this->withoutExceptionHandling();
        $this->signInAdmin(null, 'manager');

        create('App\Models\MainCategory');
        $this->get('/api/admin/maincategory')->assertStatus(403);
    }

    /** @test */
    public function admin_can_list_all_maincategories_with_permission()
    {
        // $this->withoutExceptionHandling();
        $this->signInAdmin(null, 'manager', 'category');
        
        create('App\Models\MainCategory');
        $this->get('/api/admin/maincategory')->assertStatus(200);
    }

    /** @test */
    public function super_admin_or_admin_with_permission_can_create_maincategory()
    {
        $this->signInAdmin(null, 'manager', 'category');

        $this->post('/api/admin/maincategory', ['name' => 'category'])->assertStatus(200);
    }

    /** @test */
    public function can_not_create_main_category_without_permission()
    {
        $this->signInAdmin(null, 'manager');

        $this->post('/api/admin/maincategory', ['name' => 'category'])->assertStatus(403);
    }

    /** @test */
    public function super_admin_or_admin_with_permission_can_update_maincategory()
    {
        $this->signInAdmin(null, 'manager', 'category');
    
        $cat = create('App\Models\MainCategory');
        $this->patch("/api/admin/maincategory/{$cat->id}", ['name' => 'updated category'])->assertStatus(200);
        $this->assertDatabaseHas('main_categories', [
            'id' => $cat->id,
            'name' => 'updated category'
        ]);
    }

    /** @test */
    public function can_not_update_main_category_without_permission()
    {
        $this->signInAdmin(null, 'manager');
        $cat = create('App\Models\MainCategory');
        $this->patch("/api/admin/maincategory/{$cat->id}", ['name' => 'updated category'])->assertStatus(403);
    }

    /** @test */
    public function super_admin_or_admin_with_permission_can_delete_maincategory()
    {
        $this->signInAdmin(null, 'manager', 'category');
    
        $cat = create('App\Models\MainCategory');
        $this->delete("/api/admin/maincategory/{$cat->id}")->assertStatus(200);
        $this->assertDatabaseMissing('main_categories', [
            'id' => $cat->id,
        ]);
    }

    /** @test */
    public function can_not_delete_main_category_without_permission()
    {
        $this->signInAdmin(null, 'manager');
        $cat = create('App\Models\MainCategory');
        $this->delete("/api/admin/maincategory/{$cat->id}")->assertStatus(403);
    }
}
