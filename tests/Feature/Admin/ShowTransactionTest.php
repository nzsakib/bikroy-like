<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowTransactionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_see_all_transactions()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();

        $p = create('App\Models\Purchase');
        $p2 = create('App\Models\Purchase');
        $p2 = create('App\Models\Purchase', ['transaction_id' => '123456789']);
        $p2 = create('App\Models\Purchase', ['transaction_id' => '123456789']);

        $response = $this->get('/api/admin/transactions')
            ->assertSuccessful();
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(3, $result);
    }

    /** @test */
    public function admin_can_see_transaction_details_of_single_transaction()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();

        $p2 = create('App\Models\Purchase', ['transaction_id' => '123456789']);
        $p2 = create('App\Models\Purchase', ['transaction_id' => '123456789']);

        $response = $this->get('/api/admin/transaction/' . $p2->transaction_id)
            ->assertSuccessful();
        
        $result = json_decode($response->getContent())->data;
        $this->assertCount(2, $result);
        $this->assertCount(11, (array)$result[0]);
    }

    /** @test */
    public function admin_can_see_transactions_of_single_post_bye_post_id()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        
        $post = create('App\Models\Post');
        $cart = create('App\Models\Cart', ['post_id' => $post->id]);
        $t1 = create('App\Models\Purchase', ['cart_id' => $cart->id]);
        $cart = create('App\Models\Cart', ['post_id' => $post->id]);
        $t2 = create('App\Models\Purchase', ['cart_id' => $cart->id]);

        $response = $this->get('/api/admin/transaction/post/' . $post->id)
            ->assertSuccessful()
            ->assertSee($t1->transaction_id)
            ->assertSee($t2->transaction_id);
        
        $result = json_decode($response->getContent())->data;
        
        $this->assertCount(2, $result);
        $this->assertCount(11, (array)$result[0]);
    }
}
