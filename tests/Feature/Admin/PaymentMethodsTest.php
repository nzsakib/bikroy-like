<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentMethodsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_see_single_payment_details_by_id()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        $payment = create('App\Models\PaymentMethod');
        $response = $this->get('/api/admin/payment/' . $payment->id)
            ->assertSuccessful()
            ->assertSee($payment->name);
        $result = json_decode($response->getContent());
        $this->assertCount(2, (array)$result);
    }
}
