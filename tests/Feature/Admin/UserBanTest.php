<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use App\Models\Post;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserBanTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function banned_user_posts_should_be_removed_from_cart()
    {
        $this->withoutExceptionHandling();

        // create user 
        $user = create('App\Models\UserAccount');
        // add a post to cart 
        $post = create('App\Models\Post');

        $cart = $user->addToCart($post);
        // ban the poster 
        $this->signInAdmin();
        $this->post('/api/admin/user/ban', ['user_id' => $post->poster->id])->assertSuccessful();
        // assert cart is empty 
        $this->assertEmpty($user->fresh()->carts);
    }

    /** @test */
    public function banned_user_post_will_be_blocked()
    {
        $post = create('App\Models\Post');
        $this->signInAdmin();
        $this->post('/api/admin/user/ban', ['user_id' => $post->poster->id])->assertSuccessful();

        $this->assertEquals(Post::STATUS_USER_BANNED, $post->fresh()->status);
    }

    /** @test */
    public function restored_user_post_will_be_restored()
    {
        $this->withoutExceptionHandling();
        $user = create('App\Models\UserAccount', ['isActive' => false]);
        $post = create('App\Models\Post', ['poster_id' => $user->id, 'status' => Post::STATUS_USER_BANNED]);

        $this->assertEquals(Post::STATUS_USER_BANNED, $post->status);
        $this->signInAdmin();
        $this->post('/api/admin/user/activate', ['user_id' => $user->id])->assertSuccessful();

        $this->assertEquals(Post::STATUS_ACTIVE, $post->fresh()->status);
    }

    /** @test */
    public function banned_user_should_be_excluded_from_likes_list_of_a_post()
    {
        $post = create('App\Models\Post');
        $user = create('App\Models\UserAccount');

        $post->likes()->sync($user->id);
        $this->assertCount(1, $post->likes);

        // Ban user 
        $this->signInAdmin();
        $this->post('/api/admin/user/ban', ['user_id' => $user->id])->assertSuccessful();

        // Post should not include banned user likes
        $this->assertCount(0, $post->fresh()->likes);
    }
}
