<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_see_all_counts_in_dashboard()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        create('App\Models\UserAccount');
        create('App\Models\Post');
        $response = $this->get('/api/admin/all-count')
            ->assertSuccessful();
        $result = json_decode($response->getContent());

        $this->assertCount(12, (array)$result);
    }
}
