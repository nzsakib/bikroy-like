<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Mail\AdminWasCreated;

class CreateAdminsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();
        // now re-register all the roles and permissions
        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    /** @test */
    public function admin_can_create_another_admin()
    {
        $this->signInAdmin();

        $role = Role::create(['name' => 'admin', 'guard_name' => 'admin']);
        $data = [
            'name' => 'sakib',
            'email' => 'new@admin.com',
            'role' => $role->id
        ];

        Mail::fake();

        $this->post('/api/admin/create', $data)
            ->assertStatus(200);

        Mail::assertSent(AdminWasCreated::class, function ($mail) use ($data) {
            return $mail->hasTo($data['email']);
        });
    }

    /** @test */
    public function non_super_admin_can_not_create_a_new_admin()
    {
        $this->signInAdmin(null, 'admin');
        $role = Role::findOrCreate('admin', 'admin');
        $data = [
            'name' => 'sakib',
            'email' => 'new@admin.com',
            'role' => $role->id
        ];

        $this->post('/api/admin/create', $data)
            ->assertStatus(403);
    }

    /** @test */
    public function super_admin_can_delete_an_admin_account()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        
        $admin = create('App\Models\Admin');
        
        $this->delete('/api/admin/admin-delete', ['user_id' => $admin->id])
            ->assertSuccessful();

        $this->assertDatabaseMissing('admins', [
            'id' => $admin->id,
            'email' => $admin->email
        ]);

        $this->assertDatabaseMissing('model_has_roles', [
            'model_type' => 'App\Models\Admin',
            'model_id' => $admin->id
        ]);

    }

    protected function createAdmin($role=null)
    {
        $admin = create('App\Models\Admin');
        $role = Role::create(['name' => $role ?: 'admin', 'guard_name' => 'admin']);
        $admin->syncRoles($role);
        return $admin;
    }

    /** @test */
    public function non_super_admin_can_not_delete_any_admin()
    {
        $this->signInAdmin(null, 'admin');

        $admin = create('App\Models\Admin');
        $this->delete('/api/admin/admin-delete', ['user_id' => $admin->id])
            ->assertStatus(403);
    }

    /** @test */
    public function admin_can_not_delete_their_own_account()
    {
        $this->signInAdmin();
        $this->delete('/api/admin/admin-delete', ['user_id' => auth()->id()])
            ->assertStatus(403);
    }

    /** @test */
    public function normal_user_cannot_delete_admin()
    {
        $this->signIn();

        $this->delete('/api/admin/admin-delete', ['user_id' => 1])
            ->assertStatus(401);
    }

    /** @test */
    public function super_admin_can_update_other_admin_details()
    {
        $this->signInAdmin();
        $admin = create('App\Models\Admin');
        $role = Role::create(['name' => 'manager', 'guard_name' => 'admin']);
        $data = [
            'name' => 'updated name',
            'email' => 'updated@email.com',
            'role' => $role->id,
            'user_id' => $admin->id,
        ];
        $this->patch('/api/admin/admin-update', $data)
            ->assertSuccessful();
    }

    /** @test */
    public function non_super_admin_can_not_update_other_admin_details()
    {
        $this->signInAdmin(null, 'admin');
        $admin = create('App\Models\Admin');
        $role = Role::create(['name' => 'manager', 'guard_name' => 'admin']);
        $data = [
            'name' => 'updated name',
            'email' => 'updated@email.com',
            'role' => $role->id,
            'user_id' => $admin->id,
        ];
        $this->patch('/api/admin/admin-update', $data)
            ->assertStatus(403);
    }
}
