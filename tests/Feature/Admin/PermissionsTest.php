<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;

class PermissionsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function super_admin_can_create_a_new_permission()
    {
        $this->signInAdmin();

        $this->post('/api/admin/permission', ['name' => 'update post'])
            ->assertSuccessful();
    }

    /** @test */
    public function non_super_admin_can_not_create_permission()
    {
        $this->signInAdmin(null, 'manager');

        $this->post('/api/admin/permission', ['name' => 'update post'])
            ->assertStatus(403);
    }

    /** @test */
    public function super_admin_can_update_permission()
    {
        $this->signInAdmin();
        $p = Permission::create(['name' => 'update post', 'guard_name' => 'admin']);
        $this->patch('/api/admin/permission/' . $p->id, ['name' => 'delete post'])
            ->assertSuccessful();
        $this->assertDatabaseHas('permissions', [
            'id' => $p->id,
            'name' => 'delete post'
        ]);
    }

    /** @test */
    public function super_admin_can_delete_a_permission()
    {
        $this->signInAdmin();
        $p = create(Permission::class);
        $role = create(Role::class);

        $role->givePermissionTo($p);

        $this->delete('/api/admin/permission/' . $p->id)
            ->assertSuccessful();
        $this->assertDatabaseMissing('permissions', [
            'id' => $p->id,
            'name' => $p->name
        ]);
        $this->assertDatabaseMissing('role_has_permissions', [
            'permission_id' => $p->id,
            'role_id' => $role->id
        ]);
    }

    /** @test */
    public function admin_can_not_delete_permission_without_permission()
    {
        $this->signInAdmin(null, 'manager');
        $p = create(Permission::class);
        $role = create(Role::class);

        $role->givePermissionTo($p);

        $this->delete('/api/admin/permission/' . $p->id)
            ->assertStatus(403);
    }

    /** @test */
    public function super_admin_can_assign_permissions_to_a_role()
    {
        $this->signInAdmin();
        $per1 = create(Permission::class);
        $per2 = create(Permission::class);
        $role = create(Role::class);

        $this->post('/api/admin/permission/assign-to-role', [
            'permissions' => [$per1->id, $per2->id],
            'role_id' => $role->id
        ])->assertSuccessful();
        $this->assertCount(2, $role->permissions->toArray());
    }

    /** @test */
    public function super_admin_can_see_all_permissions()
    {
        $this->signInAdmin();
        $p = create(Permission::class);
        $p2 = create(Permission::class);
        $response = $this->get('/api/admin/permission')
                        ->assertSee($p->name)
                        ->assertSee($p2->name)
                        ->assertSuccessful();
        $result = json_decode($response->getContent());

        $this->assertCount(2, $result);
    }
    
}
