<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeliveryTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_delete_delivery_when_it_is_not_used_in_any_purchase()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();

        $delivery = create('App\Models\DeliveryType');
        $this->delete("/api/admin/delivery-type/{$delivery->id}")->assertSuccessful();
    }

    /** @test */
    public function admin_can_not_delete_delivery_when_it_is_used_in_any_purchase()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();
        
        $p = create('App\Models\Purchase');
        $delivery = $p->deliveryType;
        $this->delete("/api/admin/delivery-type/{$delivery->id}")->assertStatus(400);
        $this->assertDatabaseHas('delivery_types', [
            'id' => $delivery->id
        ]);
    }
}
