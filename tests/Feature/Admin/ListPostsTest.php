<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Enum\PostStatus;

class ListPostsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_list_all_types_of_post()
    {
        $this->signInAdmin();

        $active = create('App\Models\Post');
        $pending = create('App\Models\Post', ['status' => PostStatus::pending]);
        $sold = create('App\Models\Post', ['status' => PostStatus::sold]);

        $response = $this->get('/api/admin/posts/all')
            ->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(3, $result);
    }

    /** @test */
    public function admin_can_list_pending_post()
    {
        $this->signInAdmin();

        $active = create('App\Models\Post');
        $pending = create('App\Models\Post', ['status' => PostStatus::pending]);
        $sold = create('App\Models\Post', ['status' => PostStatus::sold]);


        $this->get('/api/admin/posts/new')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'post_id' => $pending->id
                    ],
                ]
            ]);
    }

    /** @test */
    public function admin_can_list_active_posts()
    {
        $this->signInAdmin();

        $active = create('App\Models\Post');
        $pending = create('App\Models\Post', ['status' => PostStatus::pending]);
        $sold = create('App\Models\Post', ['status' => PostStatus::sold]);


        $this->get('/api/admin/posts/active')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'post_id' => $active->id
                    ],
                ]
            ]);
    }

    /** @test */
    public function admin_can_list_sold_posts()
    {
        $this->signInAdmin();

        $active = create('App\Models\Post');
        $pending = create('App\Models\Post', ['status' => PostStatus::pending]);
        $sold = create('App\Models\Post', ['status' => PostStatus::sold]);


        $this->get('/api/admin/posts/sold')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'post_id' => $sold->id
                    ],
                ]
            ]);
    }

    /** @test */
    public function admin_can_approve_a_pending_post()
    {
        $this->withoutExceptionHandling();
        $this->signInAdmin();

        $post = create('App\Models\Post', ['status' => PostStatus::pending]);

        $r = $this->patch('/api/admin/post/active', ['post_id' => $post->id])
            ->assertStatus(200);
            
        
        $this->assertDatabaseHas('posts', ['id' => $post->id, 'status' => PostStatus::active]);
    }

    /** @test */
    public function admin_can_list_all_post_of_a_user_by_user_id()
    {
        $this->signInAdmin();

        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['user_id' => $user->id]);
        create('App\Models\Post', [
            'status' => PostStatus::pending, 
            'poster_id' => $user->id
        ]);
        create('App\Models\Post', [
            'status' => PostStatus::sold, 
            'poster_id' => $user->id
        ]);
        create('App\Models\Post', [
            'status' => PostStatus::active, 
            'poster_id' => $user->id
        ]);

        $response = $this->get('/api/admin/user/' . $user->id . '/posts')
            ->assertSuccessful();
        $result = json_decode($response->getContent())->data;

        $this->assertCount(3, $result);
        $this->assertCount(24, (array) $result[0]);
    }

    /** @test */
    public function admin_can_list_all_blocked_posts()
    {
        $this->signInAdmin();

        $active = create('App\Models\Post');
        $pending = create('App\Models\Post', ['status' => PostStatus::pending]);
        $blocked = create('App\Models\Post', ['status' => PostStatus::block]);

        $response = $this->get('/api/admin/posts/blocked')
            ->assertStatus(200);
        $result = json_decode($response->getContent())->data;

        $this->assertCount(1, $result);
    }

    /** @test */
    public function admin_can_list_all_posts()
    {
        $this->signInAdmin();

        $active = create('App\Models\Post');
        $pending = create('App\Models\Post', ['status' => PostStatus::pending]);
        $blocked = create('App\Models\Post', ['status' => PostStatus::block]);

        $response = $this->get('/api/admin/posts/all')
            ->assertStatus(200);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(3, $result);
    }
    
}
