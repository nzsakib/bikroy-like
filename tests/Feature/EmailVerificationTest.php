<?php

namespace Tests\Feature;

use App\Models\Otp;
use Tests\TestCase;
use App\Mail\OtpEmailVerification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmailVerificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_will_receive_otp_via_email()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        Mail::fake();
        $data = [
            'email' => 'test@email.com'
        ];
        $this->post('/api/email/send', $data)
            ->assertStatus(200);
        $this->assertDatabaseHas('otps', [
            'user_id' => auth()->id()
        ]);
        $otp = Otp::where('user_id', auth()->id())->first();
        Mail::assertSent(OtpEmailVerification::class, function ($mail) use($data, $otp) {
            return $mail->hasTo($data['email']) && $mail->code == $otp->code;
        });
    }

    /** @test */
    public function verify_the_email_via_otp_received()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $otp = Otp::create([
            'user_id' => auth()->id(),
            'code' => '123456',
            'email' => 'test@test.com'
        ]);
        
        $data = [
            'code' => $otp->code
        ];
        $profile = auth()->user()->userProfile;
    
        $this->assertEquals($profile->email_verified, 0);

        $this->post('/api/email/verify', $data)
            ->assertStatus(200);
        
        $profile = auth()->user()->fresh()->userProfile;
        $this->assertEquals($profile->email, $otp->email);
        $this->assertEquals($profile->email_verified, true);
    }

    /** @test */
    public function user_can_not_verify_with_expired_token()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $otp = Otp::create([
            'user_id' => auth()->id(),
            'code' => '123456',
            'email' => 'test@test.com',
            'created_at' => now()->addMinutes(20)
        ]);
        $data = [
            'code' => $otp->code
        ];
        $profile = auth()->user()->userProfile;
    
        $this->assertEquals($profile->email_verified, 0);

        $this->post('/api/email/verify', $data)
            ->assertStatus(400);
        $profile = auth()->user()->fresh()->userProfile;
        $this->assertEquals($profile->email_verified, 0);
        
        // $this->assertTrue($otp->fresh()->used);
    }

    /** @test */
    public function user_can_not_enter_duplicate_email_entry_during_verification()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $user = create('App\Models\UserAccount');
        create('App\Models\UserProfile', ['email' => 'test@test.com', 'user_id' => $user->id]);

        $data =[
            'email' => 'test@test.com'
        ];
        Mail::fake();
        $this->post('/api/email/send', $data)
            ->assertStatus(400);
        Mail::assertNotSent(OtpEmailVerification::class);
    }
}
