<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserMessagesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_user_can_create_a_chatroom()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        
        $post = create('App\Models\Post');
        $this->post('/api/chatroom', ['post_id' => $post->id])
            ->assertStatus(200)
            ->assertJsonStructure([
                'chat_room_id'
            ]);
        $this->assertDatabaseHas('chat_rooms', [
            'post_id' => $post->id,
            'buyer_id' => auth()->id(),
            'buyer_archived' => false,
            'seller_archived' => false,
        ]);
    }

    /** @test */
    public function unauthorized_user_cannot_create_chatroom()
    {
        $post = create('App\Models\Post');
        $this->post('/api/chatroom', ['post_id' => $post->id])
            ->assertStatus(401);
    }

    /** @test */
    public function authorized_user_can_send_a_message()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $data = [
            'chat_room_id' => $room->id,
            'message' => 'a message'
        ];
        $this->post('/api/chat', $data)
            ->assertStatus(200);
    }

    /** @test */
    public function authorized_user_can_view_chatroom_messages()
    {
        $this->signIn();
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $data = [
            'chat_room_id' => $room->id,
            'sender_id' => auth()->id()
        ];
        $one = create('App\Models\ChatMessage', $data);
        $two = create('App\Models\ChatMessage', $data);
        $three =create('App\Models\ChatMessage', $data);

        $response = $this->get('/api/chatroom/' . $room->id)
            ->assertSee($one->message)
            ->assertSee($two->message)
            ->assertSee($three->message);
        $result = json_decode($response->getContent())->data;
        $this->assertCount(5, (array) $result[0]);
    }

    /** @test */
    public function unauthorized_user_cannot_view_chatroom_messages()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom');
        create('App\Models\ChatMessage', ['chat_room_id' => $room->id]);

        $this->get('/api/chatroom/' . $room->id)
            ->assertStatus(403);
    }

    /** @test */
    public function user_will_know_if_message_is_own_message()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $data = [
            'chat_room_id' => $room->id,
            'sender_id' => auth()->id()
        ];
        create('App\Models\ChatMessage', $data);

        $response = $this->get("/api/chatroom/{$room->id}");
        $result = json_decode($response->getContent())->data;
        $this->assertTrue($result[0]->isOwnMessage);
    }

    /** @test */
    public function message_will_mark_as_seen_if_user_opens_it()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $data = [ 'chat_room_id' => $room->id ];
        
        $message = create('App\Models\ChatMessage', $data);
        $this->assertDatabaseHas('chat_messages', [
            'id' => $message->id,
            'seen' => false, 
        ]);
        $this->post("/api/chatroom/seen", $data)
                ->assertStatus(200);
        $this->assertDatabaseHas('chat_messages', [
            'id' => $message->id,
            'seen' => true, 
        ]);
    }

    /** @test */
    public function unauthorized_user_cannot_make_other_messages_seen()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $room = create('App\Models\ChatRoom');
        $data = [ 'chat_room_id' => $room->id ];
        create('App\Models\ChatMessage', $data);

        $this->post("/api/chatroom/seen", $data)
                ->assertStatus(403);
    }
    
    /** @test */
    public function authorized_user_can_delete_own_chat_message()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $message = create('App\Models\ChatMessage', [
            'chat_room_id' => $room->id,
            'sender_id' => auth()->id(),
        ]);

        $this->delete("/api/chat/{$message->id}")
            ->assertStatus(200);
        
        $this->assertDatabaseMissing('chat_messages', [
            'id' => $message->id,
            'sender_id' => auth()->id(),
            'message' => $message->message,
        ]);
    }

    /** @test */
    public function unauthorized_user_can_not_delete_chat_messages()
    {
        $message = create('App\Models\ChatMessage');
        $this->delete("/api/chat/{$message->id}")
            ->assertStatus(401);
    }

    /** @test */
    public function authorized_user_can_not_delete_other_persons_message()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $msg = create('App\Models\ChatMessage', ['chat_room_id' => $room->id]);

        $this->delete("/api/chat/{$msg->id}")
            ->assertStatus(403);
    }

    /** @test */
    public function user_can_see_all_unseen_chat_room_count()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        // room 1
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $data = [ 'chat_room_id' => $room->id, 'receiver_id' => auth()->id() ];
        $message = create('App\Models\ChatMessage', $data);
        $message = create('App\Models\ChatMessage', $data);
        $message = create('App\Models\ChatMessage', $data);
        // room 2
        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $data = [ 'chat_room_id' => $room->id, 'receiver_id' => auth()->id() ];
        $message = create('App\Models\ChatMessage', $data);

        $response = $this->get('/api/message/unseencount')
            ->assertStatus(200);
        $result = json_decode($response->getContent());
        
        $this->assertEquals(2, $result->unseenCount);
    }

    /** @test */
    public function user_can_not_message_a_blocked_user()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $room = create('App\Models\ChatRoom', ['buyer_id' => auth()->id()]);
        $room->poster()->update(['isActive' => 0]);
        $this->post('/api/chat', [
            'chat_room_id' => $room->id,
            'message' => 'message is here'
        ])->assertStatus(400);
    }
}
