<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostDeleteTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_not_delete_post_if_post_is_sold_once()
    {
        $this->withoutExceptionHandling();
        $post = create('App\Models\Post');
        $post->item->itemCounter->increment('sold');
        
        $this->be($post->poster);
        $this->delete("/api/posts/{$post->id}")
            ->assertStatus(400);
        $this->assertDatabaseHas('posts', [
            'id' => $post->id
        ]);
    }
}
