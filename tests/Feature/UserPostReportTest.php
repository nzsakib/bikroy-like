<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserPostReportTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_report_a_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();

        $post = create('App\Models\Post');
        $data = [
            'post_id' => $post->id,
            'comment' => 'a bad post',
        ];
        $this->post('/api/post/report', $data)
            ->assertSuccessful();
        $this->assertDatabaseHas('report_posts', [
            'reporter_id' => auth()->id(),
            'post_id' => $post->id,
            'comment' => $data['comment']
        ]);
    }

    /** @test */
    public function guest_can_not_report_any_post()
    {
        $this->post('/api/post/report', [])
            ->assertStatus(401);
    }
}
