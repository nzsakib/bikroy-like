<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserLikesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_user_can_like_a_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post');

        $this->post('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();
        
        $this->assertDatabaseHas('likes', [
            'user_id' => auth()->id(),
            'post_id' => $post->id
        ]);
    }

    /** @test */
    public function unauthorized_user_can_not_like_any_post()
    {
        $this->post('/api/like', ['post_id' => 2])
            ->assertStatus(401);
    }

    /** @test */
    public function authorized_user_can_unlike_a_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post');

        $this->post('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();

        $this->delete('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();
        
        $this->assertDatabaseMissing('likes', [
            'user_id' => auth()->id(),
            'post_id' => $post->id
        ]);
    }

    /** @test */
    public function user_can_see_all_liker_of_a_post()
    {
        $this->signIn();
        $post = create('App\Models\Post');
        $user = auth()->user()->userProfile;
        $this->post('/api/like', ['post_id' => $post->id])
            ->assertSuccessful();
        $this->get('/api/like/' . $post->id)
            ->assertSee($user->name);
    }

    /** @test */
    public function user_will_not_get_notification_if_likes_own_post()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $post = create('App\Models\Post', ['poster_id' => auth()->id()]);

        $this->post('/api/like', ['post_id' => $post->id])
            ->assertStatus(200);
        
        $this->assertCount(0, auth()->user()->notifications); 
    }
}
