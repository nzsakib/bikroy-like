<?php

namespace Tests;

use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Spatie\Permission\Models\Permission;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signIn($user = null)
    {
        if (!$user) {
            $user = create('App\Models\UserAccount');
            create('App\Models\UserProfile', ['user_id' => $user->id]);

        }

    	$this->actingAs($user);

    	return $this;
    }

    protected function signInAdmin($user = null, $role = null, $permission = null)
    {
        if (!$user) {
            $user = create('App\Models\Admin');
        }
        
        $role=Role::create(['name' => $role?: 'super-admin', 'guard_name' => 'admin']);
        if ($permission) {
            $permission = Permission::create(['name' => $permission, 'guard_name' => 'admin']);
            $role->syncPermissions([$permission]);
        }
        $user->assignRole($role);
    	$this->actingAs($user, 'admin');

    	return $this;
    }
}
