<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Notification;
use Faker\Generator as Faker;

$factory->define(Notification::class, function (Faker $faker) {
    $type = ['YouHaveAReview', 'YouHaveBeenFollowed', 'YouHaveNewComment', 'YourPostIsApproved', 'YourPostIsLiked', 'YourPostWasPurchased'];
    $source = ['Review', 'UserAccount', 'Post', 'Purchase', 'Comment'];
    $from_user = create('App\Models\UserProfile');
    $data = [
        'message' => $from_user->name . ' Liked your post.',
        'profile_image' => 'random image',
        'source_id' => 1,
        'source_type' => 'Post',
        'link' => 'some link',
    ];
    return [
        'type' => $faker->randomElement($type),
        'notifiable_type' => 'App\Models\UserAccount',
        'notifiable_id' => function () {
            return create('App\Models\UserProfile')->user_id;
        },
        'source_type' => $faker->randomElement($source),
        'source_id' => 1,
        'data' => $data,
        'read_at' => null,
    ];
});
