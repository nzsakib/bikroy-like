<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\UserProfile;
use Faker\Generator as Faker;

$factory->define(UserProfile::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory('App\Models\UserAccount')->create()->id;
        },
        'username' => $faker->userName,
        'name' => $faker->name,
        'email' => $faker->email,
        'date_of_birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'address' => $faker->city . ', ' . $faker->country,
        'about' => $faker->sentence,
        'gender' => $faker->randomElement(['male', 'female']),
        'created_at' => $faker->dateTime('now', null),
    ];
});
