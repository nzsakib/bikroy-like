<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    $item = factory('App\Models\Item')->create();

    factory('App\Models\ItemGallery', 3)->create(['item_id' => $item->id]);

    $digit = $faker->randomDigitNotNull;

    $item->itemCounter()->create([
        'count' => 5,
        'sold' => 0
    ]);

    return [
        'poster_id' => function () {
            $user = factory('App\Models\UserAccount')->create();
            factory('App\Models\UserProfile')->create(['user_id' => $user->id]);
            return $user->id;
        },
        'item_id' => $item->id,
        'status' => 1,
        'created_at' => $faker->dateTime('now', null),
        'featured' => false
    ];
});
