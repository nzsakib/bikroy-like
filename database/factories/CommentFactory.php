<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    
    return [
        'post_id' => function() {
            return factory('App\Models\Post')->create()->id;
        },
        'comment' => $faker->paragraph,
        'user_id' => function() {
            $user = factory('App\Models\UserAccount')->create();
            factory('App\Models\UserProfile')->create(['user_id' => $user->id]);
            return $user->id;
        },
        'created_at' => $faker->dateTime('now', null),
    ];
});
