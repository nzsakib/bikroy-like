<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Admin;
use Faker\Generator as Faker;

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => '$2y$10$No6q.1A4oO7.pOu709F0ReGWyDYptVlB8KIWjT4Wb1EZt/e/9hCSK',
    ];
});
