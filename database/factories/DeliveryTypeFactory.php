<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\DeliveryType;
use Faker\Generator as Faker;

$factory->define(DeliveryType::class, function (Faker $faker) {
    $names = [
        'Shundarban',
        'SA Paribahan',
        'Pathao'
    ];
    $scopes = [
        'Inside Dhaka',
        'Outside Dhaka'
    ];

    return [
        'name' => array_random($names),
        'delivery_scope' => array_random($scopes),
        'cost' => $faker->numberBetween(40, 100)
    ];
});
