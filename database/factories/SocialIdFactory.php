<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\SocialId;
use Faker\Generator as Faker;

$factory->define(SocialId::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory('App\Models\UserAccount')->create()->id;
        },
        'social_id' => str_random(6),
        'social_type' => $faker->randomElement(['facebook', 'twitter', 'google']),
    ];
});
