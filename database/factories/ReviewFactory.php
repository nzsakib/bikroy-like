<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'purchase_id' => function () {
            return factory('App\Models\Purchase')->create()->id;
        },
        'rating' => $faker->randomElement([1,2,3,4,5]),
        'body' => $faker->sentence,
        'poster_id' => 1,
        'reviewer_id' => 1,
        'created_at' => $faker->dateTime('now', null),
    ];
});
