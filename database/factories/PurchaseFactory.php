<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Purchase;
use Faker\Generator as Faker;

$factory->define(Purchase::class, function (Faker $faker) {
    return [
        'cart_id' => function () {
            return factory('App\Models\Cart')->create()->id;
        },
        'seller_release' => false,
        'at_head_office' => false,
        'qc_passed' => false,
        'head_office_release' => false,
        'delivered' => false,
        'payment_method_id' => function() {
            return factory('App\Models\PaymentMethod')->create()->id;
        },
        'delivery_type_id' => function() {
            return factory('App\Models\DeliveryType')->create()->id;
        },
        'shipping_address' => $faker->address,
        'transaction_id' => uniqid(),
        'price' => $faker->randomNumber($nbDigits = 3, $strict = false),
        // 'created_at' => $faker->dateTime('now', null),
    ];
});
