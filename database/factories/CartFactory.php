<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Cart;
use Faker\Generator as Faker;

$factory->define(Cart::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            $user = factory('App\Models\UserAccount')->create();
            factory('App\Models\UserProfile')->create(['user_id' => $user->id]);
            return $user->id;
        },
        'post_id' => function() {
            return factory('App\Models\Post')->create()->id;
        },
        'bought' => false,
        'removed' => false
    ];
});
