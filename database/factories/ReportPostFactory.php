<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\ReportPost;
use Faker\Generator as Faker;

$factory->define(ReportPost::class, function (Faker $faker) {
    $reporter = factory('App\Models\UserAccount')->create();
    factory('App\Models\UserProfile')->create(['user_id' => $reporter->id]);
    return [
        'reporter_id' => $reporter->id,
        'post_id' => function() {
            return factory('App\Models\Post')->create()->id;
        },
        'comment' => $faker->sentence,
        'status' => 0
    ];
});
