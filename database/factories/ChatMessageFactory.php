<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\ChatMessage;
use Faker\Generator as Faker;

$factory->define(ChatMessage::class, function (Faker $faker) {
    return [
        'chat_room_id' => function() {
            return factory('App\Models\ChatRoom')->create()->id;
        },
        'message' => $faker->sentence,
        'sender_id' => function() {
            return factory('App\Models\UserAccount')->create()->id;
        },
        'receiver_id' => function () {
            return factory('App\Models\UserAccount')->create()->id;
        },
        'seen' => false
    ];
});
