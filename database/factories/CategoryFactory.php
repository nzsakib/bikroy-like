<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'main_category_id' => function() {
            return factory('App\Models\MainCategory')->create()->id;
        },
        'sub_category_id' => function() {
            return factory('App\Models\SubCategory')->create()->id;
        }
    ];
});
