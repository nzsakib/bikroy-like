<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'price' => $faker->randomNumber($nbDigits = 3, $strict = false),
        'heading' => $faker->sentence(),
        'caption' => $faker->sentence(),
        'address' => $faker->address,
        'category_id' => function() {
            return factory('App\Models\Category')->create()->id;
        },
        
        'used' => $faker->boolean,
        'created_at' => $faker->dateTime('now', null),
    ];
});
