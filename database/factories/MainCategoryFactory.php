<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\MainCategory;
use Faker\Generator as Faker;

$factory->define(MainCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
