<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\ItemGallery;
use Faker\Generator as Faker;

$factory->define(ItemGallery::class, function (Faker $faker) {
    
    $cat = ['city', 'food', 'fashion', 'cats', 'nightlife'];

    return [
        'item_id' => function() {
            return factory('App\Models\Item')->create()->id;
        },
        'image_link' => 'http://lorempixel.com/800/800/' . array_random($cat)
    ];
});
