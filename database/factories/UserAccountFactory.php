<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\UserAccount;
use Faker\Generator as Faker;

$factory->define(UserAccount::class, function (Faker $faker) {
    return [
        'phone_number' => substr($faker->e164PhoneNumber, 1, 11),
        // Hello1234
        'password' => '$2y$10$cGivyLRo8HNHTUGpkeDiTeDLwcEEcwGkDyIOf047Bg.yC28iVk6Oi',
        'created_at' => $faker->dateTime('now', null),
        'isActive' => 1,
        'isComplete' => 1,
        'featured' => 0,
    ];
});
