<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\PaymentMethod;
use Faker\Generator as Faker;

$factory->define(PaymentMethod::class, function (Faker $faker) {
    $names = [
        'bKash',
        'Cash on Delivery',
        'Card Payment'
    ];
    return [
        'name' => array_random($names)
    ];
});
