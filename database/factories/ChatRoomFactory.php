<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\ChatRoom;
use Faker\Generator as Faker;

$factory->define(ChatRoom::class, function (Faker $faker) {
    return [
        'post_id' => function() {
            return factory('App\Models\Post')->create()->id;
        },
        'buyer_id' => function() {
            $user = factory('App\Models\UserAccount')->create();
            create('App\Models\UserProfile', ['user_id' => $user->id]);
            return $user->id;
        },
        'buyer_archived' => false,
        'seller_archived' => false
    ];
});
