<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id');
            $table->bigInteger('cart_id');
            $table->boolean('seller_release');
            $table->boolean('at_head_office');
            $table->boolean('qc_passed');
            $table->boolean('head_office_release');
            $table->integer('delivery_type_id');
            $table->integer('payment_method_id');
            $table->integer('price');
            $table->boolean('delivered')->default(false);
            $table->text('shipping_address');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
