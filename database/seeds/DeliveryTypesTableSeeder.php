<?php

use Illuminate\Database\Seeder;
use App\Models\DeliveryType;

class DeliveryTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeliveryType::truncate();
        DeliveryType::create([
            'name' => 'Sundarban',
            'delivery_scope' => 'Inside Dhaka',
            'cost' => 60
        ]);

        DeliveryType::create([
            'name' => 'Sundarban',
            'delivery_scope' => 'Outside Dhaka',
            'cost' => 100
        ]);

        DeliveryType::create([
            'name' => 'SA Paribahan',
            'delivery_scope' => 'Inside Dhaka',
            'cost' => 60
        ]);

        DeliveryType::create([
            'name' => 'Sundarban',
            'delivery_scope' => 'Outside Dhaka',
            'cost' => 110
        ]);

        DeliveryType::create([
            'name' => 'Pathao',
            'delivery_scope' => 'Inside Dhaka',
            'cost' => 60
        ]);
    }
}
