<?php

use Illuminate\Database\Seeder;
use App\Models\UserAccount;
use App\Models\Post;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('likes')->truncate();
        
        $users = UserAccount::all();

        $posts = Post::latest()->take(20)->get();

        $ids = $users->pluck('id')->toArray();

        $posts->each(function ($post) use($ids) {
            $post->likes()->attach($ids);
        });
    }
}
