<?php

use Illuminate\Database\Seeder;

class ItemGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('item_galleries')->truncate();
        
        factory('App\Models\ItemGallery')->create();
    }
}
