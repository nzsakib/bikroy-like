<?php

use Illuminate\Database\Seeder;
use App\Models\MainCategory;
use App\Models\SubCategory;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->truncate();
        \DB::table('main_categories')->truncate();
        \DB::table('sub_categories')->truncate();

        // factory('App\Models\Category', 10)->create();

        $one = MainCategory::create(['name' => 'Menswear']);
        $two = MainCategory::create(['name' => 'Womenswear']);
        $three = MainCategory::create(['name' => 'Kids']);
        $four = MainCategory::create(['name' => 'Electronics']);
        $five = MainCategory::create(['name' => 'Computer']);
        SubCategory::create(['name' => 'Tops']); //1
        SubCategory::create(['name' => 'Pants/Jeans']); //2
        SubCategory::create(['name' => 'Outwear/Jackets']); //3
        SubCategory::create(['name' => 'Accessories']); //4 
        SubCategory::create(['name' => 'Shoes']); //5
        SubCategory::create(['name' => 'Lingerie']); //6
        SubCategory::create(['name' => 'Bags']); //7 
        SubCategory::create(['name' => 'Glass']); // 8
        SubCategory::create(['name' => 'Belt']); // 9
        SubCategory::create(['name' => 'Socks']); // 10
        SubCategory::create(['name' => 'Laptop']); //11
        SubCategory::create(['name' => 'Graphics Card']); //12
        SubCategory::create(['name' => 'Processor']); //13

        $one->subCategory()->attach([1,2,3,4,5]);
        $two->subCategory()->attach([1,2,3,4,5,6]);
        $three->subCategory()->attach([7,8,9,10]);
        $four->subCategory()->attach([11,12,13]);
        $five->subCategory()->attach([11,12,13]);
        // Category::create([
        //     'main_category_id' => 1,
        //     'sub_category_id' => 1
        // ]);
        // Category::create([
        //     'main_category_id' => 1,
        //     'sub_category_id' => 2
        // ]);
        // Category::create([
        //     'main_category_id' => 1,
        //     'sub_category_id' => 3
        // ]);
        // Category::create([
        //     'main_category_id' => 1,
        //     'sub_category_id' => 4
        // ]);
        // Category::create([
        //     'main_category_id' => 1,
        //     'sub_category_id' => 4
        // ]);

    }
}
