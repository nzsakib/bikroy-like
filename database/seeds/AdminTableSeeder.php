<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        \DB::table('admins')->truncate();
        \DB::table('roles')->delete();
        \DB::table('permissions')->delete();
        \DB::table('model_has_roles')->delete();
        \DB::table('role_has_permissions')->delete();
        \DB::table('model_has_permissions')->delete();
        
        $admin = Admin::create([
            'name' => 'Neo Market',
            'email' => 'admin@neomarket.xyz',
            'password' => bcrypt('Hello1234'),
        ]);

        $role = Role::create(['name' => 'super-admin', 'guard_name' => 'admin']);

        $admin->assignRole($role);
    }
}
