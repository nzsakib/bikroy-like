<?php

use App\Models\Tag;
use App\Models\Item;
use App\Models\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('posts')->truncate();
        \DB::table('items')->truncate();
        \DB::table('item_counters')->truncate();
        \DB::table('tags')->truncate();
        \DB::table('item_galleries')->truncate();
        \DB::table('tag_item')->truncate();

        /**
         * https://s3.ap-south-1.amazonaws.com/shopitbd/items/product1.jpg
         */
        $item = Item::create([
            'price' => 200,
            'heading' => 'Dart Board',
            'caption' => 'Product Type: Sports
            Color: Black, Grey, Yellow, Red, Navy blue, White, Blue
            Main Material: Steal
            Stylish and Fashionable',
            'address' => 'North, Banani',
            'used' => true,
            'category_id' => 4
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro1.jpeg'
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro1a.jpeg'
        ]);
        $tagIds = $this->createTags(['#dart', '#sports', '#dart2']);
        $item->tags()->attach($tagIds);
        $item->addQuantity(3);
        Post::create([
            'poster_id' => 1,
            'item_id' => $item->id,
            'status' => 1,
        ]);
        ////////////////////////////////////////////// 2
        $item = Item::create([
            'price' => 150,
            'heading' => 'Passport Holder',
            'caption' => 'Smart Leather Passport Holder, Material: 100% Genuine leather.
            Size: 4/5.5 Inches.
            Weight: 90gm.
            Pocket/slot: 4.
            Gender: Men/women.
            Color: As same as picture.',
            'address' => 'Dhanmondi, Dhaka',
            'used' => true,
            'category_id' => 2 
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro2.jpeg'
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro2a.jpeg'
        ]);
        $tagIds = $this->createTags(['#passport', '#passport_holder', '#leather']);
        $item->tags()->attach($tagIds);
        $item->addQuantity(5);
        Post::create([
            'poster_id' => 2,
            'item_id' => $item->id,
            'status' => 1,
        ]);
        //////////////////////////////////////////////
        $item = Item::create([
            'price' => 2900,
            'heading' => 'M29 Smart Watch for Men & Women 1.22 "Touch Screen',
            'caption' => 'Main chip: NRF52832 QFAA Acceleration.
            Sensor: for BOSCH BMA421.
            Heart rate: HRS3300.
            Screen: IPS true color screen.
            Size: 1.22 rounds.
            240x240 resolutions.
            Blood pressure, blood pressure measurement, supporting.
            Dynamic heart rate: heart rate curve.
            Touch: a touch screen.
            Ambient temperature ambient temperature 10-50 degrees.
            Chargeand: USB charging interface.
            Bluetooth: Bluetooth LE 4, 0.
            Applicable system: Applicable to support Android 4, 4 or higher for Apple IOS9.0 or higher.',
            'address' => 'Dhanmondi, Dhaka',
            'used' => true,
            'category_id' => 4 
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro3.jpeg'
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro3a.jpeg'
        ]);
        $tagIds = $this->createTags(['#watch', '#smartwatch', '#smart', '#sports']);
        $item->tags()->attach($tagIds);
        $item->addQuantity(7);
        Post::create([
            'poster_id' => 1,
            'item_id' => $item->id,
            'status' => 1,
        ]);
        //////////////////////////////////////////////
        $item = Item::create([
            'price' => 22000,
            'heading' => 'Xiaomi Redmi Note 7 Pro 4GB/64GB',
            'caption' => 'Xiaomi Redmi Note 7 Pro 4GB/64GB comes with 48MP AI rear camera with Sony IMX586 camera sensor, Qualcomm® Snapdragon™ 675 processor, 4000mAh two-day battery, Type-C charging port and 16cm (6.3) FHD+ Dot Notch Display. Grab this phone at the best price from Neo Market.',
            'address' => 'Farmgate, Dhaka',
            'used' => false,
            'category_id' => 7 
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro4.jpeg'
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro4a.jpeg'
        ]);
        $tagIds = $this->createTags(['#mi', '#xiaomi', '#mobile', '#smartphone', '#android']);
        $item->tags()->attach($tagIds);
        $item->addQuantity(2);
        Post::create([
            'poster_id' => 4,
            'item_id' => $item->id,
            'status' => 1,
        ]);
        //////////////////////////////////////////////
        $item = Item::create([
            'price' => 740,
            'heading' => 'Long Gown Style Single Kamiz ',
            'caption' => 'Kamiz: Linen fabric.
            Stitched.
            Size: Chest 40” / Length 46”.
            Color: Violet.',
            'address' => 'Bashundhara, Dhaka',
            'used' => false,
            'category_id' => 6 
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro5.jpeg'
        ]);
        $item->images()->create([
            'image_link' => 'https://s3.ap-south-1.amazonaws.com/shopitbd/items/pro5a.jpeg'
        ]);
        $tagIds = $this->createTags(['#kamiz', '#saloar_kamiz', '#women']);
        $item->tags()->attach($tagIds);
        $item->addQuantity(3);
        Post::create([
            'poster_id' => 5,
            'item_id' => $item->id,
            'status' => 1,
        ]);

        // factory('App\Models\Post', 20)->create();
    }

    protected function createTags($tags) 
    {
        // dd($tags);
        $ids = [];
        foreach ($tags as $tag) {
            $record = Tag::create(['tag_name' => $tag]);
            $ids[] = $record->id;
        }

        return $ids;
    }
}
