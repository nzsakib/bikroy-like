<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentMethod;
use App\Models\Item;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::truncate();
        PaymentMethod::create([
            'name' => 'bKash'
        ]);

        PaymentMethod::create([
            'name' => 'Cash in delivery'
        ]);

        PaymentMethod::create([
            'name' => 'Card Payment'
        ]);
    }
}
