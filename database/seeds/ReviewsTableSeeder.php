<?php

use Illuminate\Database\Seeder;
use App\Models\UserAccount;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('carts')->truncate();
        \DB::table('purchases')->truncate();
        \DB::table('reviews')->truncate();


        // user with id 2 buys 10 product of user with id 3
        // $userTwo = UserAccount::find(2);
        $userOne = UserAccount::find(3);

        $posts = $userOne->posts;

        $posts->each(function($post) {
            // seed cart first 
            $cart = factory('App\Models\Cart')->create([
                'user_id' => 2, 
                'post_id' => $post->id,
                'bought' => true
            ]);
            // seed purchases 
            $p = factory('App\Models\Purchase')->create([
                'cart_id' => $cart->id
            ]);
    
            factory('App\Models\Review')->create([
                'purchase_id' => $p->id,
                'reviewer_id' => 2,
                'poster_id' => 3
            ]);

        });
    }
}
