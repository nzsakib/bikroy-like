<?php

use Illuminate\Database\Seeder;
use App\Models\Purchase;
use Carbon\Carbon;

class PurchaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\Purchase')->create(['created_at' => Carbon::today()->subDays(1)]);
        factory('App\Models\Purchase')->create(['created_at' => Carbon::today()->subDays(2)]);
        factory('App\Models\Purchase')->create(['created_at' => Carbon::today()->subDays(3)]);
        factory('App\Models\Purchase')->create(['created_at' => Carbon::today()->subDays(4)]);
        factory('App\Models\Purchase')->create(['created_at' => Carbon::today()->subDays(5)]);
        factory('App\Models\Purchase')->create(['created_at' => Carbon::today()->subDays(6)]);
        factory('App\Models\Purchase')->create(['created_at' => Carbon::today()->subDays(7)]);
    }
}
