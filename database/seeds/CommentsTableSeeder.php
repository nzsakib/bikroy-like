<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('comments')->truncate();
        
        $users = App\Models\UserAccount::latest()->take(10)->get();
        $posts = Post::latest()->take(10)->get();

        $comments = [
            'How is the quality?',
            'A good product',
            'Can you reduce the price please',
            'Good service'
        ];
        foreach ($users as $user ) {
            
            foreach ($posts as $post) {
                
                $post->comments()->create([
                    'comment' => $comments[array_rand($comments)],
                    'user_id' => $user->id,
                ]);
                
            }
        }
    }
}
