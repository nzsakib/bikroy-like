<?php

use Illuminate\Database\Seeder;
use App\Models\Item;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tags')->truncate();
        \DB::table('tag_item')->truncate();

        $tags = factory('App\Models\Tag', 6)->create();
        $ids = $tags->pluck('id')->toArray();
        $items = Item::all();

        $items->each(function($item) use($ids) {
            $item->tags()->attach($ids);
        });
    }
}
