<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $tables = [
        'user_accounts', 'posts', 'items', 'user_profiles', 'item_galleries',
        'item_counters', 'admins', 'payment_methods', 'purchases', 'reviews', 'notifications', 'likes', 'follows', 'comments', 'chat_rooms', 'chat_messages', 'carts', 
    ];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->tables as $table) {
            \DB::table($table)->truncate();
        }

        // Creates 10 user with 10 post with each
        $this->call(UserAccountTableSeeder::class);
        $this->command->info('Created 5 user profile with 1 post each.');

        $this->call(CategoryTableSeeder::class);
        $this->command->info('Category seeded');
        
        $this->call(PostTableSeeder::class);
        $this->command->info('Post Table seeded');

        
        
        $this->call(CommentsTableSeeder::class);
        $this->command->info('Comments table seeded');

        // $this->call(ReviewsTableSeeder::class);
        // $this->command->info('Reviews table seeded');

        // $this->call(TagsTableSeeder::class);
        // $this->command->info('Tags table seeded');

        // $this->call(LikesTableSeeder::class);
        // $this->command->info('Likes table seeded');

        $this->call(PaymentMethodsTableSeeder::class);
        $this->command->info('Payment Methods table seeded');

        $this->call(DeliveryTypesTableSeeder::class);
        $this->command->info('delivery types table seeded');

        // $this->call(ChatsTableSeeder::class);
        // $this->command->info('Chats table seeded');

        $this->call(AdminTableSeeder::class);
        $this->command->info('Admin table seeded');
        
    }
}
