<?php

use App\Models\UserAccount;
use App\Models\UserProfile;
use Illuminate\Database\Seeder;

class UserAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_accounts')->truncate();
        \DB::table('user_profiles')->truncate();

        $user = UserAccount::create([
           'phone_number' => '+8801917169307',
           'password' => bcrypt('Hello1234'),
           'isActive' => true,
           'isComplete' => true,
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'username' => 'sakib',
            'name' => 'Sakib',
            'gender' => 'male',
            'email' => 'sakib@doe.com',
            'phone_verified' => true,
            'address' => 'Bashundhara, Dhaka'
        ]);


        $user = UserAccount::create([
           'phone_number' => '+8801630295320', // tanha
           'password' => bcrypt('Hello1234'),
           'isActive' => true,
           'isComplete' => true,
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'username' => 'tanha',
            'name' => 'Tanha',
            'gender' => 'female',
            'email' => 'tanha@doe.com',
            'phone_verified' => true,
            'address' => 'Bashundhara, Dhaka'
        ]);


        $user = UserAccount::create([
           'phone_number' => '+8801620405850', // ellen
           'password' => bcrypt('Hello1234'),
           'isActive' => true,
           'isComplete' => true,
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'username' => 'ellen',
            'name' => 'Ellen',
            'gender' => 'female',
            'email' => 'ellen@doe.com',
            'phone_verified' => true,
            'address' => 'Bashundhara, Dhaka'
        ]);


        $user = UserAccount::create([
           'phone_number' => '+8801676449264', // Saif
           'password' => bcrypt('Hello1234'),
           'isActive' => true,
           'isComplete' => true,
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'username' => 'saif',
            'name' => 'Saif',
            'gender' => 'male',
            'email' => 'saif@doe.com',
            'phone_verified' => true,
            'address' => 'Bashundhara, Dhaka'
        ]);


        $user = UserAccount::create([
           'phone_number' => '+8801685413741', // Samin
           'password' => bcrypt('Hello1234'),
           'isActive' => true,
           'isComplete' => true,
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'username' => 'samin',
            'name' => 'Samin',
            'gender' => 'male',
            'email' => 'samin@doe.com',
            'phone_verified' => true,
            'address' => 'Bashundhara, Dhaka'
        ]);
        
        // $custom = factory('App\Models\UserAccount')->create([
        //     'phone_number' => '+8801911111111'
        // ]);
        // factory('App\Models\UserProfile')->create(['user_id' => $custom->id]);
        // factory('App\Models\Post', 10)->create(['poster_id' => $custom->id]);
        
        // factory('App\Models\UserAccount', 10)->create()->each(function($user) {
        //     factory('App\Models\Post', 10)->create(['poster_id' => $user->id]);
        //     factory('App\Models\UserProfile')->create(['user_id' => $user->id]);
        // });
    }
}
