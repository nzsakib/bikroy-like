<?php

use App\Models\Post;
use App\Models\UserAccount;
use Illuminate\Database\Seeder;

class ChatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('chat_rooms')->truncate();
        \DB::table('chat_messages')->truncate();
        
        $posts = Post::latest()->take(10)->get();

        $posts->each(function($post) {
            $room = factory('App\Models\ChatRoom')->create([
                                            'source_id' => $post->id,
                                            'buyer_id' => 1 ]);
            // 
            factory('App\Models\ChatMessage', 30)->create(['chat_room_id' => $room->id]);
        });

    }
}
