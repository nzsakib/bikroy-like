<?php

use Illuminate\Http\Request;
use App\Notifications\TestNotification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Models\UserAccount;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

// Firebase test push 
Route::post('/firebase/test', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'token' => 'required|string'
    ]);

    if ($validator->fails()) {
        return response(['error' => 'Token required'], 400);
    }

    $result = Notification::send(new UserAccount, new TestNotification($request->token));

    return response([
        'status' => 'success',
        'result' => $result
    ]);
});