<?php


Route::group([
    'prefix' => 'admin',
    'middleware' => 'auth:admin',
    'namespace' => 'Admin'

], function () {
    Route::get('user-count', 'CountController@userCount');
    Route::get('post-count', 'CountController@postCount');
    Route::get('transaction-count', 'CountController@transactionCount');
    Route::get('fb-signup-count', 'CountController@fbCount');
    Route::get('google-signup-count', 'CountController@googleCount');
    Route::get('gender-count', 'CountController@genderCount');
    Route::get('new-signup', 'CountController@newSignup');
    Route::get('all-count', 'CountController@allCount');

    Route::get('top-user-analytics', 'AnalyticsController@topUserAnalytics');
    Route::get('user-analytics', 'AnalyticsController@userAnalytics');
    Route::get('sells-analytics', 'AnalyticsController@sellsAnalytics');

    // user related routes
    Route::get('user/{user}/posts', 'PostController@userPosts');
    Route::post('user/ban', 'UserController@banUser');
    Route::post('user/activate', 'UserController@activateUser');
    Route::get('all-users', 'UserController@allUsers');
    Route::patch('update-user', 'UserController@updateUser');
    Route::delete('delete-user', 'UserController@deleteUser');

    Route::apiResource('vat', 'VatController');
    Route::apiResource('delivery-type', 'DeliveryTypeController');
    Route::apiResource('post-report', 'PostReportController');
    Route::post('report/resolve', 'PostReportController@resolvePostReport');
    Route::post('report/pending', 'PostReportController@pendingPostReport');

    Route::get('posts/new', 'PostController@newPost');
    Route::get('posts/all', 'PostController@index');
    Route::get('posts/active', 'PostController@activePost');
    Route::get('posts/sold', 'PostController@soldPost');
    Route::patch('post/active', 'PostController@makePostActive');
    Route::post('post/block', 'PostController@blockPost');
    Route::get('posts/blocked', 'PostController@getBlockedPost');
    Route::get('posts/featured', 'PostController@getFeaturedPost');
    Route::patch('post/featured', 'PostController@makeFeaturedPost');


    Route::apiResource('subcategory', 'SubcategoryController');
    Route::apiResource('maincategory', 'MainCategoryController');
    Route::post('maincategory/{maincategory}', 'MainCategoryController@update');
    Route::apiResource('category', 'CategoryController');

    Route::post('/create', 'AdminController@store'); // create new admin
    Route::post('/role/assign', 'RolesController@assignToAdmin');
    Route::apiResource('role', 'RolesController');
    Route::apiResource('permission', 'PermissionController');
    Route::post('/permission/assign-to-role', 'PermissionController@assignToRole');

    Route::get('/admin-users', 'AdminController@index');
    Route::patch('/admin-update', 'AdminController@update');
    Route::get('/{admin}/info', 'AdminController@show');
    Route::delete('/admin-delete', 'AdminController@destroy');

    // Auth related routes
    Route::get('info', 'LoginController@getAdminDetails');
    Route::post('refresh', 'LoginController@refreshToken');

    // Transaction details
    Route::get('/transactions', 'TransactionController@index');
    Route::get('/transaction/{transaction_id}', 'TransactionController@show');
    Route::get('/transaction/post/{post}', 'TransactionController@showByPost');

    //seller release
    Route::get('purchase/seller_release', 'PurchaseController@getSellerRelease'); // TODO pending
    Route::post('/delivered', 'PurchaseController@deliver');
});

Route::group([
    'prefix' => 'admin',
    'middleware' => 'auth:admin'
], function () {
    Route::apiResource('payment', 'PaymentMethodsController');

    // TODO: complete message from user to admin end 
    Route::get('support-chatroom/{room}', 'SupportAdmin\SupportRoomController@show');
    Route::post('support-chatroom', 'SupportAdmin\SupportRoomController@create');
    Route::delete('support-chatroom/{room}', 'SupportAdmin\SupportRoomController@destroy');

    // Route::get('chatroom/{room}/archive', 'Support\ChatRoomController@archive');
    Route::post('support-message', 'SupportAdmin\SupportMessageController@store');
    Route::delete('support-message/{message}', 'SupportAdmin\SupportMessageController@destroy');
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin'

], function () {
    Route::post('login', 'LoginController@authenticate');
});
