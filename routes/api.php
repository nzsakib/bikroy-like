<?php

// Authentication
// Route::post('register', 'UserAccountController@register');
Route::post('register', 'RegisterController@register');
Route::post('login', 'LoginController@authenticate');
Route::post('check/email-mobile', 'RegisterController@checkIfExists');
Route::post('logout', 'LoginController@logout')->middleware('auth.all');

Route::group(['middleware' => ['auth']], function () {

    Route::get('token/refresh', 'LoginController@refreshToken');

    Route::get('user', 'LoginController@getAuthenticatedUser');

});

Route::get('profile/{username}', 'ProfileController@showByUsername');

Route::apiResource('posts', 'Api\PostController');
Route::post('post/report', 'PostReportController@store');

Route::get('cart/count', 'Api\CartController@count');
Route::apiResource('cart', 'Api\CartController');
// Transaction id generate 
Route::get('transaction-id', 'Api\CartController@generateTransaction');

Route::post('cart/purchase', 'Api\CartController@purchase');
Route::get('purchased', 'Api\CartController@allPurchased');
Route::get('sold', 'Api\CartController@allSold');


Route::get('posts/{post}/comment', 'Api\CommentsController@index');
Route::post('posts/{post}/comment', 'Api\CommentsController@store');
Route::delete('comment/{comment}', 'Api\CommentsController@destroy');
Route::patch('comment/{comment}', 'Api\CommentsController@update');

Route::post('image', 'ImageController@store');
Route::post('images', 'ImageController@storeAll');
Route::post('image/delete', 'ImageController@destroy');

// Route::apiResource('like', 'LikesController');
Route::post('like', 'LikesController@store');
Route::delete('like', 'LikesController@destroy');
Route::get('like/{post}', 'LikesController@show');

Route::post('wishlist', 'WishesController@store');
Route::delete('wishlist', 'WishesController@destroy');
Route::get('wishlist', 'WishesController@index');

Route::get('chatroom/selling', 'Chat\ChatRoomController@selling');
Route::get('chatroom/buying', 'Chat\ChatRoomController@buying');
Route::get('chatroom/all', 'Chat\ChatRoomController@index');
Route::get('chatroom/{room}', 'Chat\ChatRoomController@show');
Route::post('chatroom', 'Chat\ChatRoomController@create');
// destroy chatroom Should not be allowed by user
// Route::delete('chatroom/{room}', 'Chat\ChatRoomController@destroy');
Route::post('chatroom/archive', 'Chat\ChatRoomController@archive');
Route::post('chatroom/seen', 'Chat\ChatRoomController@seen');
Route::get('message/unseencount', 'Chat\ChatMessageController@unseenCount');

Route::post('chat', 'Chat\ChatMessageController@store');
Route::delete('chat/{message}', 'Chat\ChatMessageController@destroy');

Route::post('token/firebase', 'UserAccountController@firebaseToken')
    ->middleware('jwt.verify');

// User related routes
Route::post('user/update', 'UserAccountController@update');
Route::get('user/post', 'ProfileController@index');
Route::get('user/{id}', 'ProfileController@show');
Route::get('user/{id}/liked', 'ProfileController@likedPost');
Route::get('user/{id}/review', 'ReviewsController@index');
Route::get('user/{id}/posts', 'ProfileController@allPost');

// /review 
// /review/purchase_id
// 
Route::post('review', 'ReviewsController@store');
Route::get('review/{id}', 'ReviewsController@show')->middleware('auth');
Route::patch('review/{id}', 'ReviewsController@edit')->middleware('auth');
Route::delete('review', 'ReviewsController@destroy');

Route::post('password/change', 'PasswordController@change');
Route::post('password/email', 'ResetPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'ResetPasswordController@reset');

// Email verify via OTP
Route::post('email/send', 'EmailVerificationController@sendCode');
Route::post('email/verify', 'EmailVerificationController@verify');

Route::get('/search', 'SearchController@index');

Route::get('payment-methods', 'PaymentMethodsController@index');
Route::get('delivery-methods', 'Admin\DeliveryTypeController@index');

Route::get('category', 'CategoryController@index');
Route::get('maincategory/{maincategory}', 'CategoryController@showSubcategory');

// Tags
Route::get('tags', 'TagsController@index');
Route::get('tags/related', 'TagsController@related');
Route::get('tags/popular', 'TagsController@popular');

// Follow system
Route::post('follow', 'FollowController@follow');
Route::delete('unfollow', 'FollowController@unfollow');
Route::get('user/{id}/following', 'FollowController@following');
Route::get('user/{id}/follower', 'FollowController@follower');

// User feed 
Route::get('feed', 'FeedController@index');

// Notifications 
Route::get('notifications', 'NotificationController@index');
Route::post('notification', 'NotificationController@markAsRead');

// Mention users 
Route::get('/posts/{id}/mention', 'Api\CommentsController@mentionableUsername');

Route::get('/range', 'Api\PostController@minMaxPriceRange');
