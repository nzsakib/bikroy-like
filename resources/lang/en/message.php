<?php 

return [
    'bannedLogin' => 'Your account is banned. Please contact our customer support at support@neomarket.xyz or call us at 0199999999999',
    'socialExists' => "The social account is used in a different user's neomarket account."
];  