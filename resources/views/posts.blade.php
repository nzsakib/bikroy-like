<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        * {
            font-family: sans-serif;

        }

        body {
        }
    </style>

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <br><br>
    <div class="container">
        <div class="text-center">

            <form action="/api/search" method="GET">
                <input type="text" name="search">
                <button class="btn btn-primary" type="submit">Search</button>
            </form>
        </div>
        <br><br>

@foreach ($posts as $post)
        
        <h2> <strong>Heading: </strong> {{ $post->item->heading }} </h2>
        
        <strong>Caption:</strong>
        {{ $post->item->caption }}

        <br><br>
        <strong>Username: </strong>
        {{ $post->poster->userProfile->username }}

<br>
    <br>
    
        <strong>Item location: </strong>
    {{ $post->item->address }}
    <br>
    <br>
    <strong>Tags: </strong>
    @foreach ($post->item->tags as $tag)
        {{ $tag->tag_name }}
    @endforeach
    
    <br><br>
    <strong>Category : </strong>
    {{ $post->item->category->subCategory->name }}

    <br>
    <hr>
@endforeach
    
</div>
</body>
</html>

