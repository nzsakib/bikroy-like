@component('mail::message')
# Login Credentials 

Email: {{ $user->email }}

Password: {{ $password }}

Click the button to go to admin login page.

@component('mail::button', ['url' => 'http://neomarket.xyz/admin/login'])
Admin Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
