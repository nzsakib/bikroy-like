@component('mail::message')
# Password reset Link
Hi {{ $user->name }},

You've recently asked to reset the password for this NeoMarket account:
{{ $user->email }}

To update your password, click the button below:

@component('mail::button', ['url' => "www.neomarket.xyz/reset/{$token}"])
Reset My Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
