@component('mail::message')
# Your email verification code 

Hello {{ $user->name }},

Here is the code you need to verify your email: <code> {{ $code }} </code>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
