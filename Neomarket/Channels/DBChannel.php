<?php 
namespace Neomarket\Channels;

use ReflectionClass;
use Illuminate\Notifications\Notification;

class DBChannel 
{
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toDatabase($notifiable);
        
        $from = isset($notification->from) ? $notification->from : null;
        $source = $notification->source;
        $source_type = (new ReflectionClass($source))->getShortName();
        $source_id = $source->id;

        return $notifiable->notifications()->create([
            'type' => (new ReflectionClass($notification))->getShortName(),
            'data' => $message,
            'source_type' => $source_type,
            'source_id' => $source_id,
            'from_user_id' => $from ? $from->id : null,
        ]);
    }
}