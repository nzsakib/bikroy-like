<?php 

namespace Neomarket\Notifications;

use App\Notifications\YourPostIsLiked;
use Illuminate\Notifications\RoutesNotifications;
use Neomarket\Notifications\HasDatabaseNotifications;

trait Notifiable 
{
    use RoutesNotifications, HasDatabaseNotifications;

    public function deleteLikeNotification($model)
    {
        $n = $this->getNotification($model, 'YourPostIsLiked');
        
        if ($n) {
            $n->delete();
        }
    }

    public function deleteCommentNotification($model)
    {
        $n = $this->getNotification($model, 'YouHaveNewComment');
        
        if ($n) {
            $n->delete();
        }
    }

    public function deleteFollowNotification($model)
    {
        $n = $this->getNotification($model, 'YouHaveBeenFollowed');
        if ($n) {
            $n->delete();
        }
    }

    public function deletereviewNotification($model)
    {
        $n = $this->getNotification($model, 'YouHaveAReview');
        if ($n) {
            $n->delete();
        }
    }

    public function deleteApprovePostNotification($model)
    {
        $n = $this->getNotification($model, 'YourPostIsApproved', 'admin');
        if ($n) {
            $n->delete();
        }
    }

    protected function getNotification($model, $type, $fromUser = null)
    {
        $query = $this->notifications()
                    ->source($model)
                    ->whereType($type);
                    
        if ($fromUser === 'admin') {
            return $query->first();
        }

        return $query->whereFromUserId(auth()->id())->first();
    }
}